package me.knighthood.protocol;

import java.io.Serializable;

/**
 *
 * @author Emir
 */
public class Item implements Serializable {

    private static final long serialVersionUID = 1525163321838262369L;
    private final String name;
    private final String examine;
    private final String imageid;
    private final boolean stackable;
    private final String[] rightClick;

    public Item(String name, String examine, String imageid, boolean stackable, String[] rightClick) {
        this.name = name;
        this.examine = examine;
        this.imageid = imageid;
        this.stackable = stackable;
        this.rightClick = rightClick == null ? new String[0] : rightClick;
    }

    public String getExamine() {
        return examine;
    }

    public String getImageid() {
        return imageid;
    }

    public String getName() {
        return name;
    }

    public boolean isStackable() {
        return stackable;
    }

    public String[] getRightClick() {
        return rightClick;
    }

}
