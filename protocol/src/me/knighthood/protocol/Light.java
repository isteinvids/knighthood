package me.knighthood.protocol;

import java.io.Serializable;

/**
 *
 * @author Emir
 */
public class Light implements Serializable {

    private final int x;
    private final int y;
    private final int scale;
    private final float red, green, blue, alpha;

    public Light(int x, int y, int scale, float red, float green, float blue, float alpha) {
        this.x = x;
        this.y = y;
        this.scale = scale;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public float getRed() {
        return red;
    }

    public float getGreen() {
        return green;
    }

    public float getBlue() {
        return blue;
    }

    public float getAlpha() {
        return alpha;
    }

    public int getScale() {
        return scale;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
