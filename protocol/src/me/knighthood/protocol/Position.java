package me.knighthood.protocol;

import java.io.Serializable;

/**
 * @author Roe
 *
 */
public final class Position implements Serializable
{

	private static final long serialVersionUID = 2093912803981038L;
	private int x;
	private int y;
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public Position(int x, int y)
	{
		
		this.setX(x);
		this.setY(y);
		
	}

	/**
	 * 
	 * @return x
	 */
	public int getX()
	{
	
		return this.x;
	
	}

	/**
	 * 
	 * @param x
	 */
	public void setX(int x)
	{
	
		this.x = x;
	
	}

	/**
	 * 
	 * @return y
	 */
	public int getY()
	{
	
		return this.y;
	
	}

	/**
	 * 
	 * @param y
	 */
	public void setY(int y)
	{
	
		this.y = y;
	
	}

        public Position offset(float offx, float offy)
        {
            
                this.x += offx;
                this.y += offy;
                return this;
            
        }
        
        public Position copy()
        {
        
                return new Position(x, y);
            
        }

        @Override
        public boolean equals(Object o)
        {
            
                if (o instanceof Position)
                {
                        
                        if (((Position) o).getX() == this.getX() && ((Position) o).getY() == this.getY())
                        {
                            
                                return true;
                                
                        }
                        
                }
                
                return false;
            
        }        

        @Override
        public int hashCode()
        {
            
                int hash = 7;
                hash = 37 * hash + this.x;
                hash = 37 * hash + this.y;
                return hash;
                
        }
        
        @Override
        public String toString()
        {
            
                return "[x : " + x + ", y : " + y + "]";

        }
	
}