package me.knighthood.protocol;

/**
 * 
 * This is the first packet sent to the server. It's used to verify the protocol version, and to say hello to the server
 * 
 * @author Roe
 *
 */
public class PacketHandshake extends Packet
{
	
	private static final long serialVersionUID = -1771799775841973502L;
	private final int major;
	private final int minor;

	public PacketHandshake()
	{
	
                super("Handshake", "This is the first packet sent to the server. It's used to verify the protocol version, and to say hello to the server");
		this.major = 1;
		this.minor = 2;
		
	}
        
	/**
	 * 
	 * This method is used to get the major version of the protocol
	 * 
	 * @return the major protocol version
	 */
	public int getMajor()
	{
	
		return this.major;
	
	}

	/**
	 * 
	 * This method is used to get the minor version of the protocol
	 * 
	 * @return the minor version of the protocol
	 */
	public int getMinor()
	{
	
		return this.minor;
	
	}

}