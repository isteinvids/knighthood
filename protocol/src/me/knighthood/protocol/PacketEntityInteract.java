package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketEntityInteract extends Packet {

    private final String entityName;

    public PacketEntityInteract(String entityName) {
        super("PacketEntityInteract", "Client interacts with an entity");
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

}
