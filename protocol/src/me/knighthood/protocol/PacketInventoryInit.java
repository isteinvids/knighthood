package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketInventoryInit extends Packet {

    private static final long serialVersionUID = 1892839839489238498L;
    private final Item[] items;

    public PacketInventoryInit(Item[] items) {
        super("InventoryInit", "This packet is used to init inventory categories, images, and item list");
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }

}
