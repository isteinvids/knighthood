package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketInventory extends Packet {

    private static final long serialVersionUID = 72198374069284L;
    private final InventoryItem[] items;

    public PacketInventory(InventoryItem[] items) {
        super("PacketInventory", "This packet is used to sync the inventory to the client");
        this.items = items;
    }

    public InventoryItem[] getItems() {
        return items;
    }

}
