package me.knighthood.protocol;

import java.io.Serializable;

/**
 *
 * @author Emir
 */
public class Tile implements Serializable {

    private static final long serialVersionUID = 3981723981729837198L;
    private String tileid;
    private boolean collide;
    private int x;
    private int y;
    private int tilex;
    private int tiley;
    private int tilewidth;
    private int tileheight;
    private int layer;

    public Tile() {
    }

    public Tile(String tileid, boolean collide, int x, int y, int tilex, int tiley, int tilewidth, int tileheight, int layer) {
        this.tileid = tileid;
        this.collide = collide;
        this.x = x;
        this.y = y;
        this.tilex = tilex;
        this.tiley = tiley;
        this.tilewidth = tilewidth;
        this.tileheight = tileheight;
        this.layer = layer;
    }

    public boolean isCollide() {
        return collide;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getTilex() {
        return tilex;
    }

    public int getTiley() {
        return tiley;
    }

    public int getTileWidth() {
        return tilewidth;
    }

    public int getTileHeight() {
        return tileheight;
    }

    public String getTileid() {
        return tileid;
    }

    public int getLayer() {
        return layer;
    }

}
