/**
 * 
 */
package me.knighthood.protocol;

/**
 * @author Roe
 *
 */
public class PacketQuery extends Packet
{
	
	private static final long serialVersionUID = -6248084222039004870L;
	private final int online;
	private final int capacity;
	private final String[] players;

	/**
	 * 
	 * @param online the online player
	 * @param capacity the capacity
	 * @param players the online players
	 */
	public PacketQuery(int online, int capacity, String[] players)
	{
		
		super( "Query", "This packet is used to query the server");
		
		this.online = online;
		this.capacity = capacity;
		this.players = players;

	}

	/**
	 * 
	 * This method is used to get the online player count
	 * 
	 * @return the player count
	 */
	public int getOnline()
	{
	
		return this.online;
	
	}

	/**
	 * 
	 * This method is used to get the capacity
	 * 
	 * @return the capacity
	 */
	public int getCapacity()
	{
	
		return this.capacity;
	
	}

	/**
	 * 
	 * This method is used to get the list of players
	 * 
	 * @return the list of players
	 */
	public String[] getPlayers()
	{
	
		return this.players;
	
	}

}