package me.knighthood.protocol;

import java.io.Serializable;

/**
 * 
 * This class is the primary packet class which all packets HAVE extend
 * 
 * @author Roe
 *
 */
public abstract class Packet implements Serializable
{

	private static final long serialVersionUID = 152516332183826459L;
	private final String name;
	private final String description;
	
	/**
	 * 
	 * @param name the name of the packet. This should be unique for each packet TYPE and not each packet
	 * @param description the description of the packet. This should be unique for each packet TYPE and not each packet
	 */
	protected Packet(String name, String description)
	{
		
		this.name = name;
		this.description = description;
		
	}

	/**
	 * 
	 * This method is used to get the name of the packet
	 * 
	 * @return the name
	 */
	public final String getName()
	{
	
		return this.name;
	
	}

	/**
	 * 
	 * This method is used to get the description of the packet
	 * 
	 * @return the description
	 */
	public final String getDescription()
	{
	
		return this.description;
	
	}
	
}