package me.knighthood.protocol;

/**
 *
 * @author EmirRhouni
 */
public class PacketMoveRequest extends Packet {

    private final int newX;
    private final int newY;

    public PacketMoveRequest(int newX, int newY) {
        super("PacketMoveRequest", "Client sends this packet to server");
        this.newX = newX;
        this.newY = newY;
    }

    public int getNewX() {
        return newX;
    }

    public int getNewY() {
        return newY;
    }

}
