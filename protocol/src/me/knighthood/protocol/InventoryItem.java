package me.knighthood.protocol;

import java.io.Serializable;

/**
 *
 * @author Emir
 */
public class InventoryItem implements Serializable {

    private static final long serialVersionUID = 1229387493827183879L;
    private String itemName;
    private int count;

    public InventoryItem(String itemName, int count) {
        this.itemName = itemName;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public String getItemName() {
        return itemName;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof InventoryItem) {
            InventoryItem ii = (InventoryItem) o;
            if (getItemName().equalsIgnoreCase(ii.getItemName())) {
                if (getCount() == ii.getCount()){// || ii.getCount() == -1) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.itemName != null ? this.itemName.hashCode() : 0);
        hash = 17 * hash + this.count;
        return hash;
    }

    @Override
    public String toString() {
        return "{\"itemName\" : \"" + itemName + "\", \"count\" : " + count + "}";
    }

}
