package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketUpdatePlayers extends Packet {

    private final Player[] players;

    public PacketUpdatePlayers(Player[] players) {
        super("PacketUpdatePlayers", "Updates the players on the client side");
        this.players = players;
    }

    public Player[] getPlayers() {
        return players;
    }

}
