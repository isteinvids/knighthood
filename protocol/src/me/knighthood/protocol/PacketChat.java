/**
 * 
 */
package me.knighthood.protocol;

/**
 * @author Roe
 *
 */
public class PacketChat extends Packet
{

	private static final long serialVersionUID = -5152857859650122323L;
	private final String message;
	
	/**
	 * 
	 * @param message the message to send
	 */
	public PacketChat(String message)
	{
		
		super("Chat", "This is sent when something is chatting");
                this.message = message;
		
	}
	
	/**
	 * 
	 * This method is used to get the message
	 * 
	 * @return the message
	 */
	public String getMessage()
	{
		
		return this.message;
		
	}
	
}