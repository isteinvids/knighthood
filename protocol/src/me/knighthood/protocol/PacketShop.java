package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketShop extends Packet {

    private final int shopId;
    private final String shopName;
    private final String[] shopItems;
    private final InventoryItem[] items;

    public PacketShop(int shopId, String shopName, String[] shopItems, InventoryItem[] items) {
        super("PacketShop", "This packet is used to update a shop");
        this.shopId = shopId;
        this.shopName = shopName;
        this.shopItems = shopItems;
        this.items = items;
    }

    public int getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public String[] getShopItems() {
        return shopItems;
    }

    public InventoryItem[] getItems() {
        return items;
    }

}
