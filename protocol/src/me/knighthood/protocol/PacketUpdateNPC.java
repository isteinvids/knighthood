package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketUpdateNPC extends Packet {

    private final String name;
    private final String type;
    private final String textureId;
    private final double health;
    private final Position position;
    private final boolean dead;

    public PacketUpdateNPC(String name, String type, String textureId, double health, Position position, boolean dead) {
        super("PacketUpdateNPC", "Updates an NPC");
        this.name = name;
        this.type = type;
        this.textureId = textureId;
        this.health = health;
        this.position = position;
        this.dead = dead;
    }

    public String getType() {
        return type;
    }

    public double getHealth() {
        return health;
    }

    public String getNPCName() {
        return name;
    }

    public Position getPosition() {
        return position;
    }

    public String getTextureId() {
        return textureId;
    }

    public boolean isDead() {
        return dead;
    }

}
