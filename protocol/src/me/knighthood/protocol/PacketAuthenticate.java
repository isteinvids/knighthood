/**
 * 
 */
package me.knighthood.protocol;

/**
 * @author Roe
 *
 */
public class PacketAuthenticate extends Packet
{

	private static final long serialVersionUID = 1471474613422056882L;
	private final String username;
	private final String sessionID;
	
	public PacketAuthenticate(String username, String sessionID)
	{
		
		super("Authenticate", "This packet is sent to the server when a user wants to authenticate");
		
		this.sessionID = sessionID;
		this.username = username;
		
	}
	
	/**
	 * 
	 * This method is used to get the username of the player
	 * 
	 * @return the player username
	 */
	public String getUsername()
	{
		
		return this.username;
		
	}

	/**
	 * 
	 * This method is used to get the session id of the player
	 * 
	 * @return the session id
	 */
	public String getSessionID()
	{
	
		return this.sessionID;
	
	}
	
}