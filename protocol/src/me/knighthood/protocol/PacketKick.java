package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketKick extends Packet {

    private final String reason;

    public PacketKick(String reason) {
        super("PacketKick", "This packet is used to inform the client of a kick");
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

}
