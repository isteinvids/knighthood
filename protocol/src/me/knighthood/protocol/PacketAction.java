package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketAction extends Packet {

    private static final long serialVersionUID = 2304006906900493284L;
    private final String command;

    public PacketAction(String command) {
        super("PacketAction", "This packet is used for any action, such as inventory, pressing space, etc etc");
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}
