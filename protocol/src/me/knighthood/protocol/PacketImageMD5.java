package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketImageMD5 extends Packet {

    private static final long serialVersionUID = 923049892384093284L;
    private final String imageName;
    private final String md5;

    public PacketImageMD5(String imageName, String md5) {
        super("PacketImageMD5", "Packet is used by server to match images");
        this.imageName = imageName;
        this.md5 = md5;
    }

    public String getImageName() {
        return imageName;
    }

    public String getMd5() {
        return md5;
    }

}
