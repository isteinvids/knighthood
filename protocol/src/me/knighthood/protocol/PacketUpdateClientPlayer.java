package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketUpdateClientPlayer extends Packet {

    private final Player player;

    public PacketUpdateClientPlayer(Player player) {
        super("PacketUpdateClientPlayer", "This packet is used to update client player");
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

}
