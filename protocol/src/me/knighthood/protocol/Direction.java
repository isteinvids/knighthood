package me.knighthood.protocol;

public enum Direction
{

	NORTH(0),
	NORTH_WEST(1),
	WEST(2),
	SOUTH_WEST(3),
	SOUTH(4),
	SOUTH_EAST(5),
	EAST(6),
	NORTH_EAST(7);
	
	private final int value;
	
	/**
	 * 
	 * @param value
	 */
	Direction(int value)
	{
		
		this.value = value;
		
	}
	
	/**
	 * 
	 * This method is used to get the value of the direction
	 * 
	 * @return
	 */
	public int getValue()
	{
		
		return this.value;
		
	}
	
	/**
	 * 
	 * This method is used to get a direction from it's code
	 * 
	 * @param value the direction code
	 * @return the direction that belongs to the code, but null if the code doesn't match any direction
	 */
	public Direction getDirection(int value)
	{
		
		for(Direction direction : values())
		{
			
			if(direction.getValue() == value)
			{
				
				return direction;
				
			}
			
		}
		
		return null;
		
	}
	
}