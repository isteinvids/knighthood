package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketTeleport extends Packet {

    private static final long serialVersionUID = -6248084222039002270L;
    private final Position teleportTo;

    public PacketTeleport(Position teleportTo) {
        super("PacketTeleport", "This packet is used by server to teleport the client");
        this.teleportTo = teleportTo;
    }

    public Position getTeleportTo() {
        return teleportTo;
    }

}
