package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketWorld extends Packet {

    private final int worldSize;
    private final Tile[] tiles;

    public PacketWorld(int worldSize, Tile[] tiles) {
        super("PacketWorld", "This packet is used to send world to client");
        this.worldSize = worldSize;
        this.tiles = tiles;
    }

    public int getWorldSize() {
        return worldSize;
    }

    public Tile[] getTiles() {
        return tiles;
    }

}
