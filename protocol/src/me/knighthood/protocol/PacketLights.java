package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketLights extends Packet{

    private final Light[] lights;

    public PacketLights(Light[] lights) {
        super("PacketLights", "This packet is used to update client's lighting");
        this.lights = lights;
    }

    public Light[] getLights() {
        return lights;
    }

}
