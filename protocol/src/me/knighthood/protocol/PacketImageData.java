/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public class PacketImageData extends Packet {

    private static final long serialVersionUID = 923049892384093284L;
    private final String imageName;
    private final byte[] bytes;

    public PacketImageData(String imageName, byte[] bytes) {
        super("PacketImageData", "Packet is used by server to send image data");
        this.imageName = imageName;
        this.bytes = bytes;
    }

    public String getImageName() {
        return imageName;
    }

    public byte[] getBytes() {
        return bytes;
    }

}
