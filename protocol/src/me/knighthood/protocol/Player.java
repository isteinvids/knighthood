package me.knighthood.protocol;

import java.io.Serializable;

/**
 *
 * @author Emir
 */
public class Player implements Serializable {

    private static final long serialVersionUID = 23453453L;
    private final String username;
    private final Position position;
    private double health;

    public Player(String username, Position position, double health) {
        this.username = username;
        this.position = position;
        this.health = health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public double getHealth() {
        return health;
    }

    public Position getPosition() {
        return position;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "[username : " + username + ", position : " + position + ", health : " + health + "]";
    }
}
