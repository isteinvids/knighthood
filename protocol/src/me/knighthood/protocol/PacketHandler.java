package me.knighthood.protocol;

/**
 *
 * @author Emir
 */
public abstract class PacketHandler {

    public final void handlePacket(Packet packet) {
        if (packet instanceof PacketAction) {
            this.handlePacketAction((PacketAction) packet);
        }
        if (packet instanceof PacketAuthenticate) {
            this.handlePacketAuthenticate((PacketAuthenticate) packet);
        }
        if (packet instanceof PacketInventory) {
            this.handlePacketInventory((PacketInventory) packet);
        }
        if (packet instanceof PacketChat) {
            this.handlePacketChat((PacketChat) packet);
        }
        if (packet instanceof PacketHandshake) {
            this.handlePacketHandshake((PacketHandshake) packet);
        }
        if (packet instanceof PacketImageMD5) {
            this.handlePacketImageMD5((PacketImageMD5) packet);
        }
        if (packet instanceof PacketImageData) {
            this.handlePacketImageData((PacketImageData) packet);
        }
        if (packet instanceof PacketInventoryInit) {
            this.handlePacketInventoryInit((PacketInventoryInit) packet);
        }
        if (packet instanceof PacketQuery) {
            this.handlePacketQuery((PacketQuery) packet);
        }
        if (packet instanceof PacketTeleport) {
            this.handlePacketTeleport((PacketTeleport) packet);
        }
        if (packet instanceof PacketWorld) {
            this.handlePacketWorld((PacketWorld) packet);
        }
        if (packet instanceof PacketUpdatePlayers) {
            this.handlePacketUpdatePlayers((PacketUpdatePlayers) packet);
        }
        if (packet instanceof PacketUpdateNPC) {
            this.handlePacketUpdateNPC((PacketUpdateNPC) packet);
        }
        if (packet instanceof PacketLights) {
            this.handlePacketLights((PacketLights) packet);
        }
        if (packet instanceof PacketMoveRequest) {
            this.handlePacketMoveRequest((PacketMoveRequest) packet);
        }
        if (packet instanceof PacketEntityInteract) {
            this.handlePacketEntityInteract((PacketEntityInteract) packet);
        }
        if (packet instanceof PacketShop) {
            this.handlePacketShop((PacketShop) packet);
        }
        if (packet instanceof PacketKick) {
            this.handlePacketKick((PacketKick) packet);
        }
        if (packet instanceof PacketUpdateClientPlayer) {
            this.handlePacketUpdateClientPlayer((PacketUpdateClientPlayer) packet);
        }
    }

    public abstract void handlePacketMoveRequest(PacketMoveRequest packetMoveRequest);

    public abstract void handlePacketAction(PacketAction packetAction);

    public abstract void handlePacketInventory(PacketInventory packetInventory);

    public abstract void handlePacketLights(PacketLights packetLights);

    public abstract void handlePacketImageMD5(PacketImageMD5 packetImage);

    public abstract void handlePacketImageData(PacketImageData packetImageData);

    public abstract void handlePacketInventoryInit(PacketInventoryInit packetInventoryInit);

    public abstract void handlePacketAuthenticate(PacketAuthenticate packetAuthenticate);

    public abstract void handlePacketChat(PacketChat packetChat);

    public abstract void handlePacketHandshake(PacketHandshake packetHandshake);

    public abstract void handlePacketQuery(PacketQuery packetQuery);

    public abstract void handlePacketTeleport(PacketTeleport packetQuery);

    public abstract void handlePacketWorld(PacketWorld packetWorld);

    public abstract void handlePacketUpdatePlayers(PacketUpdatePlayers packetUpdatePlayers);

    public abstract void handlePacketUpdateNPC(PacketUpdateNPC packetUpdateNPC);

    public abstract void handlePacketEntityInteract(PacketEntityInteract packetEntityInteract);

    public abstract void handlePacketShop(PacketShop packetShop);

    public abstract void handlePacketKick(PacketKick packetKick);

    public abstract void handlePacketUpdateClientPlayer(PacketUpdateClientPlayer packetUpdateClientPlayer);

    /**
     * Is called after a packet is sent
     * @param packet
     */
    public abstract void handleSentPacket(Packet packet);
}
