package me.knighthood.client;

import java.io.File;
import java.util.Arrays;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketAction;
import me.knighthood.protocol.PacketAuthenticate;
import me.knighthood.protocol.PacketChat;
import me.knighthood.protocol.PacketEntityInteract;
import me.knighthood.protocol.PacketHandler;
import me.knighthood.protocol.PacketHandshake;
import me.knighthood.protocol.PacketImageData;
import me.knighthood.protocol.PacketImageMD5;
import me.knighthood.protocol.PacketInventory;
import me.knighthood.protocol.PacketInventoryInit;
import me.knighthood.protocol.PacketKick;
import me.knighthood.protocol.PacketLights;
import me.knighthood.protocol.PacketMoveRequest;
import me.knighthood.protocol.PacketQuery;
import me.knighthood.protocol.PacketShop;
import me.knighthood.protocol.PacketTeleport;
import me.knighthood.protocol.PacketUpdateClientPlayer;
import me.knighthood.protocol.PacketUpdateNPC;
import me.knighthood.protocol.PacketUpdatePlayers;
import me.knighthood.protocol.PacketWorld;
import me.knighthood.protocol.Player;
import me.knighthood.protocol.Position;

/**
 *
 * @author Emir
 */
public class PacketHandling extends PacketHandler {

    @Override
    public void handlePacketShop(PacketShop packetShop) {
        GuiShop guiShop = ScreenGame.getInstance().getGuiGameHUD().getGuiShop();
        guiShop.setShopId(packetShop.getShopId());
        guiShop.setShopName(packetShop.getShopName());
        guiShop.setItems(packetShop.getShopItems());
        guiShop.setInReturnFors(packetShop.getItems());
    }

    @Override
    public void handlePacketAction(PacketAction packetAction) {
        if (packetAction.getCommand().startsWith("moving ")) {
            boolean moving = packetAction.getCommand().substring(7).equals("true");
            ScreenGame.getInstance().setMoving(moving);
        }
        if (packetAction.getCommand().startsWith("dialog ")) {
            String dialog = packetAction.getCommand().substring(7);
            ScreenGame.getInstance().getGuiGameHUD().setDialog(dialog);
        }
        if (packetAction.getCommand().equals("loaded")) {
            System.out.println("got action: " + packetAction.getCommand());
            ScreenGame.getInstance().setLoading(false);
        }
    }

    @Override
    public void handlePacketInventory(PacketInventory packetInventory) {
        ScreenGame.getInstance().getInventory().setInventoryItems(packetInventory.getItems());
    }

    @Override
    public void handlePacketInventoryInit(PacketInventoryInit packetInventoryInit) {
        ScreenGame.getInstance().getInventory().setItems(packetInventoryInit.getItems());
    }

    @Override
    public void handlePacketImageData(PacketImageData packetImageData) {
        MainGame.getInstance().getImageManager().createServerImage(packetImageData.getImageName(), packetImageData.getBytes());
    }

    @Override
    public void handlePacketImageMD5(PacketImageMD5 packetImage) {
        if (MainGame.getInstance().getServerImageManager().imageExists(packetImage.getImageName(), packetImage.getMd5())) {
            File f = MainGame.getInstance().getServerImageManager().getImageFile(packetImage.getImageName());
            MainGame.getInstance().getImageManager().addServerImage(packetImage.getImageName(), f);
        } else {
            ScreenGame.getInstance().getConnection().addPacketToQueue(packetImage);
        }
    }

    @Override
    public void handlePacketAuthenticate(PacketAuthenticate packetAuthenticate) {
        //server only
    }

    @Override
    public void handlePacketChat(PacketChat packetChat) {
        ScreenGame.getInstance().getGuiGameHUD().addMessage(packetChat.getMessage());
    }

    @Override
    public void handlePacketHandshake(PacketHandshake packetHandshake) {
    }

    @Override
    public void handlePacketQuery(PacketQuery packetQuery) {
    }

    @Override
    public void handlePacketTeleport(PacketTeleport packetQuery) {
        ScreenGame.getInstance().getPlayer().setPosition(packetQuery.getTeleportTo());
    }

    @Override
    public void handlePacketWorld(PacketWorld packetWorld) {
        ScreenGame.getInstance().getWorld().mapSize = packetWorld.getWorldSize();
        ScreenGame.getInstance().getWorld().tiles = packetWorld.getTiles();
    }

    @Override
    public void handlePacketUpdatePlayers(PacketUpdatePlayers packetUpdatePlayers) {
        World w = ScreenGame.getInstance().getWorld();
        w.players = new EntityPlayer[packetUpdatePlayers.getPlayers().length];
        for (int i = 0; i < w.players.length; i++) {
            Player sp = packetUpdatePlayers.getPlayers()[i];
            if (sp != null) {
                w.players[i] = (EntityPlayer) new EntityPlayer(sp.getUsername()).setPosition(sp.getPosition());
                w.players[i].setHealth(sp.getHealth());
            }
        }
    }

    @Override
    public void handlePacketUpdateNPC(PacketUpdateNPC packetUpdateNPC) {
        if (packetUpdateNPC.isDead()) {
            if (ScreenGame.getInstance().getWorld().entities.containsKey(packetUpdateNPC.getNPCName())) {
                ScreenGame.getInstance().getWorld().entities.remove(packetUpdateNPC.getNPCName());
            }
        } else {
            EntityPerson entityPerson = new EntityPerson(packetUpdateNPC.getNPCName(), packetUpdateNPC.getTextureId());
            entityPerson.setHealth(packetUpdateNPC.getHealth());
            entityPerson.setPosition(packetUpdateNPC.getPosition());
            ScreenGame.getInstance().getWorld().addEntity(entityPerson);
        }
    }

    @Override
    public void handlePacketLights(PacketLights packetLights) {
        ScreenGame.getInstance().getWorld().getShaderManager().setLights(packetLights.getLights());
    }

    @Override
    public void handlePacketMoveRequest(PacketMoveRequest packetMoveRequest) {
//        throw new UnsupportedOperationException("Cannot handle server-only packet");
    }

    @Override
    public void handlePacketEntityInteract(PacketEntityInteract packetEntityInteract) {
    }

    @Override
    public void handlePacketKick(PacketKick packetKick) {
        ScreenGame.getInstance().getConnection().close(packetKick.getReason());
    }

    @Override
    public void handleSentPacket(Packet packet) {
    }

    @Override
    public void handlePacketUpdateClientPlayer(PacketUpdateClientPlayer packetUpdateClientPlayer) {
        EntityPlayer playa = ScreenGame.getInstance().getWorld().getPlayer(packetUpdateClientPlayer.getPlayer().getUsername());
        if (playa != null) {
            double curhealth = playa.getHealth(), newhealth = packetUpdateClientPlayer.getPlayer().getHealth();

            playa.setPosition(packetUpdateClientPlayer.getPlayer().getPosition());
            playa.setHealth(packetUpdateClientPlayer.getPlayer().getHealth());

            if (newhealth != curhealth) {
                HealthSplash hs = new HealthSplash((int) (newhealth - curhealth), playa.getPosition());
                ScreenGame.getInstance().getGuiWorldOverlay().showHealthSplash(hs);
            }
        }
    }
}
