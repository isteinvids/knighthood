package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Emir
 */
public class ScreenGame extends ScreenAdapter {

    private static ScreenGame instance;
    private EntityPlayer player;
    private Connection connection;
    private World world;
    private GuiGameHUD guiGameHUD;
    private Inventory inventory;
    private OrthographicCamera camera;
    private GuiWorldOverlay guiWorldOverlay;
    private boolean initiated;
    private float offX, offY;
    /* ip & port */
    private final String ip;
    private final int port;
    private boolean loading = true;
    private boolean moving = false;

    public ScreenGame(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void show() {
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        ScreenGame.instance = this;
        this.camera = new OrthographicCamera(1, h / w);
        this.world = new World();
        this.guiGameHUD = new GuiGameHUD(this);
        this.player = new EntityPlayer(ScreenLogin.username);
        this.inventory = new Inventory(player);
        this.guiWorldOverlay = new GuiWorldOverlay();
        try {
            this.connection = new Connection(ip, port);
        } catch (Exception ex) {
            Logger.getLogger(ScreenGame.class.getName()).log(Level.SEVERE, null, ex);
            MainGame.getInstance().setScreenThreadSafe(Settings.DEBUG_MODE ? new ScreenLogin().notificate(ex.getMessage()) : new ScreenLobby().notificate(ex.getMessage()));
        }
    }

    @Override
    public void render(float delta) {
        int width = Gdx.graphics.getWidth(), height = Gdx.graphics.getHeight();

        /*
         if (pathfinderPositions != null) {
         if (pathfinderIndex < pathfinderPositions.length) {
         Position npos = pathfinderPositions[pathfinderIndex];
         if (tick1 > 3) {
         tick1 = 0;
         }
         if (tick0 > 32) {
         player.setPosition(npos);
         pathfinderIndex++;
         tick0 = 0;
         }
         tick0++;
         tick1++;
         } else {
         pathfinderPositions = null;
         }
         }
         */
        if (loading) {
            SpriteBatch batch = MainGame.getInstance().getSpriteBatch();
            BitmapFont font = MainGame.getInstance().getFont10();
            batch.begin();
            BitmapFont.TextBounds tb = font.getBounds("Loading world...");
            font.draw(batch, "Loading world...", (width / 2) - (tb.width / 2), (height / 2));
            batch.end();
        } else {
            offX = -(player.getPosition().getX() * getWorld().mapSize) - (235 / 2) - (player.getTexture().getWidth() / 2);
            offY = -(player.getPosition().getY() * getWorld().mapSize) + (170 / 2) - (player.getTexture().getHeight() / 2);

            this.camera.position.set(-offX, -offY, 0);
            this.camera.zoom = Gdx.graphics.getWidth();
            this.camera.update();

            SpriteBatch batch = MainGame.getInstance().getSpriteBatch();
            batch.begin();
            batch.setProjectionMatrix(camera.combined);
            world.render(batch, 10);
            guiWorldOverlay.render(batch);
            batch.end();
            this.guiGameHUD.render();
        }
    }

    public static ScreenGame getInstance() {
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }

    public World getWorld() {
        return world;
    }

    public EntityPlayer getPlayer() {
        return player;
    }

    public GuiGameHUD getGuiGameHUD() {
        return guiGameHUD;
    }

    public void setInitiated(boolean initiated) {
        this.initiated = initiated;
    }

    public boolean isInitiated() {
        return initiated;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public float getOffX() {
        return offX;
    }

    public float getOffY() {
        return offY;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public boolean isLoading() {
        return loading;
    }

    public GuiWorldOverlay getGuiWorldOverlay() {
        return guiWorldOverlay;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public boolean isMoving() {
        return moving;
    }

}
