package me.knighthood.client;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import java.io.File;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emir
 */
public class ImageManager {

    private final HashMap<String, File> fileMap = new HashMap();
    private final HashMap<String, Texture> textureMap = new HashMap();
    private boolean refresh = false;

    public void addServerImage(String id, File file) {
        this.fileMap.put(id, file);
    }

    public void createServerImage(String name, byte[] bytes) {
        System.out.println("getting image " + name);
        MainGame.getInstance().getServerImageManager().saveImage(name, bytes);
        this.fileMap.put(name, MainGame.getInstance().getServerImageManager().getImageFile(name));
    }

    public Texture getImage(String id) {
        return textureMap.get(id);
    }

    public void refreshImages() {
        this.refresh = true;
    }

    public void update() {
        try {
            int i = 0;
            List<String> notToUpdate = new ArrayList();
            for (Map.Entry<String, File> ent1 : this.fileMap.entrySet()) {
                for (Map.Entry<String, Texture> ent2 : this.textureMap.entrySet()) {
                    if (ent1.getKey().equals(ent2.getKey())) {
                        notToUpdate.add(ent1.getKey());
                    }
                }
                i++;
            }
            if (refresh) {
                this.textureMap.clear();
            }
            for (Map.Entry<String, File> ent1 : this.fileMap.entrySet()) {
                if ((!refresh && !notToUpdate.contains(ent1.getKey())) || refresh) {
                    File imgf = ent1.getValue();
                    System.out.println("loading texture: " + imgf);
                    this.textureMap.put(ent1.getKey(), new Texture(new FileHandle(imgf)));
                }
            }
            refresh = false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Texture generateTexture(int w, int h, int[] pixels) {
        Pixmap map = new Pixmap(w, h, Pixmap.Format.RGBA8888);
        int x = 0, y = 0;
        for (int i1 = 0; i1 < pixels.length; i1++) {
            x++;
            if (x >= w) {
                x = 0;
                y++;
            }
            map.setColor(convertABRGtoRBGA(pixels[i1]));
            map.drawPixel(x, y);
        }

        return new Texture(map);
    }

    public static int swapByte1And3(int inValue) {
        int swap = inValue & 0xFF;
        swap = swap << 16 | (inValue >>> 16 & 0xFF);
        return inValue & 0xFF00FF00 | swap;
    }

    public static int convertBRGtoRBG(int inColor) {
        return swapByte1And3(inColor);
    }

    public static int convertABRGtoRBGA(int inColor) {
        int swap = inColor >>> 24;
        inColor = inColor << 8;
        return inColor | swap;
    }

}
