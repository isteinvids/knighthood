package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import me.knighthood.protocol.PacketAction;
import me.knighthood.protocol.PacketChat;
import me.knighthood.protocol.PacketEntityInteract;
import me.knighthood.protocol.PacketMoveRequest;
import me.knighthood.protocol.Position;
import me.knighthood.protocol.Tile;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Emir
 */
public class GuiGameHUD {

    private final ScreenGame screenGame;
    private final GuiTextField txtChat;
    private boolean typing = false, pr = false;
    private final String[] msgs = new String[9];
    private final SpriteBatch miniMapBatch;
    private final GuiShop guiShop;
    private Texture textureChat;
    private Texture textureSide;
    private Texture textureMap;
    private String dialog = "null";
//  
    private String notif = "";
    private int notifCount = 0;

    public GuiGameHUD(ScreenGame screenGame) {
        this.screenGame = screenGame;
        this.txtChat = new GuiTextField(MainGame.getInstance().getFont10()).setText("Press enter to chat...");
        for (int i = 0; i < msgs.length; i++) {
            msgs[i] = "";
        }
        this.miniMapBatch = new SpriteBatch();
        this.textureChat = new Texture(Gdx.files.internal("data/chat.png"));
        this.textureSide = new Texture(Gdx.files.internal("data/side.png"));
        this.textureMap = new Texture(Gdx.files.internal("data/minimap.png"));
        this.guiShop = new GuiShop(screenGame);
    }

    public void resume() {
        this.textureChat = new Texture(Gdx.files.internal("data/chat.png"));
        this.textureSide = new Texture(Gdx.files.internal("data/side.png"));
        this.textureMap = new Texture(Gdx.files.internal("data/minimap.png"));
        this.getGuiShop().resume();
    }

    public ScreenGame getScreenGame() {
        return screenGame;
    }

    public void render() {
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();

        boolean doupdate = !getGuiShop().shopOpen();

        ShapeRenderer shapeRenderer = MainGame.getInstance().getShapeRenderer();
        SpriteBatch spriteBatch = MainGame.getInstance().getGuiSpriteBatch();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(new Color(0.4f, 0.1f, 0.05f, 1f));
        shapeRenderer.rect(0, 0, 575, 170);
        shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.rect(575, 0, 235, 600);
        shapeRenderer.end();

        spriteBatch.begin();
        spriteBatch.setColor(Color.WHITE);
        spriteBatch.draw(textureSide, 575, 0, 235, 600);
        spriteBatch.draw(textureChat, 0, 0, 576, 170);
        screenGame.getInventory().render();
//        /*
        if (dialog.equals("null")) {
            renderChat();
        } else {
            renderDialog();
        }

        getGuiShop().render(spriteBatch);
//        */
        spriteBatch.end();

//        float offx = -getScreenGame().getPlayer().getPosition().getX() + (Gdx.graphics.getWidth() / 2) - (235 / 2) - (64 / 2);
//        float offy = -getScreenGame().getPlayer().getPosition().getY() + (Gdx.graphics.getHeight() / 2) + (170 / 2) - (64 / 2);
        int mx = Gdx.graphics.getWidth() - 110 - (textureMap.getWidth() / 2), my = Gdx.graphics.getHeight() - 110 - (textureMap.getHeight() / 2);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(mx + 10, my + 10, textureMap.getWidth() - 20, textureMap.getHeight() - 20);
        shapeRenderer.end();

        spriteBatch.begin();
        /*
         Gdx.gl20.glPushMatrix();
         spriteBatch.begin();
         Gdx.gl20.glTranslatef(Gdx.graphics.getWidth() - 140, Gdx.graphics.getHeight() - 145, 0);
         Gdx.gl20.glScalef(0.1f, 0.1f, 0.1f);
         Gdx.gl20.glTranslatef(offx, offy, 0);
         ScreenGame.getInstance().getWorld().render(spriteBatch, false);
         spriteBatch.end();
         Gdx.gl20.glPopMatrix();*/

        MainGame.getInstance().getFont10().setColor(Color.WHITE);
        //576, 170
        double px = ScreenGame.getInstance().getPlayer().getPosition().getX(), py = ScreenGame.getInstance().getPlayer().getPosition().getY();

        Tile standingOn = getScreenGame().getPlayer().getTilePlayerIsStandingOn();
        String stnd = (standingOn != null ? "BX: " + standingOn.getX() + ", BY: " + standingOn.getY() : "BX: null, BY: null");

//        System.out.println(screenGame.getWorld().mapSize  );
//        TODO 575
//        System.out.println(screenGame.getPlayer().getPosition().getX());
        int size = screenGame.getWorld().mapSize;
        int lookx = (int) ((Gdx.input.getX() + (screenGame.getPlayer().getPosition().getX() * size) - 235 - 32) / screenGame.getWorld().mapSize);
        int looky = (int) ((Gdx.input.getY() - (screenGame.getPlayer().getPosition().getY() * size) - 170 - 32) / screenGame.getWorld().mapSize);
        if (looky < 0) {
            looky--;
        }

        //575 170
        boolean entityFlag = false;
        if (doupdate) {
            for (Entity ent : screenGame.getWorld().getEntities()) {
                int entx = ent.getPosition().getX();
                int enty = ent.getPosition().getY();
                if (!entityFlag) {
                    if (entx == lookx && enty == -looky) {
                        entityFlag = true;

                        int popx = Gdx.input.getX(), popy = Gdx.graphics.getHeight() + -Gdx.input.getY();
                        spriteBatch.end();
                        Gdx.gl.glEnable(GL11.GL_BLEND);
                        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                        shapeRenderer.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
                        shapeRenderer.rect(popx, popy, 220, 30);
                        shapeRenderer.end();
                        Gdx.gl.glDisable(GL11.GL_BLEND);
                        spriteBatch.begin();

                        String wr = ent.getName() + " - Interact";
                        if (wr.startsWith("--")) {
                            wr = "Interact";
                        }

                        MainGame.getInstance().getFont10().setColor(Color.WHITE);
                        MainGame.getInstance().getFont10().draw(spriteBatch, wr, popx + 20, popy + 20);

                        if (MainGame.getInstance().isMousePress(Input.Buttons.LEFT)) {
                            if (!screenGame.isMoving()) {
                                screenGame.getGuiWorldOverlay().setClickColor(Color.YELLOW);
                                screenGame.getGuiWorldOverlay().setClickPosition(new Position(lookx, -looky));
                            }
                            screenGame.getConnection().addPacketToQueue(new PacketEntityInteract(ent.getName()));
                        }
                    }
                }
            }
            if (!entityFlag) {
                if (Gdx.input.getX() < 575) {
                    if (Gdx.input.getY() < (Gdx.graphics.getHeight() + -170)) {
                        if (MainGame.getInstance().isMousePress(Input.Buttons.LEFT)) {
                            if (!screenGame.isMoving()) {
                                screenGame.getGuiWorldOverlay().setClickColor(Color.RED);
                                screenGame.getGuiWorldOverlay().setClickPosition(new Position(lookx, -looky));
                            }
                            screenGame.getConnection().addPacketToQueue(new PacketMoveRequest(lookx, -looky));
                        }
                    }
                }
            }
        }

        String[] strs = new String[]{"Looking at : " + lookx + ", " + looky, "X: " + px + ", Y:" + py, stnd, "FPS: " + Gdx.graphics.getFramesPerSecond()};
        for (int i = strs.length - 1; i >= 0; i--) {
            TextBounds tb = MainGame.getInstance().getFont10().getBounds(strs[i]);
            MainGame.getInstance().getFont10().setColor(Color.BLACK);
            MainGame.getInstance().getFont10().draw(spriteBatch, strs[i], 576 - tb.width - 4 + 1, 185 + (i * 20) - 1);
            MainGame.getInstance().getFont10().setColor(Color.YELLOW);
            MainGame.getInstance().getFont10().draw(spriteBatch, strs[i], 576 - tb.width - 4, 185 + (i * 20));
        }

        spriteBatch.end();

        OrthographicCamera camera = new OrthographicCamera(1, height / width);
        getMiniMapBatch().begin();
        camera.zoom = 6000;
        camera.position.set(-screenGame.getOffX(), -screenGame.getOffY(), 0);
        camera.position.add(-Gdx.graphics.getWidth() - 1500, -1370, 0);
        camera.update();
        getMiniMapBatch().setProjectionMatrix(camera.combined);
        screenGame.getWorld().render(getMiniMapBatch(), true, 20);
        getMiniMapBatch().end();

        if (!notif.isEmpty()) {
            Gdx.gl.glEnable(GL11.GL_BLEND);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
            shapeRenderer.rect((width / 2) - 150, (height / 2) + 45 - 50, 300, 100);
            shapeRenderer.end();
            Gdx.gl.glDisable(GL11.GL_BLEND);
        }

        spriteBatch.begin();
        spriteBatch.draw(textureMap, mx, my);
        if (!notif.isEmpty()) {
            MainGame.getInstance().getFont10().setColor(Color.WHITE);
            for (String ln : notif.split("\n")) {
                float notifWidth = MainGame.getInstance().getFont10().getBounds(ln).width;
                MainGame.getInstance().getFont10().draw(spriteBatch, ln, (width / 2) - (notifWidth / 2), (height / 2) + 50);
            }
            notifCount++;
        }
        spriteBatch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        boolean screenshotMouseover = false;
        shapeRenderer.setColor(Color.RED);
        if (MainGame.getInstance().isOn(0, height - 32, 32, 32)) {
            screenshotMouseover = true;
            shapeRenderer.setColor(Color.PINK);
            if (MainGame.getInstance().isMousePress(Input.Buttons.LEFT)) {
                ScreenshotFactory.saveScreenshot();
                notificate("Captured screenshot!");
            }
        }
        shapeRenderer.rect(0, height - 32, 32, 32);
        int popx1 = Gdx.input.getX(), popy1 = (Gdx.graphics.getHeight() + -Gdx.input.getY()) - 30;
        shapeRenderer.end();
        if (screenshotMouseover) {
            Gdx.gl.glEnable(GL11.GL_BLEND);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
            shapeRenderer.rect(popx1, popy1, 200, 30);
            shapeRenderer.end();
            Gdx.gl.glDisable(GL11.GL_BLEND);
        }

        spriteBatch.begin();
        if (screenshotMouseover) {
            String wr1 = "Take screenshot";
            MainGame.getInstance().getFont10().setColor(Color.WHITE);
            MainGame.getInstance().getFont10().draw(spriteBatch, wr1, popx1 + 20, popy1 + 20);
        }
        spriteBatch.end();

        if (notifCount > 150) {
            notif = "";
            notifCount = 0;
        }

        if (Gdx.input.isKeyPressed(Keys.ENTER) && !pr) {
            this.typing = !this.typing;
            if (!this.typing) {
                this.getScreenGame().getConnection().addPacketToQueue(new PacketChat(txtChat.getText()));
            }
            this.txtChat.setText(this.typing ? "" : "Press enter to chat...");
            pr = true;
        } else if (!Gdx.input.isKeyPressed(Keys.ENTER)) {
            pr = false;
        }
    }

    public void notificate(String msg) {
        this.notif = msg;
        this.notifCount = 0;
    }

    private void renderDialog() {
        //575
        SpriteBatch spriteBatch = MainGame.getInstance().getGuiSpriteBatch();
        BitmapFont font = MainGame.getInstance().getFont10();

        String op = dialog.split(":")[0].replaceAll("_", " ");
        String yes = dialog.split(":")[1].replaceAll("_", " ");
        String no = dialog.split(":")[2].replaceAll("_", " ");

        TextBounds tbop = font.getBounds(op);

        font.setColor(Color.BLACK);
        font.draw(spriteBatch, op, (575 / 2) - (tbop.width / 2) - 1, 120 - 1);
        font.setColor(Color.WHITE);
        font.draw(spriteBatch, op, (575 / 2) - (tbop.width / 2), 120);

        tbop = font.getBounds(yes);
        boolean ison = MainGame.getInstance().isOn((575 / 2) - 240, 50 - 15, (int) tbop.width, 20);
        if (ison && MainGame.getInstance().isMousePress(Input.Buttons.LEFT)) {
            this.dialog = "null";
            PacketAction ac = new PacketAction("dialog option 1");
            screenGame.getConnection().addPacketToQueue(ac);
        }
        font.setColor(Color.BLACK);
        font.draw(spriteBatch, yes, (575 / 2) - 240 - 1, 50 - 1);
        font.setColor(ison ? Color.RED : Color.WHITE);
        font.draw(spriteBatch, yes, (575 / 2) - 240, 50);

        tbop = font.getBounds(no);

        ison = MainGame.getInstance().isOn((int) ((575 / 2) - tbop.width + 240 - 3), 50 - 15, (int) tbop.width, 20);
        if (ison && MainGame.getInstance().isMousePress(Input.Buttons.LEFT)) {
            this.dialog = "null";
            PacketAction ac = new PacketAction("dialog option 0");
            screenGame.getConnection().addPacketToQueue(ac);
        }
        font.setColor(Color.BLACK);
        font.draw(spriteBatch, no, (575 / 2) - tbop.width + 240 - 1, 50 - 1);
        font.setColor(ison ? Color.RED : Color.WHITE);
        font.draw(spriteBatch, no, (575 / 2) - tbop.width + 240, 50);
    }

    private void renderChat() {
        SpriteBatch spriteBatch = MainGame.getInstance().getGuiSpriteBatch();
        for (int i = 0; i < this.msgs.length; i++) {
            if (msgs[i] != null) {
                Color col = Color.WHITE;
                String disp = msgs[i];
                if (msgs[i].length() > 2 && msgs[i].charAt(0) == '#' && Character.isDigit(msgs[i].charAt(1))) {
                    StringColour stringColour = StringColour.getFromValue(Integer.parseInt(msgs[i].charAt(1) + ""));
                    col = stringColour.getColor();
                    disp = msgs[i].substring(2);
                }
                MainGame.getInstance().getFont10().setColor(Color.BLACK);
                MainGame.getInstance().getFont10().draw(spriteBatch, disp, 10 + 1, (15 * i) + 40 - 1);
                MainGame.getInstance().getFont10().setColor(col);
                MainGame.getInstance().getFont10().draw(spriteBatch, disp, 10, (15 * i) + 40);
            }
        }
        spriteBatch.end();
        txtChat.setPosition(0, 0).setWidth(576).setFocus(typing).render();
        spriteBatch.begin();
    }

    public boolean isTyping() {
        return typing;
    }

    public void addMessage(String msg) {
        for (int i = msgs.length - 1; i > 0; i--) {
            msgs[i] = (i == 0 ? msgs[i] : msgs[i - 1]);
        }
        msgs[0] = msg;
    }

    public void setDialog(String dialog) {
        this.dialog = dialog;
    }

    public String getDialog() {
        return dialog;
    }

    public SpriteBatch getMiniMapBatch() {
        return miniMapBatch;
    }

    public GuiShop getGuiShop() {
        return guiShop;
    }

}
