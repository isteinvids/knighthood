package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import me.knighthood.protocol.Position;

/**
 *
 * @author Emir
 */
public class HealthSplash {

    public static Texture splashImage;
    public int difference;
    public Position position;

    public HealthSplash(int difference, Position position) {
        this.difference = difference;
        this.position = position;
    }

    public void render() {
        int offx = ScreenGame.getInstance().getPlayer().getPosition().getX(), offy = ScreenGame.getInstance().getPlayer().getPosition().getY();
        int size = ScreenGame.getInstance().getWorld().mapSize;

        SpriteBatch spriteBatch = MainGame.getInstance().getSpriteBatch();
        BitmapFont bitmapFont = MainGame.getInstance().getFont10();

        spriteBatch.setColor(difference > 0 ? Color.GREEN : Color.RED);
        spriteBatch.draw(splashImage, (offx * size), (offy * size));
        bitmapFont.setColor(Color.BLACK);
        bitmapFont.draw(spriteBatch, String.valueOf(difference), (offx * size) + 16 + 2, (offy * size) + 25 - 2);
        bitmapFont.setColor(Color.WHITE);
        bitmapFont.draw(spriteBatch, String.valueOf(difference), (offx * size) + 16, (offy * size) + 25);
    }

    public Color getColor() {
        return Color.WHITE;
    }
}
