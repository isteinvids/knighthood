package me.knighthood.client;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Emir
 */
public class ThreadLobby extends Thread {

    private boolean looping;
    public static final String URL = "http://knighthood.isteinvids.co.uk/lobby/";

    public ThreadLobby() {
        try {
            String login = Utils.getTextFromUrl(URL + "login.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);

            System.out.println("ScreenLogin.username = " + ScreenLogin.username);
            System.out.println("ScreenLogin.sessionid = " + ScreenLogin.sessionid);
            System.out.println("url = " + URL + "login.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);
            System.out.println("login = " + login);

            this.looping = true;
        } catch (IOException ex) {
            Logger.getLogger(ThreadLobby.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        while (isLooping()) {
            try {
                if (ScreenLobby.command.equals("servers")) {
                    String rawservers = Utils.getTextFromUrl(URL + "serverlist.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);
                    ScreenLobby.servers = rawservers.split("\n");
                }
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadLobby.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ThreadLobby.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean isLooping() {
        return looping;
    }

    public void setLooping(boolean looping) {
        this.looping = looping;
    }

}
