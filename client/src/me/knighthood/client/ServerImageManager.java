package me.knighthood.client;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Emir
 */
public class ServerImageManager {

    public static final File cacheDir = new File(System.getProperty("user.home") + File.separator + ".knighthoodcache");;

    public ServerImageManager() {
        this.cacheDir.mkdirs();
    }

    public void saveImage(String id, byte[] bytes) {
        try {
            System.out.println("saving image " + id);
            File img = new File(cacheDir, id);
            
//            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(bytes);
            FileOutputStream fileOutputStream = new FileOutputStream(img);
            fileOutputStream.write(bytes);
            fileOutputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(ServerImageManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public File getImageFile(String id) {
        File img = new File(cacheDir, id);
        return img;
    }

    public BufferedImage getImage(String id) {
        try {
            return ImageIO.read(new File(cacheDir, id));
        } catch (IOException ex) {
            Logger.getLogger(ServerImageManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean imageExists(String id) {
        return this.imageExists(id, "null");
    }

    public boolean imageExists(String id, String md5) {
        try {
            File img = new File(cacheDir, id);
            if (md5 == null || md5.equals("null")) {
                return img.exists();
            }
            if (!img.exists()) {
                return false;
            }
            String filemd5 = getMD5Checksum(img);
            return filemd5.equals(md5);
        } catch (Exception ex) {
            Logger.getLogger(ServerImageManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public File getCacheDir() {
        return cacheDir;
    }

    private String getMD5Checksum(File filename) throws Exception {
        InputStream fis = new FileInputStream(filename);
        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        byte[] b = complete.digest();
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

}
