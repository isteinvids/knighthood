package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static me.knighthood.client.ThreadLobby.URL;
import me.knighthood.protocol.SharedSettings;

/**
 *
 * @author Emir
 */
public class ScreenLobby extends ScreenAdapter {

    public static Texture title, background;
    private GuiButton btnWorldSelect, btnLogout0, btnLogout1;
    private int state = 0, prevState = -1;
    private String notificate = "null";
    /* Properties used by the networking thread */
    public static String command = "null";
//    public static String[] msgs;
    public static String[] servers;

    @Override
    public void show() {
        BitmapFont font = MainGame.getInstance().getFont10();
        btnLogout0 = new GuiButton(font, "Logout");
        btnLogout1 = new GuiButton(font, "Logout");
        btnWorldSelect = new GuiButton(font, "Select world");

        try {
            String login = Utils.getTextFromUrl(URL + "login.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);

            if (!login.startsWith("true:")) {
                ScreenLogin screenLogin = new ScreenLogin();
                screenLogin.notificate("Could not login to lobby");
                MainGame.getInstance().setScreen(screenLogin);
            }

            System.out.println("ScreenLogin.username = " + ScreenLogin.username);
            System.out.println("ScreenLogin.sessionid = " + ScreenLogin.sessionid);
            System.out.println("url = " + URL + "login.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);
            System.out.println("login = " + login);
        } catch (IOException ex) {
            Logger.getLogger(ScreenLobby.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void render(float delta) {
        if (prevState != state) {
            //init stuff in new state
            if (state == 0) {
                updateServers();
            }
            prevState = state;
        }

        SpriteBatch spriteBatch = MainGame.getInstance().getGuiSpriteBatch();
        BitmapFont font = MainGame.getInstance().getFont10();

        spriteBatch.begin();
        spriteBatch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        spriteBatch.draw(title, (Gdx.graphics.getWidth() / 2) - (title.getWidth() / 2), Gdx.graphics.getHeight() - title.getHeight());

        if (!this.notificate.equalsIgnoreCase("null")) {
            TextBounds tbNotif = MainGame.getInstance().getFont10().getBounds(notificate);
            boolean ison = MainGame.getInstance().isOn((int) ((Gdx.graphics.getWidth() / 2) - (tbNotif.width / 2)), (int) (Gdx.graphics.getHeight() / 2 + 20) - 15, (int) tbNotif.width, 25);
            MainGame.getInstance().getFont10().setColor(Color.BLACK);
            MainGame.getInstance().getFont10().draw(MainGame.getInstance().getGuiSpriteBatch(), notificate, (Gdx.graphics.getWidth() / 2) - (tbNotif.width / 2) - 2, Gdx.graphics.getHeight() / 2 + 20 - 2);
            MainGame.getInstance().getFont10().setColor(ison ? Color.RED : Color.WHITE);
            MainGame.getInstance().getFont10().draw(MainGame.getInstance().getGuiSpriteBatch(), notificate, (Gdx.graphics.getWidth() / 2) - (tbNotif.width / 2), Gdx.graphics.getHeight() / 2 + 20);
            MainGame.getInstance().getGuiSpriteBatch().end();

            if ((ison && Gdx.input.isTouched()) || Gdx.input.isKeyPressed(Keys.SPACE)) {
                state = 1;
                this.notificate = "null";
            }
            return;
        }

        if (state == 0) {
            for (int i = 0; i < servers.length; i++) {
//                System.out.println("servers[i] : "+servers[i]);
                String name = servers[i].contains(":") ? servers[i].split(":")[2] : servers[i];

                BitmapFont.TextBounds tb = font.getBounds(name);
                float sx = (Gdx.graphics.getWidth() / 2) - (tb.width / 2);
                float sy = Gdx.graphics.getHeight() - 230 + (i * 20) - (servers.length * 20)-20;
                boolean ison = MainGame.getInstance().isOn(sx, sy - 15, tb.width, 20);

                font.setColor(ison ? Color.RED : Color.WHITE);
                font.draw(spriteBatch, name, sx, sy);

                if (ison && Gdx.input.isTouched()) {
                    if (servers[i].contains(":")) {
                        String ip = servers[i].split(":")[0];
                        int port = Integer.parseInt(servers[i].split(":")[1]);
                        MainGame.getInstance().setScreen(new ScreenGame(ip, port));
                    }
                }
            }
        } else if (state == 1) {
            drawCenter(spriteBatch, font, "When you're done playing,", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 270);
            drawCenter(spriteBatch, font, "press the button below to", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 290);
            drawCenter(spriteBatch, font, "safely log out from the lobby.", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 310);
            drawCenter(spriteBatch, font, "Come back later!", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 330);
        }
        spriteBatch.end();

        btnWorldSelect.setPosition((Gdx.graphics.getWidth() / 2) - 75 - (GuiButton.txtButton.getWidth()), Gdx.graphics.getHeight() - GuiButton.txtButton.getHeight() - 170).render();
        btnLogout0.setPosition((Gdx.graphics.getWidth() / 2) + 75, Gdx.graphics.getHeight() - GuiButton.txtButton.getHeight() - 170).render();
        if (state == 1) {
            btnLogout1.setDown(new Color(1f, 0.3f, 0.2f, 1f)).setNormal(new Color(0.6f, 0.2f, 0.1f, 1f)).setPosition((Gdx.graphics.getWidth() / 2) - (GuiButton.txtButton.getWidth() / 2), Gdx.graphics.getHeight() - GuiButton.txtButton.getHeight() - 360).render();
            if (btnLogout1.isPressed()) {
                MainGame.getInstance().setScreen(new ScreenLogin());
            }
        }

        if (btnWorldSelect.isPressed()) {
            state = 0;
        }
        if (btnLogout0.isPressed()) {
            state = 1;
        }
    }

    public void drawCenter(SpriteBatch batch, BitmapFont font, String text, int x, int y) {
        font.draw(batch, text, x - (font.getBounds(text).width / 2), y);
    }

    public void updateServers() {
//        try {
        ScreenLobby.servers = new String[]{"Loading servers..."};
        Net.HttpRequest httpRequest = new Net.HttpRequest(Net.HttpMethods.GET);
        httpRequest.setHeader("User-Agent", SharedSettings.USER_AGENT);

        httpRequest.setUrl(URL + "serverlist.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);
        Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {

            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                ScreenLobby.servers = httpResponse.getResultAsString().split("\n");
            }

            @Override
            public void failed(Throwable t) {
                Logger.getLogger(ScreenLobby.class.getName()).log(Level.SEVERE, null, t);
            }

            @Override
            public void cancelled() {
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
//            String rawservers = Utils.getTextFromUrl(URL + "serverlist.php?user=" + ScreenLogin.username + "&session=" + ScreenLogin.sessionid);
//        } catch (IOException ex) {
//        }
    }

    public String getNotificate() {
        return notificate;
    }

    public ScreenLobby notificate(String notificate) {
        this.notificate = notificate;
        return this;
    }

}
