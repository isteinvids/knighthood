package me.knighthood.client;

/**
 *
 * @author Emir
 */
public class Settings {

    public static final boolean DEBUG_MODE = true;
    public static final String BUILD = "002";
    public static final String LOGIN_PAGE = "http://knighthood.isteinvids.co.uk/knighthoodv2newlogin.php";
    public static final String BUY_MEMBERSHIP = "http://knighthood.isteinvids.co.uk/buyknighthood";
    public static final String REGISTER_PAGE = "http://knighthood.isteinvids.co.uk/signup";
    public static final String WEBSITE = "http://knighthood.isteinvids.co.uk/";
    public static final String BUG_THREAD = "http://knighthood.isteinvids.co.uk/forum";
    public static final String FORUMS = "http://knighthood.isteinvids.co.uk/forum/";
    public static final String RESOURCE_LINK = "http://knighthood.isteinvids.co.uk/jars/";
}
