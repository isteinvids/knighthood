package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import me.knighthood.protocol.Position;

/**
 *
 * @author Emir
 */
public class GuiWorldOverlay {

    private final Texture moving_cursor;
    private final Map<HealthSplash, Integer> healthSplashes = new ConcurrentHashMap<HealthSplash, Integer>();
    private Position clickPosition;
    private double clickCounter = 0;
    private Color clickColor = Color.WHITE;

    public GuiWorldOverlay() {
        this.moving_cursor = new Texture(Gdx.files.internal("data/moving_cursor.png"));
        HealthSplash.splashImage = new Texture(Gdx.files.internal("data/splash.png"));
    }

    public void render(SpriteBatch batch) {
        World world = ScreenGame.getInstance().getWorld();
        int size = world.mapSize;
        float fs = 2.0f;

        if (clickPosition != null) {
            float sz = (float) ((fs - clickCounter) * size);
            batch.setColor(new Color(clickColor.a, clickColor.g, clickColor.b, (float) clickCounter));
            batch.draw(moving_cursor, (clickPosition.getX() * size) - (sz / 2) + (size / 2), (clickPosition.getY() * size) - (sz / 2) + (size / 2), sz, sz);
            batch.setColor(Color.WHITE);
            clickCounter -= 0.08;
            if (clickCounter < 0) {
                clickPosition = null;
            }
        }

        for (Map.Entry<HealthSplash, Integer> entry : healthSplashes.entrySet()) {
            int val = entry.getValue();
            entry.getKey().render();
            entry.setValue(val + 1);
            if (val > 30) {
                healthSplashes.remove(entry.getKey());
            }
        }
    }

    public void showHealthSplash(HealthSplash hs) {
        healthSplashes.put(hs, 0);
    }

    public double getClickCounter() {
        return clickCounter;
    }

    public void setClickCounter(double clickCounter) {
        this.clickCounter = clickCounter;
    }

    public Position getClickPosition() {
        return clickPosition;
    }

    public void setClickColor(Color clickColor) {
        this.clickColor = clickColor;
    }

    public Color getClickColor() {
        return clickColor;
    }

    public void setClickPosition(Position clickPosition) {
        this.clickCounter = 2.0;
        this.clickPosition = clickPosition;
    }

}
