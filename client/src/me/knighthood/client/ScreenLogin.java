package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Emir
 */
public class ScreenLogin extends ScreenAdapter {

    private Texture txtTitle;
    private Texture txtBackground;
    private GuiTextField guiUsername;
    private GuiTextField guiPassword;
    private GuiButton guiLogin, guiRegister, guiExit, guiBug;
    private String notificate = "null";

    public static String username;
    public static String sessionid;

    @Override
    public void show() {
        this.txtTitle = new Texture(Gdx.files.internal("data/title.png"));
        this.txtBackground = new Texture(Gdx.files.internal("data/poop.png"));
        this.guiUsername = new GuiTextField(MainGame.getInstance().getFont10());
        this.guiPassword = new GuiTextField(MainGame.getInstance().getFont10()).setPassword(true);
        this.guiLogin = new GuiButton(MainGame.getInstance().getFont10(), "Login to lobby");
        this.guiRegister = new GuiButton(MainGame.getInstance().getFont10(), "Register");
        this.guiExit = new GuiButton(MainGame.getInstance().getFont10(), "Exit game");
        this.guiBug = new GuiButton(MainGame.getInstance().getFont10(), "Found a bug?");
        this.guiUsername.setText(getEmail());
    }

    @Override
    public void render(float delta) {
        MainGame.getInstance().getGuiSpriteBatch().begin();
        MainGame.getInstance().getGuiSpriteBatch().draw(txtBackground, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        MainGame.getInstance().getGuiSpriteBatch().draw(txtTitle, (Gdx.graphics.getWidth() / 2) - (txtTitle.getWidth() / 2), Gdx.graphics.getHeight() - txtTitle.getHeight());

        if (this.notificate != null) {
            if (!this.notificate.equalsIgnoreCase("null")) {
                TextBounds tbNotif = MainGame.getInstance().getFont10().getBounds(notificate);
                boolean ison = MainGame.getInstance().isOn((int) ((Gdx.graphics.getWidth() / 2) - (tbNotif.width / 2)), (int) (Gdx.graphics.getHeight() / 2 + 20) - 15, (int) tbNotif.width, 25);
                MainGame.getInstance().getFont10().setColor(Color.BLACK);
                MainGame.getInstance().getFont10().draw(MainGame.getInstance().getGuiSpriteBatch(), notificate, (Gdx.graphics.getWidth() / 2) - (tbNotif.width / 2) - 2, Gdx.graphics.getHeight() / 2 + 20 - 2);
                MainGame.getInstance().getFont10().setColor(ison ? Color.RED : Color.WHITE);
                MainGame.getInstance().getFont10().draw(MainGame.getInstance().getGuiSpriteBatch(), notificate, (Gdx.graphics.getWidth() / 2) - (tbNotif.width / 2), Gdx.graphics.getHeight() / 2 + 20);
                MainGame.getInstance().getGuiSpriteBatch().end();

                if ((ison && Gdx.input.isTouched()) || Gdx.input.isKeyPressed(Keys.SPACE)) {
                    this.notificate = "null";
                }
                return;
            }
        }

        TextBounds tbEmail = MainGame.getInstance().getFont10().getBounds("Email:");
        MainGame.getInstance().getFont10().draw(MainGame.getInstance().getGuiSpriteBatch(), "Email:", (Gdx.graphics.getWidth() / 2) - (tbEmail.width / 2), Gdx.graphics.getHeight() - 195);
        TextBounds tbPass = MainGame.getInstance().getFont10().getBounds("Password:");
        MainGame.getInstance().getFont10().draw(MainGame.getInstance().getGuiSpriteBatch(), "Password:", (Gdx.graphics.getWidth() / 2) - (tbPass.width / 2), Gdx.graphics.getHeight() - 295);
        MainGame.getInstance().getGuiSpriteBatch().end();

        this.guiUsername.setWidth(400).setPosition((Gdx.graphics.getWidth() / 2) - (guiUsername.getWidth() / 2), Gdx.graphics.getHeight() - 240).render();
        this.guiPassword.setWidth(400).setPosition((Gdx.graphics.getWidth() / 2) - (guiPassword.getWidth() / 2), Gdx.graphics.getHeight() - 340).render();
        this.guiLogin.setPosition((Gdx.graphics.getWidth() / 2) - (GuiButton.txtButton.getWidth() / 2) - 150, Gdx.graphics.getHeight() - 460).render();
        this.guiRegister.setPosition((Gdx.graphics.getWidth() / 2) - (GuiButton.txtButton.getWidth() / 2) + 150, Gdx.graphics.getHeight() - 460).render();
        this.guiExit.setPosition((Gdx.graphics.getWidth() / 2) - (GuiButton.txtButton.getWidth() / 2) - 150, Gdx.graphics.getHeight() - 560).render();
        this.guiBug.setPosition((Gdx.graphics.getWidth() / 2) - (GuiButton.txtButton.getWidth() / 2) + 150, Gdx.graphics.getHeight() - 560).render();

        if (this.guiLogin.isPressed()) {
            String login = login(guiUsername.getText(), guiPassword.getText());
            if (login.contains(":")) {
                setEmail(guiUsername.getText());
                ScreenLogin.username = login.split(":")[0];
                ScreenLogin.sessionid = login.split(":")[1];
//                MainGame.getInstance().setScreen(new ScreenGame());
                ScreenLobby.background = txtBackground;
                ScreenLobby.title = txtTitle;
                MainGame.getInstance().setScreen(new ScreenLobby());
            } else {
                notificate(login);
            }
        }
        if (this.guiRegister.isPressed()) {
            Utils.openWebpage(Settings.REGISTER_PAGE);
        }
        if (this.guiBug.isPressed()) {
            Utils.openWebpage(Settings.BUG_THREAD);
        }
        if (this.guiExit.isPressed()) {
            System.exit(0);
        }
    }

    public String login(String email, String pass) {
        try {
            String salt = Utils.getTextFromUrl(Settings.LOGIN_PAGE + "?getsalt=" + email);
            String encryptedPass = Utils.encryptSHA256(pass, salt);
            String finallogin = Utils.getTextFromUrl(Settings.LOGIN_PAGE + "?email=" + email + "&password=" + encryptedPass);
            if (finallogin.contains("success:")) {
                String user = finallogin.split(":")[1];
                String session = finallogin.split(":")[2];
                finallogin = user + ":" + session;
//                return finallogin;
            } else {
                if (finallogin.contains("null")) {
                    finallogin = "Could not connect to login server";
                }
            }
            return finallogin;
        } catch (IOException ex) {
            Logger.getLogger(ScreenLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Could not connect to login server";
    }

    public ScreenLogin notificate(String msg) {
        this.notificate = msg;
        return this;
    }

    public String getEmail() {
        String email = "";
        try {
            File lastlogin = new File(ServerImageManager.cacheDir, "lastlogin");
            if (lastlogin.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(lastlogin));
                email = br.readLine();
                br.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(ScreenLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return email;
    }

    public void setEmail(String email) {
        try {
            File lastlogin = new File(ServerImageManager.cacheDir, "lastlogin");
            PrintWriter out = new PrintWriter(lastlogin);
            out.println(email);
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScreenLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
