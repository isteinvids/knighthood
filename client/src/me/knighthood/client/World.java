package me.knighthood.client;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.HashMap;
import java.util.Map;
import me.knighthood.protocol.Player;
import me.knighthood.protocol.Tile;

/**
 *
 * @author Emir
 */
public class World {

    public int mapSize = 64;
    public Tile[] tiles;
    public EntityPlayer[] players;
    public Map<String, Entity> entities = new HashMap();
    private final ShaderManager shaderManager;

    public World() {
        this.shaderManager = new ShaderManager();
    }

    public void render(SpriteBatch batch, int rendertilesize) {
        this.render(batch, true, rendertilesize);
    }

    private void renderTiles(SpriteBatch batch, int sx, int sy, int ex, int ey) {
        if (this.tiles != null) {
            getShaderManager().render(batch);
            for (int i = 0; i < 32; i++) {
                for (Tile t : this.tiles) {
                    if (t != null) {
                        if (t.getX() > sx && t.getX() < ex && t.getY() > sy && t.getY() < ey) {
                            if (t.getLayer() == i) {
                                Texture texture = MainGame.getInstance().getImageManager().getImage(t.getTileid());
                                if (texture != null) {
                                    int x = t.getX() * this.mapSize;
                                    int y = t.getY() * this.mapSize;

                                    float srcX = t.getTilex();
                                    float srcY = t.getTiley();
                                    float srcWidth = this.mapSize;
                                    float srcHeight = this.mapSize;

                                    float u = srcX / texture.getWidth();
                                    float v = srcY / texture.getHeight();
                                    float u2 = (srcX + t.getTileWidth()) / texture.getWidth();
                                    float v2 = (srcY + t.getTileHeight()) / texture.getHeight();

                                    batch.draw(texture, x, y, srcWidth, srcHeight, u, v2, u2, v);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void render(SpriteBatch batch, boolean drawPlayers, int rendertilesize) {
        try {
            EntityPlayer playa = ScreenGame.getInstance().getPlayer();
            int stx = playa.getPosition().getX() - rendertilesize;
            int sty = playa.getPosition().getY() - rendertilesize;
            int enx = playa.getPosition().getX() + rendertilesize;
            int eny = playa.getPosition().getY() + rendertilesize;
            renderTiles(batch, stx, sty, enx, eny);
            if (this.entities != null && drawPlayers) {
                for (Map.Entry<String, Entity> entry : entities.entrySet()) {
                    if (entry.getValue() != null) {
                        if (entry.getValue().getPosition().getX() > stx && entry.getValue().getPosition().getX() < enx) {
                            if (entry.getValue().getPosition().getY() > sty && entry.getValue().getPosition().getY() < eny) {
                                entry.getValue().render(batch, false);
                            }
                        }
                    }
                }
            }
            if (this.players != null && drawPlayers) {
                for (EntityPlayer p : this.players) {
                    if (p != null) {
                        if (p.getPosition().getX() > stx && p.getPosition().getX() < enx) {
                            if (p.getPosition().getY() > sty && p.getPosition().getY() < eny) //                    if(p.getPosition().getX())
                            {
                                p.render(batch, false);
                            }
                        }
                    }
                }
            }
            if (drawPlayers) {
                ScreenGame.getInstance().getPlayer().render(batch, false);
                getShaderManager().post();
            }
            if (this.entities != null && drawPlayers) {
                for (Map.Entry<String, Entity> entry : entities.entrySet()) {
                    if (entry.getValue() != null) {
                        if (entry.getValue().getPosition().getX() > stx && entry.getValue().getPosition().getX() < enx) {
                            if (entry.getValue().getPosition().getY() > sty && entry.getValue().getPosition().getY() < eny) {
                                entry.getValue().render(batch, true);
                            }
                        }
                    }
                }
            }
            if (this.players != null && drawPlayers) {
                for (EntityPlayer p : this.players) {
                    if (p != null) {
                        if (p.getPosition().getX() > stx && p.getPosition().getX() < enx) {
                            if (p.getPosition().getY() > sty && p.getPosition().getY() < eny) {
                                p.render(batch, true);
                            }
                        }
                    }
                }
            }
            if (drawPlayers) {
                ScreenGame.getInstance().getPlayer().render(batch, true);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public EntityPlayer getPlayer(String name) {
        if (ScreenGame.getInstance().getPlayer().getName().equals(name)) {
            return ScreenGame.getInstance().getPlayer();
        }
        if (players != null) {
            for (EntityPlayer player : players) {
                if (player != null && player.getName().equals(name)) {
                    return player;
                }
            }
        }
        return null;
    }

    public void addEntity(Entity entity) {
        if (!(entity instanceof EntityPlayer)) {
            this.entities.put(entity.getName(), entity);
        }
    }

    public ShaderManager getShaderManager() {
        return shaderManager;
    }

    public Map<String, Entity> getEntityMap() {
        return entities;
    }

    public Entity[] getEntities() {
        return entities.values().toArray(new Entity[0]);
    }

}
