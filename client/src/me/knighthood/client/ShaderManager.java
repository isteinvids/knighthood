package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import me.knighthood.protocol.Light;

/**
 *
 * @author Emir
 */
public final class ShaderManager {

    public enum ShaderSelection {

        Default,
        Ambient,
        Light,
        Final
    };
    private final Texture light;
    private final FrameBuffer fbo;
    //out different shaders. currentShader is just a pointer to the 4 others
    private ShaderSelection shaderSelection = ShaderSelection.Default;
    private ShaderProgram currentShader;
    private final ShaderProgram defaultShader;
    private final ShaderProgram ambientShader;
    private final ShaderProgram lightShader;
    private final ShaderProgram finalShader;
    private Light[] lights;
    //values passed to the shader
    public float ambientIntensity = .1f;
    public Vector3 ambientColor = new Vector3(1f, 1f, 1f); //0.3f, 0.3f, 0.7f
    //read our shader files
    final String vertexShader = Gdx.files.internal("data/lights/vertexShader.glsl").readString();
    final String defaultPixelShader = Gdx.files.internal("data/lights/defaultPixelShader.glsl").readString();
    final String ambientPixelShader = Gdx.files.internal("data/lights/ambientPixelShader.glsl").readString();
    final String lightPixelShader = Gdx.files.internal("data/lights/lightPixelShader.glsl").readString();
    final String finalPixelShader = Gdx.files.internal("data/lights/pixelShader.glsl").readString();
    //

    public ShaderManager() {
        ShaderProgram.pedantic = false;
        defaultShader = new ShaderProgram(vertexShader, defaultPixelShader);
        ambientShader = new ShaderProgram(vertexShader, ambientPixelShader);
        lightShader = new ShaderProgram(vertexShader, lightPixelShader);
        finalShader = new ShaderProgram(vertexShader, finalPixelShader);
//        batc(defaultShader);
        refreshShaders();
        int width = Gdx.graphics.getWidth(), height = Gdx.graphics.getHeight();
        fbo = new FrameBuffer(Format.RGBA8888, width, height, false);
        light = new Texture("data/lights/light.png");
    }

    public void refreshShaders() {
        ambientShader.begin();
        ambientShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y,
                ambientColor.z, ambientIntensity);
        ambientShader.end();

        lightShader.begin();
        lightShader.setUniformi("u_lightmap", 1);
        lightShader.end();

        finalShader.begin();
        finalShader.setUniformi("u_lightmap", 1);
        finalShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y, ambientColor.z, ambientIntensity);
        finalShader.end();

        int width = Gdx.graphics.getWidth(), height = Gdx.graphics.getHeight();

        lightShader.begin();
        lightShader.setUniformf("resolution", width, height);
        lightShader.end();

        finalShader.begin();
        finalShader.setUniformf("resolution", width, height);
        finalShader.end();
    }

    public void render(SpriteBatch batch) {
        refreshShaders();

//        SpriteBatch batch = MainGame.getInstance().getSpriteBatch();

        fbo.begin();
        batch.setShader(defaultShader);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (lights != null) {
            for (Light l : lights) {
                if (l != null) {
                    int offx = l.getX(), offy = l.getY();
                    int mapsize = ScreenGame.getInstance().getWorld().mapSize;
                    float size = l.getScale() * (mapsize * 2);
                    batch.setColor(new Color(l.getRed(), l.getGreen(), l.getBlue(), l.getAlpha()));
                    batch.draw(light, (offx * mapsize) - (size / 2) + mapsize, (offy * mapsize) - (size / 2) + mapsize, size, size);
                }
            }
        }

        batch.setColor(Color.WHITE);
        batch.end();
        fbo.end();

        batch.setShader(finalShader);
        batch.begin();
        fbo.getColorBufferTexture().bind(1);
        light.bind(0);
    }

    public void post() {
//        setShader(ShaderSelection.Final);
    }

    public void drawLight(Light... lts) {
    }

    public Light[] getLights() {
        return lights;
    }

    public void setLights(Light[] lights) {
        this.lights = lights;
    }
}
