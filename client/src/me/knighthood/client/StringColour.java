package me.knighthood.client;

import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Emir
 */
public enum StringColour {

    WHITE(0, Color.WHITE),
    BLUE(1, Color.BLUE),
    RED(2, Color.RED),
    GREEN(3, Color.GREEN),
    YELLOW(4, Color.YELLOW),
    PINK(5, Color.PINK),
    CYAN(6, Color.CYAN),
    LIGHT_GRAY(7, Color.LIGHT_GRAY);

    private final int value;
    private final Color color;

    private StringColour(int value, Color color) {
        this.value = value;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public int getValue() {
        return value;
    }

    public static StringColour getFromValue(int value) {
        for (StringColour sc : values()) {
            if (sc.getValue() == value) {
                return sc;
            }
        }
        return null;
    }
}
