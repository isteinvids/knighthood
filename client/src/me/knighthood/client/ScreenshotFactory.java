package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.utils.ScreenUtils;
import java.io.File;
import java.nio.ByteBuffer;

/**
 *
 * @author Emir
 */
public class ScreenshotFactory {

    private static int counter = 1;

    public static File saveScreenshot() {
        try {
            File parent = new File(ServerImageManager.cacheDir, "screenshots");
            parent.mkdirs();

            FileHandle fh;
            do {
                fh = new FileHandle(parent + File.separator + "screenshot" + counter++ + ".png");
//                if(!fh.)
            } while (fh.exists());
            Pixmap pixmap = getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
            PixmapIO.writePNG(fh, pixmap);
            pixmap.dispose();
            
            return fh.file();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static Pixmap getScreenshot(int x, int y, int w, int h, boolean yDown) {
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);

        if (yDown) {
            // Flip the pixmap upside down
            ByteBuffer pixels = pixmap.getPixels();
            int numBytes = w * h * 4;
            byte[] lines = new byte[numBytes];
            int numBytesPerLine = w * 4;
            for (int i = 0; i < h; i++) {
                pixels.position((h - i - 1) * numBytesPerLine);
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
            }
            pixels.clear();
            pixels.put(lines);
        }

        return pixmap;
    }
}
