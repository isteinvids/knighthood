package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.text.NumberFormat;
import java.util.Locale;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.protocol.Item;
import me.knighthood.protocol.PacketAction;

/**
 *
 * @author Emir
 */
public class Inventory {

    private static final int IMAGE_SIZE = 32;
    private static final int inventory_item_width = 5;
    private static final int inventory_item_space = IMAGE_SIZE + 16;
    private Item[] items;
    private InventoryItem[] inventoryItems;
    private final EntityPlayer player;
    private final Texture unknownTexture;
    /* RIGHT CLICK STUFF */
    private InventoryItem chosen = null;
    private boolean rightClicked = false;
    private int rightClickX = 0, rightClickY = 0;

    public Inventory(EntityPlayer player) {
        this.player = player;
        this.unknownTexture = new Texture(Gdx.files.internal("data/unknown.png"));
    }

    public Texture getUnknownTexture() {
        return unknownTexture;
    }

    public void render() {
        update();
        SpriteBatch spriteBatch = MainGame.getInstance().getGuiSpriteBatch();
        BitmapFont bitmapFont = MainGame.getInstance().getFont10();
        boolean tipbox = false;
        String str = "null";
        if (getInventoryItems() != null) {
            for (int i = 0; i < getInventoryItems().length; i++) {
                Item item = getItem(getInventoryItems()[i].getItemName());

                int counterPosY = (i / inventory_item_width) * ((inventory_item_space / 2) + 15);
                int counterPosX = ((counterPosY - ((((inventory_item_space / 2) + 15)) * (inventory_item_width/**/)) - ((inventory_item_space / 2) + 15) - (((inventory_item_space / 2) + 15) * ((i / inventory_item_width) - 1))) * (i / inventory_item_width)) + (i * ((inventory_item_space / 2) + 15));

                int x = counterPosX + (IMAGE_SIZE / 2) + (Gdx.graphics.getWidth() - (IMAGE_SIZE * inventory_item_width)) - inventory_item_space - (IMAGE_SIZE / 2);
                int y = -counterPosY - (IMAGE_SIZE / 2) + 310;

                if (item != null) {
                    String wondname = firstLetterToCaps(item.getName());
                    Texture text = MainGame.getInstance().getImageManager().getImage(item.getImageid());
                    if (text == null) {
                        text = unknownTexture;
                    }

                    String amount2 = getIntegerAmount(getInventoryItems()[i].getCount());
                    if (MainGame.getInstance().isOn(x - inventory_item_width, y - inventory_item_width, IMAGE_SIZE + (inventory_item_width * 2), IMAGE_SIZE + (inventory_item_width * 2)) && !tipbox) {
                        tipbox = true;
                        str = wondname;
                        if (item.isStackable()) {
                            str += " - " + NumberFormat.getNumberInstance(Locale.US).format(getInventoryItems()[i].getCount());
                        }
                    }

                    spriteBatch.draw(text, x, y, IMAGE_SIZE, IMAGE_SIZE);
                    if (item.isStackable()) {
                        bitmapFont.setColor(Color.BLACK);
                        bitmapFont.draw(spriteBatch, amount2, x + 2, y + IMAGE_SIZE - 2);
                        bitmapFont.setColor(getIntegerColour(amount2));
                        bitmapFont.draw(spriteBatch, amount2, x, y + IMAGE_SIZE);
                    }
                } else {
                    spriteBatch.draw(unknownTexture, x, y, IMAGE_SIZE, IMAGE_SIZE);
                }
            }
        }
        String rght = chosen == null ? "null" : firstLetterToCaps(chosen.getItemName());// + " - " + NumberFormat.getNumberInstance(Locale.US).format(chosen.getCount());;
//        System.out.println(chosen.getItemName());
        TextBounds tb = bitmapFont.getBounds(str);
        spriteBatch.end();
        ShapeRenderer shapeRenderer = MainGame.getInstance().getShapeRenderer();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        boolean flip = Gdx.input.getX() + tb.width >= Gdx.graphics.getWidth() - 20;
        if (rightClicked) {
            bitmapFont.getBounds(rght);
            Item item = getItem(chosen.getItemName());

            shapeRenderer.setColor(Color.BLACK);
            shapeRenderer.rect(rightClickX, rightClickY, tb.width + 60, -100 - (item.getRightClick().length * 20));
            shapeRenderer.setColor(Color.BLUE);
            shapeRenderer.rect(rightClickX, rightClickY - 30, tb.width + 60, 3);

            int r = 60;
            boolean isinbox = MainGame.getInstance().isOn(rightClickX - r, rightClickY - ((item.getRightClick().length * 20) + 100) - r, (int) (tb.width + 60) + (r * 2), ((item.getRightClick().length * 20) + 100) + (r * 2));
            if (!isinbox) {
                rightClicked = false;
            }
        } else {
            if (tipbox) {
                shapeRenderer.setColor(Color.BLACK);
                shapeRenderer.rect(Gdx.input.getX(), -Gdx.input.getY() + Gdx.graphics.getHeight() - (tb.height * 3), flip ? -(tb.width + 20) : (tb.width + 20), tb.height * 3);
            }
        }
        shapeRenderer.end();
        spriteBatch.begin();
        if (rightClicked) {
            bitmapFont.setColor(Color.WHITE);
            bitmapFont.draw(spriteBatch, rght, rightClickX + 10, rightClickY - tb.height);

            Item item = getItem(chosen.getItemName());
            int offy = 0;

            for (String option : item.getRightClick()) {
                boolean ison = MainGame.getInstance().isOn(rightClickX + 10, (rightClickY) - offy - 55, (int) (tb.width + 50), 20);
                bitmapFont.setColor(ison ? Color.RED : Color.WHITE);
                bitmapFont.draw(spriteBatch, firstLetterToCaps(option), rightClickX + 10, rightClickY - tb.height - offy - 30);
                if (ison && Gdx.input.isTouched()) {
                    rightClicked = false;
                    PacketAction pa = new PacketAction("inv right " + chosen.getItemName() + " " + option);
                    ScreenGame.getInstance().getConnection().addPacketToQueue(pa);
                }
                offy += 20;
            }

            boolean onuse = MainGame.getInstance().isOn(rightClickX + 10, (rightClickY) - offy - 55, (int) (tb.width + 50), 20);
            boolean onexamine = MainGame.getInstance().isOn(rightClickX + 10, (rightClickY) - offy - 75, (int) (tb.width + 50), 20);
            boolean oncancel = MainGame.getInstance().isOn(rightClickX + 10, (rightClickY) - offy - 95, (int) (tb.width + 50), 20);
            invAction(onuse ? 0 : (onexamine ? 1 : (oncancel ? 2 : -1)));

            bitmapFont.setColor(onuse ? Color.RED : Color.WHITE);
            bitmapFont.draw(spriteBatch, "Use", rightClickX + 10, rightClickY - tb.height - offy - 30);
            bitmapFont.setColor(onexamine ? Color.RED : Color.WHITE);
            bitmapFont.draw(spriteBatch, "Examine", rightClickX + 10, rightClickY - tb.height - offy - 50);
            bitmapFont.setColor(oncancel ? Color.RED : Color.WHITE);
            bitmapFont.draw(spriteBatch, "Cancel", rightClickX + 10, rightClickY - tb.height - offy - 70);
        } else if (tipbox) {
            bitmapFont.setColor(Color.WHITE);
            bitmapFont.draw(spriteBatch, str, (flip ? Gdx.input.getX() - tb.width - 15 : Gdx.input.getX() + 15), -Gdx.input.getY() + Gdx.graphics.getHeight() - tb.height);
        }
    }

    public void update() {
        if (Gdx.input.isButtonPressed(Buttons.RIGHT) && !rightClicked) {
            if (getInventoryItems() != null) {
                for (int i = 0; i < getInventoryItems().length; i++) {
                    int counterPosY = (i / inventory_item_width) * ((inventory_item_space / 2) + 15);
                    int counterPosX = ((counterPosY - ((((inventory_item_space / 2) + 15)) * (inventory_item_width/**/)) - ((inventory_item_space / 2) + 15) - (((inventory_item_space / 2) + 15) * ((i / inventory_item_width) - 1))) * (i / inventory_item_width)) + (i * ((inventory_item_space / 2) + 15));

                    int x = counterPosX + (IMAGE_SIZE / 2) + (Gdx.graphics.getWidth() - (IMAGE_SIZE * inventory_item_width)) - inventory_item_space - (IMAGE_SIZE / 2);
                    int y = -counterPosY - (IMAGE_SIZE / 2) + 310;

                    if (MainGame.getInstance().isOn(x, y, IMAGE_SIZE, IMAGE_SIZE)) {
                        chosen = getInventoryItems()[i];
                        rightClicked = true;
                        rightClickX = Gdx.input.getX();
                        rightClickY = -Gdx.input.getY() + Gdx.graphics.getHeight();
                    }
                }
            }
        }
    }

    public Item[] getItems() {
        return items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public void setInventoryItems(InventoryItem[] inventoryItems) {
        this.inventoryItems = inventoryItems;
    }

    public InventoryItem[] getInventoryItems() {
        return inventoryItems;
    }

    public EntityPlayer getPlayer() {
        return player;
    }

    public Item getItem(String name) {
        for (Item item : getItems()) {
            if (item.getName().equalsIgnoreCase(name)) {
                return item;
            }
        }
        return null;
    }

    public static String firstLetterToCaps(String value) {
        value = value.replaceAll("_", " ");
        final StringBuilder result = new StringBuilder(value.length());
        String[] words = value.split("\\s");
        for (int i = 0, l = words.length; i < l; ++i) {
            if (i > 0) {
                result.append(" ");
            }
            result.append(Character.toUpperCase(words[i].charAt(0))).append(words[i].substring(1));
        }
        return result.toString();
    }

    public static String getIntegerAmount(long value) {
        String v = Long.toString(value);
        if (value < 1000) { // 1 thousand
            return value + "";
        }
        if (value < 1000000) {
            return v.substring(0, v.length() - ("000".length())) + "k";
        }
        if (value < 1000000000) { // 1 million
            return v.substring(0, v.length() - ("000000".length())) + "m";
        }
        if (value > 1000000000) { // 1 million
            return v.substring(0, v.length() - ("000000000".length())) + "B";
        }
        return "null";
    }

    public static Color getIntegerColour(String value) {
        if (value.endsWith("m")) {
            return Color.GREEN;
        } else if (value.endsWith("k")) {
            return Color.WHITE;
        } else if (value.endsWith("B")) {
            return Color.BLUE;
        }
        return Color.YELLOW;
    }

    public void invAction(int value) {
        //0 use
        //1 examine
        //2 cancel
        if (chosen != null && this.rightClicked && value != -1 && Gdx.input.isTouched()) {
            this.rightClicked = false;
            switch (value) {
                case 0:
                    ScreenGame.getInstance().getConnection().addPacketToQueue(new PacketAction("inv use " + chosen.getItemName()));
                    break;
                case 1:
                    ScreenGame.getInstance().getConnection().addPacketToQueue(new PacketAction("inv examine " + chosen.getItemName()));
                    break;
                case 2:
                    this.rightClicked = false;
                    break;
            }
        }
    }
}
