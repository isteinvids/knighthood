package me.knighthood.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketAuthenticate;
import me.knighthood.protocol.PacketHandshake;

/**
 *
 * @author Emir
 */
public class Connection extends Thread {

    private final String ip;
    private final int port;
    private final Socket socket;
    private final List<Packet> packetQueue;
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private final PacketHandling packetHandling;

    public Connection(String ip, int port) throws Exception {
        this.ip = ip;
        this.port = port;
        this.packetQueue = new ArrayList();
        this.socket = new Socket(ip, port);
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.in = new ObjectInputStream(socket.getInputStream());
        this.socket.setTcpNoDelay(true);
//        this.socket.connect(new );
        initiateConnection();
        this.packetHandling = new PacketHandling();
        this.start();
    }

    @Override
    public void run() {
        try {
            while (socket.isConnected() && !socket.isClosed()) {
//                this.packetQueue.add(new PacketMove(ScreenGame.getInstance().getPlayer().getPosition()));
                Packet[] pks = packetQueue.toArray(new Packet[0]);
                this.packetQueue.clear();

//                for (Packet p1 : pks) {
//                    if (p1 instanceof PacketChat) {
//                        System.out.println("sending " + ((PacketChat) p1).getMessage());
//                    }
//                }
                out.flush();
                out.writeObject(pks);
                out.flush();
                Packet[] packets = (Packet[]) in.readObject();
                
                if (packets != null) {
                    for (Packet p : packets) {
                        if (p != null) {
                            packetHandling.handlePacket(p);
                        }
                    }
                }
                Thread.sleep(20);
            }
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            this.close(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            this.close(ex.getMessage());
        } catch (InterruptedException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            this.close(ex.getMessage());
        }
    }

    public void close(String error) {
        try {
            this.socket.close();
            MainGame.getInstance().setScreenThreadSafe(Settings.DEBUG_MODE ? new ScreenLogin().notificate(error) : new ScreenLobby().notificate(error));
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initiateConnection() throws Exception {
        out.writeObject(new PacketHandshake());
        out.writeObject(new PacketAuthenticate(ScreenLogin.username, ScreenLogin.sessionid));
        String msg = in.readObject().toString();
        if (msg.contains(" ")) {
            this.close(msg);
        } else {
            ScreenLogin.sessionid = msg;
        }
    }

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public void addPacketToQueue(Packet packet) {
//        this.packetQueue.f
        this.packetQueue.add(packet);
    }

    public PacketHandling getPacketHandling() {
        return packetHandling;
    }

}
