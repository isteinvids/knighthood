package me.knighthood.client;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import me.knighthood.protocol.Position;

/**
 *
 * @author Emir
 */
public abstract class Entity {

    private final String name;
    private Position position;

    public Entity(String name) {
        this.name = name;
        this.position = new Position(0, 0);
    }

    public void onUpdate() {
    }

    public boolean onDeath() {
        return true;
    }

    public String getName() {
        return this.name;
    }

    public Position getPosition() {
        return this.position;
    }

    public Entity setPosition(Position position) {
        this.position = position;
        return this;
    }

    public void render(SpriteBatch batch, boolean nametag) {
    }
}
