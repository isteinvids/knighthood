package me.knighthood.client;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author Emir
 */
public class MainGame extends Game {

    private static MainGame instance;
    private BitmapFont font10;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private SpriteBatch guiSpriteBatch;
    private ImageManager imageManager;
    private Screen screenTo = null;
    private ServerImageManager serverImageManager;
    private final boolean[] mousedown = new boolean[3], press = new boolean[3];

    @Override
    public void create() {
        Texture.setEnforcePotImages(false);
        this.serverImageManager = new ServerImageManager();
        MainGame.instance = this;
        GuiButton.txtButton = new Texture(Gdx.files.internal("data/button.png"));
        this.imageManager = new ImageManager();
        this.font10 = new FreeTypeFontGenerator(Gdx.files.internal("data/font2.ttf")).generateFont(19);
        this.font10.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

        if (Settings.DEBUG_MODE) {
            this.setScreen(new ScreenGame("127.0.0.1", 4444));
        } else {
            this.setScreen(new ScreenLogin());
        }
    }

    @Override
    public void render() {
        if (shapeRenderer == null) {
            this.shapeRenderer = new ShapeRenderer();
            this.spriteBatch = new SpriteBatch();
            this.guiSpriteBatch = new SpriteBatch();
        }
        for (int i = 0; i < mousedown.length; i++) {
            press[i] = false;
            if (Gdx.input.isButtonPressed(i)) {
                if (!mousedown[i]) {
                    press[i] = true;
                    mousedown[i] = true;
                }
            } else {
                mousedown[i] = false;
            }
        }
        if (screenTo != null) {
            super.setScreen(screenTo);
            screenTo = null;
        }
        this.getImageManager().update();
        this.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        this.shapeRenderer.setColor(Color.DARK_GRAY);
        this.shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.shapeRenderer.end();

        try {
            super.render();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (!Settings.DEBUG_MODE) {
                System.exit(-1);
            }
        }
    }

    public boolean isOn(float x, float y, float w, float h) {
        int cx = Gdx.input.getX(), cy = -Gdx.input.getY() + Gdx.graphics.getHeight();
        return (cx > x && cx < x + w) && (cy > y && cy < y + h);
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }

    public static MainGame getInstance() {
        return instance;
    }

    public BitmapFont getFont10() {
        return font10;
    }

    public ImageManager getImageManager() {
        return imageManager;
    }

    public void setScreenThreadSafe(Screen screenTo) {
        this.screenTo = screenTo;
    }

    public ServerImageManager getServerImageManager() {
        return serverImageManager;
    }

    public SpriteBatch getGuiSpriteBatch() {
        return guiSpriteBatch;
    }

    public boolean isMousePress(int mouse) {
        return press[mouse];
    }

}
