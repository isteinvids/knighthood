package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;

/**
 *
 * @author Emir
 */
public class GuiButton {

    public static Texture txtButton;
    private String displayString;
    private int x, y;
    private boolean enabled;
    private final BitmapFont font;
    private boolean pressed = false;
    /* Colours here */
    private Color down = new Color(0.7f, 0.5f, 0.3f, 1f);
    private Color normal = new Color(0.4f, 0.3f, 0.2f, 1f);
    private Color disabled = new Color(0.3f, 0.2f, 0.1f, 1f);

    public GuiButton(BitmapFont font, String displayString) {
        this.displayString = displayString;
        this.font = font;
        this.enabled = true;
    }

    public void render() {
        TextBounds textBounds = font.getBounds(displayString);
        MainGame.getInstance().getGuiSpriteBatch().begin();
        boolean down = MainGame.getInstance().isOn(x, y, txtButton.getWidth(), txtButton.getHeight());

        pressed = false;
        if (enabled) {
            if (down) {
                MainGame.getInstance().getGuiSpriteBatch().setColor(getDown());
                pressed = Gdx.input.isTouched();
            } else {
                MainGame.getInstance().getGuiSpriteBatch().setColor(getNormal());
            }
        } else {
            MainGame.getInstance().getGuiSpriteBatch().setColor(getDisabled());
        }

        MainGame.getInstance().getGuiSpriteBatch().draw(txtButton, x, y);
        MainGame.getInstance().getGuiSpriteBatch().setColor(Color.WHITE);
        font.setColor(Color.WHITE);
        font.draw(MainGame.getInstance().getGuiSpriteBatch(), displayString, x + (txtButton.getWidth() / 2) - (textBounds.width / 2), y + (txtButton.getHeight() / 2) + (textBounds.height / 1.5f));
        font.setColor(Color.WHITE);
        this.enabled = true;
        MainGame.getInstance().getGuiSpriteBatch().end();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public GuiButton setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getDisplayString() {
        return displayString;
    }

    public GuiButton setDisplayString(String displayString) {
        this.displayString = displayString;
        return this;
    }

    public GuiButton setPosition(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isPressed() {
        return pressed;
    }

    public GuiButton setNormal(Color normal) {
        this.normal = normal;
        return this;
    }

    public GuiButton setDown(Color down) {
        this.down = down;
        return this;
    }

    public GuiButton setDisabled(Color disabled) {
        this.disabled = disabled;
        return this;
    }

    public Color getNormal() {
        return normal;
    }

    public Color getDisabled() {
        return disabled;
    }

    public Color getDown() {
        return down;
    }

}
