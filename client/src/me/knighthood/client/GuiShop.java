package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import static me.knighthood.client.Inventory.firstLetterToCaps;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.protocol.Item;
import me.knighthood.protocol.PacketAction;

/**
 *
 * @author Emir
 */
public class GuiShop {

    private static final int IMAGE_SIZE = 32;
    private static final int inventory_item_width = 12;
    private static final int inventory_item_space = IMAGE_SIZE + 16;
//    
    private final ScreenGame screenGame;
    private Texture invBack;
    private Texture close;
    private int shopId;
    private String shopName;
    private String[] items;
    private InventoryItem[] inReturnFors;
//    
    private String chosen = null;
    private boolean rightClicked = false;
    private int rightClickX = 0, rightClickY = 0;

    public GuiShop(ScreenGame screenGame) {
        this.screenGame = screenGame;
        this.invBack = new Texture(Gdx.files.internal("data/inventory1.png"));
        this.close = new Texture(Gdx.files.internal("data/close.png"));
    }

    public boolean shopOpen() {
        return shopId > 0;
    }

    public void render(SpriteBatch spriteBatch) {
        update();
        if (shopOpen()) {
            int closex = 475, closey = Gdx.graphics.getHeight() - 87;
            BitmapFont bitmapFont = MainGame.getInstance().getFont10();
            boolean tipbox = false;
            String str = "null";
            String rght = chosen == null ? "null" : Inventory.firstLetterToCaps(chosen);
            boolean flag = false;
            if (MainGame.getInstance().isOn(closex, closey, 40, 40)) {
                flag = true;
                if (MainGame.getInstance().isMousePress(Input.Buttons.LEFT)) {
                    getScreenGame().getConnection().addPacketToQueue(new PacketAction("shop close"));
                    closeShop();
                    return;
                }
            }
            spriteBatch.draw(invBack, 0, 0);
            spriteBatch.setColor(flag ? Color.RED : new Color(1.0F, 0.9F, 0.0F, 1.0F));
            spriteBatch.draw(close, closex, closey);
            spriteBatch.setColor(Color.WHITE);

            int fw = (int) bitmapFont.getBounds(shopName).width;
            bitmapFont.setColor(Color.BLACK);
            bitmapFont.draw(spriteBatch, shopName, (17 + (Gdx.graphics.getWidth() / 2) - (575 / 4)) - (fw / 2) + 1, Gdx.graphics.getHeight() + -60 - 1);
            bitmapFont.setColor(Color.WHITE);
            bitmapFont.draw(spriteBatch, shopName, (17 + (Gdx.graphics.getWidth() / 2) - (575 / 4)) - (fw / 2), Gdx.graphics.getHeight() + -60);

            if (getItems() != null) {
                for (int i = 0; i < getItems().length; i++) {
                    Item item = getScreenGame().getInventory().getItem(getItems()[i]);
                    Texture text = getScreenGame().getInventory().getUnknownTexture();

                    int counterPosY = (i / inventory_item_width) * ((inventory_item_space / 2) + 15);
                    int counterPosX = ((counterPosY - ((((inventory_item_space / 2) + 15)) * (inventory_item_width/**/)) - ((inventory_item_space / 2) + 15) - (((inventory_item_space / 2) + 15) * ((i / inventory_item_width) - 1))) * (i / inventory_item_width)) + (i * ((inventory_item_space / 2) + 15));

                    int x = counterPosX + (IMAGE_SIZE / 2) + 100 - inventory_item_space - (IMAGE_SIZE / 2);
                    int y = -counterPosY - (IMAGE_SIZE / 2) + (Gdx.graphics.getHeight() + -100);

                    if (item != null) {
                        String wondname = firstLetterToCaps(item.getName());
                        text = MainGame.getInstance().getImageManager().getImage(item.getImageid());
                        if (text == null) {
                            text = getScreenGame().getInventory().getUnknownTexture();
                        }

                        if (MainGame.getInstance().isOn(x - inventory_item_width, y - inventory_item_width, IMAGE_SIZE + (inventory_item_width * 2), IMAGE_SIZE + (inventory_item_width * 2)) && !tipbox) {
                            tipbox = true;
                            str = wondname + " - Costs " + getInReturnFors()[i].getCount() + " " + getInReturnFors()[i].getItemName() + "s";
                            if (item.isStackable()) {
                                str += " - " + getItems()[i];
                            }
                        }

                        spriteBatch.draw(text, x, y, IMAGE_SIZE, IMAGE_SIZE);
                        if (item.isStackable()) {
                            String idkyet = "texthere";
                            bitmapFont.setColor(Color.BLACK);
                            bitmapFont.draw(spriteBatch, idkyet, x + 2, y + IMAGE_SIZE - 2);
                            bitmapFont.setColor(Inventory.getIntegerColour(idkyet));
                            bitmapFont.draw(spriteBatch, idkyet, x, y + IMAGE_SIZE);
                        }
                    } else {
                        spriteBatch.draw(text, x, y, IMAGE_SIZE, IMAGE_SIZE);
                    }
                }
            }

            TextBounds tb = bitmapFont.getBounds(str);
            spriteBatch.end();
            ShapeRenderer shapeRenderer = MainGame.getInstance().getShapeRenderer();
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            boolean flip = Gdx.input.getX() + tb.width >= Gdx.graphics.getWidth() - 20;
            int[] amounts = new int[]{1, 2, 5, 25, 50, 100, 500, 1000};
            int rightx = 0, righty = 0, rightwidth = 0, rightheight = 0;
            if (rightClicked) {
                bitmapFont.getBounds(rght);
                rightx = rightClickX;
                righty = rightClickY;
                rightwidth = (int) (tb.width + 80);
                rightheight = -(amounts.length * 20) - 65;
                int r = 60;

                shapeRenderer.setColor(Color.BLACK);
                shapeRenderer.rect(rightx, righty, rightwidth, rightheight);
                shapeRenderer.setColor(Color.BLUE);
                shapeRenderer.rect(rightx, righty - 30, rightwidth, 3);

                int rig = -rightheight;
                boolean isinbox = MainGame.getInstance().isOn(rightx - r, righty - rig - r, rightwidth + r + (r * 2), rig + (r * 2));
                if (!isinbox) {
                    rightClicked = false;
                }
            } else {
                if (tipbox) {
                    shapeRenderer.setColor(Color.BLACK);
                    shapeRenderer.rect(Gdx.input.getX(), -Gdx.input.getY() + Gdx.graphics.getHeight() - (tb.height * 3), flip ? -(tb.width + 20) : (tb.width + 20), tb.height * 3);
                }
            }
            shapeRenderer.end();
            spriteBatch.begin();
            if (rightClicked) {
                bitmapFont.setColor(Color.WHITE);
//                String desc = rght + "Costs " + getInReturnFors()[in].getCount() + " " + getInReturnFors()[i].getItemName() + "s";
                bitmapFont.draw(spriteBatch, rght, rightClickX + 10, rightClickY - tb.height);

                int offy = 5;

                for (int i = 0; i < amounts.length; i++) {
                    int rx = rightClickX + 10, ry = ((rightClickY) - offy);
                    boolean onbuy = MainGame.getInstance().isOn(rx-10, ry - 50, 50, 20);
                    boolean onsell = MainGame.getInstance().isOn(rx-10+rightwidth-40, ry - 50, 40, 20);
                    bitmapFont.getBounds("Buy");
                    float pl = Math.round(((rightwidth-50) - tb.width)/2);
                    bitmapFont.getBounds(String.valueOf(amounts[i]));
                    pl -= (tb.width/2);
                    bitmapFont.setColor(onbuy ? Color.RED : Color.WHITE);
                    bitmapFont.draw(spriteBatch, "Buy", rx, ry - tb.height - 25);
                    bitmapFont.setColor(onsell ? Color.RED : Color.WHITE);
                    bitmapFont.draw(spriteBatch, "Sell", rx + rightwidth - 40, ry - tb.height - 25);
                    bitmapFont.setColor(Color.WHITE);
                    bitmapFont.draw(spriteBatch, String.valueOf(amounts[i]), rx + pl + 30, ry - tb.height - 25);
                    if ((onbuy||onsell) && Gdx.input.isTouched()) {
                        String choice = onbuy ? "buy" : "sell";
                        rightClicked = false;
                        PacketAction pa = new PacketAction("shop "+choice+" " + getShopId() + " " + chosen + " " + amounts[i]);
                        ScreenGame.getInstance().getConnection().addPacketToQueue(pa);
                    }
                    offy += 20;
                }
                boolean oncancel = MainGame.getInstance().isOn(rightClickX + 10, (rightClickY) - offy - 50, (int) (tb.width + 50), 20);
                bitmapFont.setColor(oncancel ? Color.RED : Color.WHITE);
                bitmapFont.draw(spriteBatch, "Cancel", rightClickX + 10, rightClickY - tb.height - offy - 25);

                if (oncancel) {
                    if (Gdx.input.isTouched()) {
                        rightClicked = false;
                    }
                }
            } else if (tipbox) {
                bitmapFont.setColor(Color.WHITE);
                bitmapFont.draw(spriteBatch, str, (flip ? Gdx.input.getX() - tb.width - 15 : Gdx.input.getX() + 15), -Gdx.input.getY() + Gdx.graphics.getHeight() - tb.height);
            }
        }
    }

    public void update() {
        if (shopOpen() && getItems() != null) {
            if (Gdx.input.isButtonPressed(Buttons.RIGHT) && !rightClicked) {
                if (getItems() != null) {
                    for (int i = 0; i < getItems().length; i++) {

                        int counterPosY = (i / inventory_item_width) * ((inventory_item_space / 2) + 15);
                        int counterPosX = ((counterPosY - ((((inventory_item_space / 2) + 15)) * (inventory_item_width/**/)) - ((inventory_item_space / 2) + 15) - (((inventory_item_space / 2) + 15) * ((i / inventory_item_width) - 1))) * (i / inventory_item_width)) + (i * ((inventory_item_space / 2) + 15));

                        int x = counterPosX + (IMAGE_SIZE / 2) + 100 - inventory_item_space - (IMAGE_SIZE / 2);
                        int y = -counterPosY - (IMAGE_SIZE / 2) + (Gdx.graphics.getHeight() + -100);

                        if (MainGame.getInstance().isOn(x, y, IMAGE_SIZE, IMAGE_SIZE)) {
                            chosen = getItems()[i];
                            rightClicked = true;
                            rightClickX = Gdx.input.getX();
                            rightClickY = -Gdx.input.getY() + Gdx.graphics.getHeight();
                        }
                    }
                }
            }
        }
    }

    public void resume() {
        this.invBack = new Texture(Gdx.files.internal("data/inventory1.png"));
        this.close = new Texture(Gdx.files.internal("data/close.png"));
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public void setItems(String[] items) {
        this.items = items;
    }

    public String[] getItems() {
        return items;
    }

    public ScreenGame getScreenGame() {
        return screenGame;
    }

    public void closeShop() {
        this.items = null;
        this.shopId = -1;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public InventoryItem[] getInReturnFors() {
        return inReturnFors;
    }

    public void setInReturnFors(InventoryItem[] inReturnFors) {
        this.inReturnFors = inReturnFors;
    }

}
