package me.knighthood.client;

/**
 *
 * @author Emir
 */
public class EntityLiving extends Entity {

    private double health;
    private boolean dead;

    public EntityLiving(String name) {
        super(name);
    }

    public double getHealth() {
        return this.health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public boolean isDead() {
        return this.dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

}
