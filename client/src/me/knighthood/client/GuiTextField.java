package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author Emir
 */
public class GuiTextField extends InputAdapter {

    private final BitmapFont font;
    private boolean password;
    private int x, y;
    private String text;
    private int width;

    public GuiTextField(BitmapFont font) {
        this.font = font;
        this.text = "";
        this.width = 300;
        this.password = false;
    }

    public boolean isPassword() {
        return password;
    }

    public GuiTextField setPassword(boolean password) {
        this.password = password;
        return this;
    }

    public GuiTextField setFocus(boolean focus) {
        Gdx.input.setInputProcessor(focus ? this : null);
        return this;
    }

    public void render() {
        String drstr = "";
        for (char c : this.text.toCharArray()) {
            drstr += isPassword() ? "*" : c;
        }

        BitmapFont.TextBounds textBounds = this.font.getBounds(drstr);
        MainGame.getInstance().getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        MainGame.getInstance().getShapeRenderer().setColor(Color.BLACK);
        MainGame.getInstance().getShapeRenderer().rect(x, y, this.getWidth(), 25);

//        if (Gdx.input.getInputProcessor() == this && System.currentTimeMillis() % 1200 < 600) {
//            MainGame.getInstance().getShapeRenderer().setColor(Color.LIGHT_GRAY);
//            MainGame.getInstance().getShapeRenderer().rect(x + textBounds.width + 6, y + 2, 3, 21);
//        }
        MainGame.getInstance().getShapeRenderer().end();
        MainGame.getInstance().getShapeRenderer().begin(ShapeRenderer.ShapeType.Line);
        MainGame.getInstance().getShapeRenderer().setColor(Color.WHITE);
//        MainGame.getInstance().getShapeRenderer().rect(x, y, this.getWidth(), 25);
        MainGame.getInstance().getShapeRenderer().line(x, y + 24, x + this.getWidth(), y + 24);
        MainGame.getInstance().getShapeRenderer().line(x, y, x + 1, y + 24);
        MainGame.getInstance().getShapeRenderer().end();

        MainGame.getInstance().getGuiSpriteBatch().begin();
        MainGame.getInstance().getGuiSpriteBatch().setColor(Color.WHITE);
        this.font.setColor(Color.WHITE);
        this.font.draw(MainGame.getInstance().getGuiSpriteBatch(), drstr, x + 5, y + (textBounds.height / 0.60f));
        if (Gdx.input.getInputProcessor() == this && System.currentTimeMillis() % 1200 < 600) {
            this.font.draw(MainGame.getInstance().getGuiSpriteBatch(), "_", x + textBounds.width + 6, y + (textBounds.height / 0.60f));
        }
        MainGame.getInstance().getGuiSpriteBatch().end();
//MainGame.getInstance().
        if (MainGame.getInstance().isOn(x, y, this.getWidth(), 25)) {
            if (Gdx.input.isTouched()) {
                Gdx.input.setInputProcessor(this);
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public GuiTextField setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public GuiTextField setPosition(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        BitmapFont.TextBounds textBounds = this.font.getBounds(text);
        if (textBounds.width < this.getWidth() - 12 || character == '\b') {
            if (character == '\b' && this.text.length() > 0) {
                this.text = this.text.substring(0, this.text.length() - 1);
            } else if (this.font.containsCharacter(character)) {
                this.text += character;
            }
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    public GuiTextField setText(String text) {
        this.text = text;
        return this;
    }

    public String getText() {
        return text;
    }

}
