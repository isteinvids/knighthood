package me.knighthood.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import me.knighthood.protocol.Tile;

/**
 *
 * @author Emir
 */
public class EntityPlayer extends EntityLiving {

    private Texture texture;

    public EntityPlayer(String name) {
        super(name);
    }

    @Override
    public void render(SpriteBatch batch, boolean nametag) {
        if (texture == null) {
            this.texture = new Texture(Gdx.files.internal("data/player.png"));
        }
        int size = ScreenGame.getInstance().getWorld().mapSize;
        BitmapFont font = MainGame.getInstance().getFont10();
        BitmapFont.TextBounds tb = font.getBounds(getName());

        if (nametag) {
            font.setColor(Color.BLACK);
            font.draw(batch, getName(), (this.getPosition().getX() * size) + (texture.getWidth() / 2) - (tb.width / 2) + 2, (this.getPosition().getY() * size) + texture.getHeight() + 5 - 2);
            font.setColor(Color.WHITE);
            font.draw(batch, getName(), (this.getPosition().getX() * size) + (texture.getWidth() / 2) - (tb.width / 2), (this.getPosition().getY() * size) + texture.getHeight() + 5);

            /*
            font.setColor(Color.BLACK);
            font.draw(batch, getHealth() + "", (this.getPosition().getX() * size) + (texture.getWidth() / 2) - 20 + 2, ((this.getPosition().getY() * size) + texture.getHeight()) - 35 - 2);
            font.setColor(Color.WHITE);
            font.draw(batch, getHealth() + "", (this.getPosition().getX() * size) + (texture.getWidth() / 2) - 20, ((this.getPosition().getY() * size) + texture.getHeight()) - 35);
            */
        } else {
            batch.draw(texture, getPosition().getX() * size, getPosition().getY() * size);
        }
    }

    public Texture getTexture() {
        if (texture == null) {
            this.texture = new Texture(Gdx.files.internal("data/player.png"));
        }
        return texture;
    }

    public Tile getTilePlayerIsStandingOn() {
        World world = ScreenGame.getInstance().getWorld();
        Tile ret = null;
        if (world.tiles != null) {
            for (Tile t : world.tiles) {
                int sx = t.getX();
                int sy = t.getY();
                if (getPosition().getX() > sx - 1 && getPosition().getX() < sx + world.mapSize) {
                    if (getPosition().getY() > sy - 1 && getPosition().getY() < sy + world.mapSize) {
                        ret = t;
//                        return t;
                    }
                }
            }
        }
        return ret;
    }
}
