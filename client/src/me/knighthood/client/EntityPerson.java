package me.knighthood.client;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 *
 * @author Emir
 */
public class EntityPerson extends EntityLiving {

    private String textureId;

    public EntityPerson(String name) {
        super(name);
    }

    public EntityPerson(String name, String textureId) {
        super(name);
        this.textureId = textureId;
    }

    public void setTextureId(String textureId) {
        this.textureId = textureId;
    }

    public String getTextureId() {
        return textureId;
    }

    @Override
    public void render(SpriteBatch batch, boolean nametag) {
        BitmapFont font = MainGame.getInstance().getFont10();

        BitmapFont.TextBounds tb = font.getBounds(getName());
        Texture texture = MainGame.getInstance().getImageManager().getImage(this.getTextureId());
        int size = ScreenGame.getInstance().getWorld().mapSize;
        if (texture != null) {
            if (nametag) {
                if (!getName().startsWith("--")) {
                    font.setColor(Color.BLACK);
                    font.draw(batch, getName(), (this.getPosition().getX() * size) + (texture.getWidth() / 2) - (tb.width / 2) + 2, (this.getPosition().getY() * size) + texture.getHeight() + 5 - 2);
                    font.setColor(Color.YELLOW);
                    font.draw(batch, getName(), (this.getPosition().getX() * size) + (texture.getWidth() / 2) - (tb.width / 2), (this.getPosition().getY() * size) + texture.getHeight() + 5);

                    font.setColor(Color.BLACK);
                    font.draw(batch, getHealth() + "", (this.getPosition().getX() * size) + (texture.getWidth() / 2) - 20 + 2, ((this.getPosition().getY() * size) + texture.getHeight()) - 35 - 2);
                    font.setColor(Color.YELLOW);
                    font.draw(batch, getHealth() + "", (this.getPosition().getX() * size) + (texture.getWidth() / 2) - 20, ((this.getPosition().getY() * size) + texture.getHeight()) - 35);
                }
            } else {
                batch.draw(texture, (this.getPosition().getX() * size), (this.getPosition().getY() * size));
            }
        }
    }

}
