
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import me.knighthood.client.ScreenLogin;
import me.knighthood.client.Settings;
//import me.knighthood.client.Utils;
//import me.knighthood.client.ServerImageManager;
//import me.knighthood.client.Utils;

/**
 *
 * @author Emir
 */
public class MainClass implements java.io.Serializable {

    public static final File cacheDir = new File(System.getProperty("user.home") + File.separator + ".knighthoodcache");
    private static JDialog dialog;
    private static int percentage = 0;
    private static final String DOWNLOAD_URL = "http://knighthood.isteinvids.co.uk/jars/game/libraries";

    public static void permutation(String str) {
        permutation("", str);
    }

    private static void permutation(String prefix, String str) {
        int n = str.length();
        if (n == 0) {
            System.out.println(prefix);
        } else {
            for (int i = 0; i < n; i++) {
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n));
            }
        }
    }

    public static void main(String args[]) throws InterruptedException {
        if (Settings.DEBUG_MODE) {
            KnighthoodClient.startupClient();
            ScreenLogin.username = args[0];
            ScreenLogin.sessionid = args[1];
        } else {
            start();
        }//*/
    }

    public static void start() {
        try {
//            /*
            dialog = new JDialog((Frame) null) {
                @Override
                public void paint(Graphics grphcs) {
                    super.paint(grphcs);
                    render((Graphics2D) grphcs);
                }
            };
            dialog.setTitle("Downloading...");
            dialog.setResizable(false);
            dialog.setSize(420, 125);
            dialog.setLocationRelativeTo(null);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);

            final String libraries = getTextFromUrl(DOWNLOAD_URL);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (String str : libraries.split("\n")) {
                        try {
                            String url = DOWNLOAD_URL.substring(0, DOWNLOAD_URL.lastIndexOf("/") + 1) + str;
                            String name = str.substring(str.indexOf("/") + 1, str.length());
                            File newfile = new File(cacheDir, "libraries" + File.separator + name);
                            dialog.setTitle("Downloading " + name);
                            boolean download = false;
                            if (newfile.exists()) {
                                String servermd5 = getTextFromUrl(DOWNLOAD_URL.substring(0, DOWNLOAD_URL.lastIndexOf("/") + 1) + "libmd5.php?id=" + str);
                                String thismd5 = getLocalFileMD5(newfile);
                                if (!servermd5.equals(thismd5)) {
                                    download = true;
                                }
                            } else {
                                download = true;
                            }
                            if (download) {
                                newfile.getParentFile().mkdirs();
                                download(url, newfile);
                            }
                            addToClasspath(newfile);
                        } catch (Exception ex) {
                            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
                            JOptionPane.showMessageDialog(dialog, ex.getMessage());
                            System.exit(-1);
                        }
                    }
                    dialog.setTitle("Downloads complete, opening game");
                    dialog.dispose();
                    KnighthoodClient.startupClient();
                }
            }, "Main Knighthood Thread").start();
            //*/
        } catch (IOException ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(dialog, ex.getMessage());
            System.exit(-1);
        }
    }

    private static void setPercentage(int newperc) {
        percentage = newperc;
        dialog.repaint();
    }

    private static void render(Graphics2D g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, dialog.getWidth(), dialog.getHeight());
        g.setColor(new Color(0.4f, 0f, 0f));
        g.fillRect(0, 0, (percentage * dialog.getWidth()) / 100, dialog.getHeight());
        g.setColor(Color.lightGray);
        String str = percentage + "%";
        int strw = g.getFontMetrics().stringWidth(str);
        g.drawString(str, (dialog.getWidth() / 2) - (strw / 2), (dialog.getHeight() / 2) + 18);
    }

    public static void download(String remotePath, File localPath) {
        try {
            setPercentage(0);
            URLConnection conn = new URL(remotePath).openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            int size = conn.getContentLength();

            BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
            FileOutputStream out = new FileOutputStream(localPath);
            byte data[] = new byte[1024];
            int count;
            double sumCount = 0.0;

            while ((count = in.read(data, 0, 1024)) != -1) {
                out.write(data, 0, count);

                sumCount += count;
                if (size > 0) {
                    setPercentage((int) (sumCount / size * 100.0));
                }
            }
            in.close();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void openWebpage(String url) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                URI uri = new URL(url).toURI();
                desktop.browse(uri);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
                System.exit(1);
            }
        }
    }

    private static void addToClasspath(File file) throws Exception {
        URL u = file.toURI().toURL();
        URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class sysclass = URLClassLoader.class;
        Method method = sysclass.getDeclaredMethod("addURL", new Class[]{URL.class});
        method.setAccessible(true);
        method.invoke(sysloader, new Object[]{u});
    }

    private static String getLocalFileMD5(File fle) {
        byte[] b = null;
        try {
            InputStream fis = new FileInputStream(fle);

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            b = complete.digest();
            String result = "";
            for (int i = 0; i < b.length; i++) {
                result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
            }
            return result;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return "null";
    }

    private static String getTextFromUrl(String link) throws IOException {
        String htmlText;
        String txt = "";

        URLConnection conn = new URL(link).openConnection();
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((htmlText = in.readLine()) != null) {
            txt += htmlText + "\n";
        }
        in.close();

        return txt.substring(0, txt.length() <= 0 ? txt.length() : txt.length() - 1);
    }
}
