
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import java.io.Serializable;
import me.knighthood.client.MainGame;

/**
 *
 * @author Emir
 */
public class KnighthoodClient implements Serializable{

    public static void startupClient() {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.resizable = false;
        config.useGL20 = true;
        config.width = 800;
        config.height = 600;
        config.title = "Title";
        LwjglApplication application = new LwjglApplication(new MainGame(), config);//*/
//        application.getType().getDeclaringClass();
    }
}
