
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.*;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author EmirRhouni
 */
public class KH {

    private static final String GAME_DL = "http://knighthood.isteinvids.co.uk/jars/game/knighthood.jar";
    private static final String GAME_MD5 = "http://knighthood.isteinvids.co.uk/jars/game/knighthood.php";
    private static final String WEBSITE_LINK = "http://knighthood.isteinvids.co.uk/";
    private static final File GAME_LOCAL = new File(getWorkingDirectory() + File.separator + "knighthood.jar");
    private static final File WORKING_DIR = getWorkingDirectory();
    private static JDialog bar;
    /* */
    private static int perc = 0;

    static void println(Object msg) {
        System.out.print(msg + "\n");
    }

    static void error(Exception ex) {
        ex.printStackTrace();
        JOptionPane.showMessageDialog(bar, ex);
        System.exit(-1);
    }

    public static void main(String args[]) {
        try {
            Image img = ImageIO.read(KH.class.getResourceAsStream("/splash.png"));

            bar = new JDialog((Frame) null) {

                @Override
                public void paint(Graphics g) {
                    super.paint(g);

                    Graphics2D g2d = (Graphics2D) g;

                    int w = this.getWidth();
                    int h = this.getHeight();

                    g2d.setColor(Color.black);
                    int r = 2;
                    int wsize = 400;
                    g2d.fillRect((w / 2) - (wsize / 2) - r, (h / 2) + 80 - r, wsize + (r * 2), 30 + (r * 2));
                    g2d.setColor(new Color(0.4f, 0f, 0f));
                    g2d.fillRect((w / 2) - (wsize / 2), (h / 2) + 80, (perc * wsize) / 100, 30);
                    g2d.setColor(Color.lightGray);
                    String str = perc + "%";
                    int strw = g2d.getFontMetrics().stringWidth(str);
                    g2d.drawString(str, (w / 2) - (strw / 2), (h / 2) + 80 + 18);
                }
            };
            bar.setAlwaysOnTop(true);
            bar.setSize(img.getWidth(bar), img.getHeight(bar));
            bar.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            bar.setResizable(false);
            bar.setLocationRelativeTo(null);
            bar.setUndecorated(true);
            bar.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            bar.add(new JLabel(new ImageIcon(img)));
//            bar.set;
            bar.setVisible(true);

            KH.CheckInternet checkWebs = new CheckInternet();
            checkWebs.start();
            try {
                for (int i = 0; i < 50; i++) {
                    if (!checkWebs.internet) {
                        Thread.sleep(100);
                    }
                }
            } catch (InterruptedException ex) {
                KH.error(ex);
            }
            checkWebs.interrupt();
            final boolean internet = checkWebs.internet;

            if (!internet) {
                error(new IOException("Could not connect to website"));
            }
            String remoteGameMD5 = getGameMD5();
            if (WORKING_DIR.exists() && GAME_LOCAL.exists()) {
                String localGameMD5 = getLocalFileMD5(GAME_LOCAL.toString());
                if (localGameMD5.equals(remoteGameMD5)) {
                    KH.perc = 100;
                    KH.bar.repaint();
                    Thread.sleep(500);
                    openGame(GAME_LOCAL);
                } else {
                    //follow download
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            downloadURL(GAME_LOCAL.toString(), GAME_DL);
                            openGame(GAME_LOCAL);
                        }
                    };
                    thread.start();
                }
            } else {
                WORKING_DIR.mkdir();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        downloadURL(GAME_LOCAL.toString(), GAME_DL);
                        openGame(GAME_LOCAL);
                    }
                };
                thread.start();
            }//*/
        } catch (Exception ex) {
            Logger.getLogger(KH.class.getName()).log(Level.SEVERE, null, ex);
            error(ex);
        }
    }
//}

    static class CheckInternet extends Thread {

        boolean internet = false;

        @Override
        public void run() {
            try {
                final URL url = new URL(KH.WEBSITE_LINK);
                final URLConnection conn = url.openConnection();
                conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                conn.getContent();
                conn.connect();
                internet = true;
            } catch (MalformedURLException ex) {
                KH.error(ex);
            } catch (IOException ex) {
                KH.error(ex);
            }
        }
    }

    public static String getLocalFileMD5(String filename) {
        byte[] b = new byte[1];
        try {
            InputStream fis = new FileInputStream(filename);

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            b = complete.digest();
        } catch (Exception ex) {
            KH.error(ex);
        }
        String result = "";

        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static String getGameMD5() {
        String htmlText, rtext = "error", ip = "error";
        try {
            URLConnection conn = new URL(KH.GAME_MD5).openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((htmlText = in.readLine()) != null) {
                rtext = htmlText;
            }
            in.close();

            return rtext;

        } catch (MalformedURLException ex) {
            KH.error(ex);
            return "";
        } catch (IOException ex) {
            KH.error(ex);
            return "";
        }
    }

    public static File getWorkingDirectory() {
        return new File(System.getProperty("user.home") + File.separator + ".knighthoodcache");
    }

    public static void openGame(final File file) {
        try {
            URL u = file.toURI().toURL();
            URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
            Class sysclass = URLClassLoader.class;
            Method method = sysclass.getDeclaredMethod("addURL", new Class[]{URL.class});
            method.setAccessible(true);
            method.invoke(sysloader, new Object[]{u});

            KH.bar.dispose();
            Class cl = Class.forName("MainClass");
            Method method2 = cl.getMethod("start");
            method2.setAccessible(true);
            method2.invoke(null);
        } catch (Exception ex) {
            KH.error(ex);
        }
//        System.exit(0);
    }

    public static void downloadURL(String localfile, String urlString) {
        URLConnection conn;
        int size = 0;

        try {
            conn = new URL(urlString).openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            size = conn.getContentLength();
            conn.getInputStream().close();
        } catch (Exception e) {
            KH.error(e);
        }
        try {
            conn = new URL(urlString).openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
            FileOutputStream fout = new FileOutputStream(localfile);

            byte data[] = new byte[1024];
            int count;
            int total = 0;
            while ((count = in.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
                total += count;

                KH.perc = (int) ((total * 100.0) / size);
                KH.bar.repaint();
//                MainClass.bar.setBarValue(total);
            }
            in.close();
            fout.close();
        } catch (MalformedURLException ex) {
            KH.error(ex);
        } catch (IOException ex) {
            KH.error(ex);
        }
    }
//*/

}
