package me.knighthood.server.webpanel;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.Item;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.Utils;
import me.knighthood.server.server.Server;
import me.knighthood.server.server.exceptions.AddressInUseException;
import me.knighthood.server.inventory.Inventory;
import me.knighthood.server.user.Player;

//The simple httpserver v. 0000000000
//Coded by Jon Berg
public class WebServer extends Thread {

    private static WebServer inst;
    private final HashMap<String, WebPage> customPages = new HashMap<String, WebPage>();
    private final HashMap<String, Session> sessions = new HashMap<String, Session>();
    private String ip = "127.0.0.1";
    private int port = 4443;

    public WebServer() {
        inst = this;
        if (load()) {
            this.start();
        }
    }

    public boolean load() {
        try {
            File propFile = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "webpanel/properties.dat");

            Properties props = new Properties();
            if (propFile.exists()) {
                props.load(new FileInputStream(propFile));
            }

            try {
                setIp(props.getProperty("ip", "127.0.0.1"));
                setPort(Integer.parseInt(props.getProperty("port", "4443")));
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
            boolean enabled = props.getProperty("enabled", "false").equals("true");
            addWebpages();
            props.setProperty("ip", getIp());
            props.setProperty("port", Integer.toString(getPort()));
            props.setProperty("enabled", Boolean.toString(enabled));
            if (!propFile.exists()) {
                props.store(new FileOutputStream(propFile), "config for web server");
            }
            return enabled;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public WebServer(String ip, int port) {
        WebServer.inst = this;
        this.ip = ip;
        this.port = port;
        this.addWebpages();
    }

    private void s(String s2) {
//        System.out.println(s2);
    }

    @Override
    public void run() {
        ServerSocket serversocket = null;
        //To easily pick up lots of girls, change this to your name!!!
        try {
            s("Trying to bind to " + getIp() + " on port " + Integer.toString(getPort()) + "...");
            serversocket = new ServerSocket();
            serversocket.bind(new InetSocketAddress(getIp(), getPort()));
        } catch (Exception e) { //catch any errors and print errors to gui
            s("\nFatal Error:" + e.getMessage());
            return;
        }
        s("OK!\n");
        //go in a infinite loop, wait for connections, process request, send response
        while (KnighthoodServer.getInstance().isRunning()) {
            s("\nReady, Waiting for requests...\n");
            try {
                //this call waits/blocks until someone connects to the port we
                //are listening to
                final Socket connectionsocket = serversocket.accept();
                s("Will start handling html...");
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            //figure out what ipaddress the client commes from, just for show!
                            InetAddress client = connectionsocket.getInetAddress();
                            //and print it to gui
                            s(Arrays.toString(client.getAddress()) + " connected to server.\n");
                            //Read the http request from the client from the socket interface
                            //into a buffer.
                            final BufferedReader input
                                    = new BufferedReader(new InputStreamReader(connectionsocket.
                                                    getInputStream()));
                            //Prepare a outputstream from us to the client,
                            //this will be used sending back our response
                            //(header + requested file) to the client.
                            final DataOutputStream output
                                    = new DataOutputStream(connectionsocket.getOutputStream());

                            //as the name suggest this method handles the http request, see further down.
                            //abstraction rules
                            http_handler(connectionsocket, input, output);
                        } catch (Exception e) { //catch any errors, and print them
                            s("Error:" + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) { //catch any errors, and print them
                s("Error:" + e.getMessage());
                e.printStackTrace();
            }

        } //go back in loop, wait for next request
    }

//our implementation of the hypertext transfer protocol
//its very basic and stripped down
    private void http_handler(Socket acceptedSocket, BufferedReader input, DataOutputStream output) {
        int method = 0; //1 get, 2 head, 0 not supported
        String path = new String(); //the various things, what http v, what path,
        String user_agent = null;
        try {
            //This is the two types of request we can handle
            //GET /index.html HTTP/1.0
            //HEAD /index.html HTTP/1.0
            String tmp = input.readLine(); //read from the stream
            int count = 0;
            while (user_agent == null && count < 10) {
                String line = input.readLine();
                if (line.startsWith("User-Agent:")) {
                    user_agent = line + acceptedSocket.getInetAddress().getHostName();
                }
                count++;
            }
            s(user_agent);
            String tmp2 = tmp;
            tmp = tmp.toUpperCase();
            if (tmp.startsWith("GET")) {
                method = 1;
            }
            if (tmp.startsWith("HEAD")) {
                method = 2;
            }

            if (method == 0) {
                try {
                    output.writeBytes(construct_http_header(501, 0));
                    output.close();
                    return;
                } catch (Exception e3) {
                    s("error:" + e3.getMessage());
                }
            }

            int start = 0;
            int end = 0;
            for (int a = 0; a < tmp2.length(); a++) {
                if (tmp2.charAt(a) == ' ' && start != 0) {
                    end = a;
                    break;
                }
                if (tmp2.charAt(a) == ' ' && start == 0) {
                    start = a;
                }
            }
            path = tmp2.substring(start + 2, end); //fill in the path
        } catch (Exception e) {
            s("errorr" + e.getMessage());
        }

        if (path.isEmpty()) {
            path = "index.html";
        }
        s("Client requested:" + path);
        InputStream requestedfile = new ByteArrayInputStream("null".getBytes());
        String name = path.substring(0, path.indexOf("?") == -1 ? path.length() : path.indexOf("?"));
        String htmlText = "";
        WebPage.Arg[] args = new WebPage.Arg[0];
        try {
            if (path.indexOf("?") != path.length() - 1) {
                args = parseUrl(path);
                s("Args: " + Arrays.toString(args));
            }

            File reqs = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "webpanel" + File.separator + "html" + File.separator + name);
            htmlText = Utils.readFile(reqs);
            if (!customPages.containsKey(name)) {
                requestedfile = new ByteArrayInputStream(htmlText.getBytes());
            }
        } catch (Exception e) {
            if (!customPages.containsKey(name)) {
                try {
                    output.writeBytes(construct_http_header(404, 0));
                    output.close();
                } catch (Exception e2) {
                }
                s("error," + e.getMessage());
                return;
            }
        }

        String original = htmlText;
        if (customPages.containsKey(name)) {

            WebPage page = customPages.get(name);
            WebPage.ArgMap argMap = new WebPage.ArgMap(args);
            boolean customhtml = page.onRequest(argMap, user_agent);
            if (customhtml) {
                requestedfile = new ByteArrayInputStream(page.html(original, argMap, user_agent).getBytes());
            }
        }
        s("Custom page done! " + name);

        try {
            int type_is = 0;
            if (name.endsWith(".htm") || name.endsWith(".html")) {
                type_is = 5;
            }
            if (name.endsWith(".zip")) {
                type_is = 3;
            }
            if (name.endsWith(".jpg") || name.endsWith(".jpeg")) {
                type_is = 1;
            }
            if (name.endsWith(".gif")) {
                type_is = 2;
            }
            output.writeBytes(construct_http_header(200, type_is));

            if (method == 1) {
                while (true) {
                    int b = requestedfile.read();
                    if (b == -1) {
                        break;
                    }
                    output.write(b);
                }

            }
            output.close();
            requestedfile.close();
        } catch (Exception e) {
        }

    }

    private String construct_http_header(int return_code, int file_type) {
        String s = "HTTP/1.0 ";

        //you probably have seen these if you have been surfing the web a while
        switch (return_code) {
            case 200:
                s = s + "200 OK";
                break;
            case 400:
                s = s + "400 Bad Request";
                break;
            case 403:
                s = s + "403 Forbidden";
                break;
            case 404:
                s = s + "404 Not Found";
                break;
            case 500:
                s = s + "500 Internal Server Error";
                break;
            case 501:
                s = s + "501 Not Implemented";
                break;
        }

        s = s + "\r\n"; //other header fields,
        s = s + "Connection: close\r\n"; //we can't handle persistent connections
        s = s + "Server: KnighthoodPanel \r\n"; //server name

        switch (file_type) {
            case 0:
                break;
            case 1:
                s = s + "Content-Type: image/jpeg\r\n";
                break;
            case 2:
                s = s + "Content-Type: image/gif\r\n";
            case 3:
                s = s + "Content-Type: application/x-zip-compressed\r\n";
            default:
                s = s + "Content-Type: text/html\r\n";
                break;
        }

        s = s + "\r\n";

        switch (return_code) {
            case 200:
                break;
            case 400:
                s = s + "400 Bad Request";
                break;
            case 403:
                s = s + "403 Forbidden";
                break;
            case 404:
                s = s + "404 Not Found";
                break;
            case 500:
                s = s + "500 Internal Server Error";
                break;
            case 501:
                s = s + "501 Not Implemented";
                break;
        }

        return s;
    }

    private WebPage.Arg[] parseUrl(String path) {
        WebPage.Arg[] args = new WebPage.Arg[0];
        if (path.contains("?")) {
            args = new WebPage.Arg[path.split("&").length];
            int i = 0;
            for (String pt : path.substring(path.indexOf("?") + 1, path.length()).split("&")) {
                args[i] = new WebPage.Arg(pt.split("=")[0], pt.split("=")[1]);
                i++;
            }
        }
        return args;
    }

    public static WebServer instance() {
        return inst;
    }

    public void addWebpages() {
        this.customPages.put("index.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return original;
                }
                String begin = original.substring(0, original.indexOf("<form") - 5);
                String end = original.substring(original.indexOf("</form>"), original.length());
                begin += "Logged in as: " + WebServer.instance().sessions.get(user_agent).username + "<br/><br/>";
                if (WebServer.instance().sessions.get(user_agent).hasRightsFor("players")) {
                    begin += "<a href=\"players.html\">Players</a><br/>";
                }
                if (WebServer.instance().sessions.get(user_agent).hasRightsFor("servers")) {
                    begin += "<a href=\"servers.html\">Servers</a><br/>";
                }
                if (WebServer.instance().sessions.get(user_agent).hasRightsFor("items")) {
                    begin += "<a href=\"items.html\">Items</a><br/>";
                }
                if (WebServer.instance().sessions.get(user_agent).hasRightsFor("restart")) {
                    begin += "<a href=\"restart.html\">Restart</a><br/>";
                }
                if (WebServer.instance().sessions.get(user_agent).hasRightsFor("stop")) {
                    begin += "<a href=\"stop.html\">Stop</a><br/>";
                }
//                KnighthoodServer.getInstance().setRunning(false);
                begin += "<br/><form name=\"input\" action=\"logout.html\" method=\"get\">\n"
                        + "     <input type=\"submit\" value=\"Logout\">"
                        + "</form><br/>" + end;
                return begin;
            }
        });
        this.customPages.put("restart.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return true;
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("stop")) {
                    return true;
                }
                KnighthoodServer.getInstance().setRunning(false);
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"index.html\"\n"
                        + "</script>"; //redirect to index
            }
        });
        this.customPages.put("stop.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return true;
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("stop")) {
                    return true;
                }
                KnighthoodServer.getInstance().setRunning(false);
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"index.html\"\n"
                        + "</script>"; //redirect to index
            }
        });
        this.customPages.put("kick.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return false;
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("player_kick")) {
                    return false;
                }
                if (!args.hasValue("user")) {
                    return false;
                }
                Player player = KnighthoodServer.getInstance().getServerManager().getPlayer(args.getValue("user"));
                if (player == null) {
                    return false;
                }
                player.getConnection().kick("You have been kicked");
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"players.html\"\n"
                        + "</script>"; //redirect to index
            }
        });
        this.customPages.put("addserver.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return "Not logged in<br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("servers")) {
                    return "No permission to do that <br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                //name, ip, port, online mode
                if (!args.hasValue("name") || !args.hasValue("ip") || !args.hasValue("port")) {
                    return "Not enough args<br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                String name = args.getValue("name");
                String ip = args.getValue("ip");
                int port = Integer.parseInt(args.getValue("port"));
                boolean online = args.hasValue("online");
                try {
                    KnighthoodServer.getInstance().getServerManager().createServer(name, ip, online, port).start();
                } catch (AddressInUseException ex) {
                    StringWriter sw = new StringWriter();
                    ex.printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    return exceptionAsString.replaceAll("\n", "<br/>");
                }
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"servers.html\"\n"
                        + "</script>";
            }
        });
        this.customPages.put("removeserver.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return "Not logged in<br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("servers")) {
                    return "No permission to do that <br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!args.hasValue("name")) {
                    return "No server arg<br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                Server srvr = KnighthoodServer.getInstance().getServerManager().getServer(args.getValue("name"));
                if (srvr == null) {
                    return "Server is offline or does not exist (" + args.getValue("name") + ")";
                }
                try {
                    KnighthoodServer.getInstance().getServerManager().stopServer(srvr.getServerName());
                } catch (IOException ex) {
                    StringWriter sw = new StringWriter();
                    ex.printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    return exceptionAsString.replaceAll("\n", "<br/>");
                }
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"servers.html\"\n"
                        + "</script>";
            }
        });
        this.customPages.put("servers.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return "Not logged in<br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("servers")) {
                    return "No permission to do that <br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                String ret = ""
                        + "<script language=\"javascript\">\n"
                        + "function isIp(evt) {\n"
                        + "    evt = (evt) ? evt : window.event;\n"
                        + "    var charCode = (evt.which) ? evt.which : evt.keyCode;\n"
                        + "    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode != 46) {\n"
                        + "        return false;\n"
                        + "    }\n"
                        + "    return true;\n"
                        + "}\n"
                        + "function isNumber(evt) {\n"
                        + "    evt = (evt) ? evt : window.event;\n"
                        + "    var charCode = (evt.which) ? evt.which : evt.keyCode;\n"
                        + "    if (charCode > 31 && (charCode < 48 || charCode > 57)) {\n"
                        + "        return false;\n"
                        + "    }\n"
                        + "    return true;\n"
                        + "}"
                        + "</script>\n";
                ret += "Servers: <br/>\n";
                List<Server> servers = KnighthoodServer.getInstance().getServerManager().getServers();
                for (Server nxt : servers) {
                    ret += nxt.getName() + " <a href=\"removeserver.html?name=" + url_encode(nxt.getServerName()) + "\">Stop server</a> <br/>\n";
                }
                ret += "<br/><form name=\"input\" action=\"addserver.html\" method=\"get\">\n"
                        + "     Name: <input type=\"text\" name=\"name\"><br/>\n"
                        + "     IP: <input type=\"text\" name=\"ip\" onkeypress=\"return isIp(event)\"><br/>\n"
                        + "     Port: <input type=\"text\" name=\"port\" onkeypress=\"return isNumber(event)\"><br/>\n"
                        + "     Online Mode: <input type=\"checkbox\" name=\"online\" value=\"true\" checked><br/>"
                        + "     <input type=\"submit\" value=\"Add Server\">"
                        + "</form><br/>\n";
                ret += "<a href=\"index.html\">Go home</a>";
                return ret;
            }
        });
        this.customPages.put("player.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return "Not logged in <br/>\n"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("player_detailed")) {
                    return "No permission to do that <br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!args.hasValue("user")) {
                    return "No player arg <br/>\n"
                            + "<a href=\"index.html\">Go home</a>";
                }
                Player plyr = KnighthoodServer.getInstance().getServerManager().getPlayer(args.getValue("user"));
                if (plyr == null) {
                    return "Player is offline or does not exist";
                }
                String ret = plyr.getDisplayName() + "<br/>"
                        + "Position: " + plyr.getPosition() + "<br/>"
                        + "IP address: " + plyr.getConnection().getIp() + "<br/>"
                        + "Server: " + plyr.getConnection().getServer().getName() + "<br/>"
                        + "Is noclipping: " + plyr.getAntiCheat().isNoclip() + "<br/>"
                        + "Is muted: " + plyr.getAntiCheat().isMuted() + "<br/>";

                if (WebServer.instance().sessions.get(user_agent).hasRightsFor("player_kick")) {
                    ret += "<a href=\"kick.html?user=" + plyr.getName() + "\">Kick</a><br/>";
                }
                ret += "<br/><a href=\"index.html\">Go home</a>";
                return ret;
            }
        });
        this.customPages.put("players.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
//                WebServer.instance().sessions.containsKey(user_agent);
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return "Not logged in <br/>\n"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("players")) {
                    return "No permission to do that <br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                String ret = "Players: <br/>";
                for (Player plyr : KnighthoodServer.getInstance().getServerManager().getAllPlayers()) {
                    if (WebServer.instance().sessions.get(user_agent).hasRightsFor("player_detailed")) {
                        ret += "<a href=\"player.html?user=" + plyr.getName() + "\">" + plyr.getName() + ", pos: " + plyr.getPosition() + ", ip: " + plyr.getConnection().getIp() + "</a><br/>";
                    } else {
                        ret += plyr.getName() + ", pos: " + plyr.getPosition() + ", ip: " + plyr.getConnection().getIp() + "<br/>";
                    }
                }
                ret += "<a href=\"index.html\">Go home</a>";
                return ret; //redirect to index
            }
        });
        this.customPages.put("items.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
//                WebServer.instance().sessions.containsKey(user_agent);
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                if (!WebServer.instance().sessions.containsKey(user_agent)) {
                    return "Not logged in <br/>\n"
                            + "<a href=\"index.html\">Go home</a>";
                }
                if (!WebServer.instance().sessions.get(user_agent).hasRightsFor("items")) {
                    return "No permission to do that <br/>"
                            + "<a href=\"index.html\">Go home</a>";
                }
                String tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                String ret = "Items: <br/>\n";
                Item[] items = KnighthoodServer.getInstance().getItemManager().getItems();
                for (Item tm : items) {
                    ret += tab + "name: " + tm.getName() + ", examine: " + tm.getExamine() + ", image id: " + tm.getImageid() + " <br/>\n";
                    if (tm.getRightClick() != null && tm.getRightClick().length > 0) {
                        ret += tab + tab + "Right click options: <br/>\n";
                        for (String rt : tm.getRightClick()) {
                            ret += tab + tab + tab + rt + " <br/>\n";
                        }
                    }
                }
                ret += "<a href=\"index.html\">Go home</a>";
                return ret; //redirect to index
            }
        });
        this.customPages.put("login.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                if (user_agent != null) {
                    if (WebServer.instance().sessions.containsKey(user_agent)) {
                        Session sess = WebServer.instance().sessions.get(user_agent);
                        long curTime = System.currentTimeMillis();
                        long msHour = 3600000;
                        //3600000
                        if (curTime > sess.lastTime + msHour) {
                            WebServer.instance().sessions.remove(user_agent);
                        }
                    } else {
                        if (args.hasValue("user") && args.hasValue("pass")) {
                            String user = args.getValue("user");
                            String pass = args.getValue("pass");
                            Session sesss = WebServer.instance().sessionValid(user, pass);
                            if (sesss != null) {
                                WebServer.instance().sessions.put(user_agent, sesss);
                            }
                        }
                    }
                }
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"index.html\"\n"
                        + "</script>"; //redirect to index
            }
        });
        this.customPages.put("logout.html", new WebPage() {

            @Override
            public boolean onRequest(WebPage.ArgMap args, String user_agent) {
                if (user_agent != null) {
                    if (WebServer.instance().sessions.containsKey(user_agent)) {
                        WebServer.instance().sessions.remove(user_agent);
                    }
                }
                return true;
            }

            @Override
            public String html(String original, WebPage.ArgMap args, String user_agent) {
                return "<script language=\"javascript\">\n"
                        + "    window.location.href = \"index.html\"\n"
                        + "</script>"; //redirect to index
            }
        });
    }

    private Session sessionValid(String user, String pass) {
        try {
            File f = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "webpanel/users.json");
            JsonArray jsonArray = new JsonParser().parse(new FileReader(f)).getAsJsonArray();
            for (JsonElement je : jsonArray) {
                if (je.isJsonObject()) {
                    JsonObject login = (JsonObject) je;
                    String jsonUser = login.get("user").getAsString();
                    String jsonPass = login.get("pass").getAsString();
                    String[] rights = new Gson().fromJson(login.get("rights"), String[].class);
                    if (jsonUser.equals(user) && jsonPass.equals(pass)) {
                        return new Session(jsonUser, rights);
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static String url_encode(String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        }
        return url;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
