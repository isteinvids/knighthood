package me.knighthood.server.webpanel;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Emir
 */
public interface WebPage {

    /**
     * Returns true if HTML is customized, otherwise html is file
     *
     * @param args
     * @param user_agent
     * @return
     */
    public boolean onRequest(WebPage.ArgMap args, String user_agent);

    public String html(String original, WebPage.ArgMap args, String user_agent);

    public static class ArgMap {

        private final List<Arg> args;

        public ArgMap(List<Arg> args) {
            this.args = args;
        }

        public ArgMap(Arg[] args) {
            this.args = Arrays.asList(args);
        }

        public List<Arg> getArgs() {
            return args;
        }

        public int getSize() {
            return args.size();
        }

        public String getValue(String key) {
            for (Arg a : args) {
                if (a.name.equals(key)) {
                    return url_decode(a.value);
                }
            }
            return "";
        }

        public boolean hasValue(String key) {
            for (Arg a : args) {
                if (a.name.equals(key)) {
                    return true;
                }
            }
            return false;
        }

        private String url_encode(String url) {
            try {
                return URLEncoder.encode(url, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
            }
            return url;
        }

        private String url_decode(String url) {
            try {
                return URLDecoder.decode(url, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
            }
            return url;
        }

    }

    public static class Arg {

        public String name;
        public String value;

        public Arg(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return name + " = " + value;
        }

    }
}
