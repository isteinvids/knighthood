package me.knighthood.server.webpanel;

import java.util.Arrays;

/**
 *
 * @author Emir
 */
public class Session {

    public String username;
    public String[] rights;
    public long lastTime;

    public Session() {
    }

    public Session(String username, String[] rights) {
        this.username = username;
        this.rights = rights;
        this.lastTime = System.currentTimeMillis();
    }

    public boolean hasRightsFor(String rrr) {
        if (Arrays.asList(rights).contains("everything")) {
            return true;
        }
        return Arrays.asList(rights).contains(rrr);
    }

}
