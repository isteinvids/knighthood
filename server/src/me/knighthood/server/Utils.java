/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.knighthood.server;

import com.google.gson.JsonElement;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.JOptionPane;
import me.knighthood.protocol.SharedSettings;

/**
 *
 * @author Emir
 */
public class Utils {

    public static final String DEFAULT_VERIFY_LINK = "http://knighthood.isteinvids.co.uk/knighthoodv2verify";

    public static byte[] getFileArray(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        for (int readNum; (readNum = fis.read(buf)) != -1;) {
            bos.write(buf, 0, readNum);
        }
        byte[] bytes = bos.toByteArray();
        bos.close();
        fis.close();
        return bytes;
    }

    public static String readFile(File file) throws IOException {
        String ret = "";
        String l;
        BufferedReader br = new BufferedReader(new FileReader(file));
        while ((l = br.readLine()) != null) {
            ret += l + "\n";
        }
        br.close();
        return ret;
    }

    public static String getMD5Checksum(File filename) throws Exception {
        InputStream fis = new FileInputStream(filename);
        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        byte[] b = complete.digest();
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static void addURL(URL u) throws IOException {
        URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class sysclass = URLClassLoader.class;
        try {
            Method method = sysclass.getDeclaredMethod("addURL", new Class[]{java.net.URL.class});
            method.setAccessible(true);
            method.invoke(sysloader, new Object[]{u});
        } catch (Exception t) {
            t.printStackTrace();
            throw new IOException("Error, could not add URL to system classloader");
        }
    }

    public static String String2MD5(String msg) throws NoSuchAlgorithmException {
        String original = msg;
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(original.getBytes());
        byte[] digest = md.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(Integer.toHexString((int) (b & 0xff)));
        }
        return sb.toString();
    }

    public static String getLocalFileMD5(String filename) {
        byte[] b = null;
        try {
            InputStream fis = new FileInputStream(filename);

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            b = complete.digest();
            String result = "";
            for (int i = 0; i < b.length; i++) {
                result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
            }
            return result;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return "null";
    }

    public static String encryptSHA256(String password, String salt) {
        String encrypted = sha256(password + salt);
        for (int i = 0; i < 65536; i++) {
            encrypted = sha256(encrypted + salt);
        }
        return encrypted;
    }

    private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static String getTextFromUrl(String link) throws IOException {
        String htmlText;
        String txt = "";

        URLConnection connection = new URL(link).openConnection();
        connection.setRequestProperty("User-Agent", SharedSettings.USER_AGENT);
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((htmlText = in.readLine()) != null) {
            txt += htmlText + "\n";
        }
        in.close();

        return txt.substring(0, txt.length() <= 0 ? txt.length() : txt.length() - 1);
    }

    public static String verifyID(String VERIFY_LINK, String username, String sessionid) {
        try {
            String url = VERIFY_LINK + "?username=" + username + "&sessionid=" + sessionid;
            String txt = getTextFromUrl(url);
            if (txt.contains("true")) {
                return txt.split(":")[1] + ":" + txt.split(":")[2];
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "5:null";
    }
}
