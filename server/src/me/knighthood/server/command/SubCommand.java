package me.knighthood.server.command;

public abstract class SubCommand
{
	
	private final String name;
	private final String command;
	private final String description;
	private final String usage;
	private final String[] aliases;
	
	/**
	 * 
	 * @param name the name of the command
	 * @param description the description of the command
	 * @param usage how to use the command
	 */
	public SubCommand(String name, String description, String usage)
	{
		
		this(name, name, description, usage, new String[0]);
		
	}
	
	/**
	 * 
	 * @param name the name of the command
	 * @param command what to enter to trigger the command
	 * @param description the description of the command
	 * @param usage how to use the command
	 * @param aliases alternative string to use to trigger the command
	 */
	public SubCommand(String name, String command, String description, String usage, String[] aliases)
	{
	
		this.name = name;
		this.command = command;
		this.description = description;
		this.usage = usage;
		this.aliases = aliases;
		
	}

	/**
	 * 
	 * This method is called when the command is triggered
	 * 
	 * @param user the user who triggered the command
	 * @param command the primary command class
	 * @param commandLabel the command string entered to trigger the primary command
	 * @param subcommandLabel the string entered to trigger this command
	 * @param args the args provided for the command
	 * @return true on command success and false on error
	 */
	public abstract boolean onCommand(
			CommandSender sender, 
			Command command, 
			String commandLabel, 
			String subcommandLabel, 
			String[] args);
	
	/**
	 * 
	 * This method is used to get the name of the command
	 * 
	 * @return the command name
	 */
	public String getName() 
	{
	
		return this.name;
	
	}

	/**
	 * 
	 * This method is used to get the string there triggers the command
	 * 
	 * @return the command string
	 */
	public String getCommand()
	{
	
		return this.command;
	
	}

	/**
	 * 
	 * This method is used to get the description of the command
	 * 
	 * @return the command description
	 */
	public String getDescription()
	{
	
		return this.description;
	
	}

	/**
	 * 
	 * This method is used to get the command usage
	 * 
	 * @return the command usage
	 */
	public String getUsage()
	{
	
		return this.usage;
	
	}
	
	/**
	 * 
	 * This method is used to get the command aliases
	 * 
	 * @return the command aliases
	 */
	public String[] getAliases()
	{
		
		return this.aliases;
		
	}

}