package me.knighthood.server.command;

/**
 * @author Roe
 *
 */
public interface CommandSender
{

	/**
	 * 
	 * This method is used to get the type of the command sender
	 * 
	 * @return the type
	 */
	public String getType();
        
	/**
	 * 
	 * This method is used to get the name of the command sender
	 * 
	 * @return the name
	 */
	public String getName();
	
	/**
	 * 
	 * This method is used to send a message to the command sender
	 * 
	 * @param message the message to send
	 */
	public void sendMessage(String message);
	
	/**
	 * 
	 * This method is used to get the rights the user has
	 * 
	 * @return true if the right level are equal or higher
	 */
	public int getRights();
	
}