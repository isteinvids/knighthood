package me.knighthood.server.command;

import java.util.ArrayList;

/**
 * 
 * @author Roe
 *
 */
public abstract class Command
{

	private final String name;
	private final String command;
	private final String description;
	private final String usage;
	private final String[] aliases;
	private final ArrayList<SubCommand> subcommands = new ArrayList<SubCommand>();
	
	public Command(String name, String description, String usage)
	{
		
		this(name, name, description, usage, new String[0]);
		
	}
	
	public Command(String name, String command, String description, String usage, String[] aliases)
	{
	
		this.name = name;
		this.command = command;
		this.description = description;
		this.usage = usage;
		this.aliases = aliases;
		
	}
	
	/**
	 * 
	 * This method is called when the command is triggered, but no subcommands we're found
	 * 
	 * @param user the user who triggered the command
	 * @param commandLabel the command string entered to trigger the primary command
	 * @param args the args provided for the command
	 * @return true on command success and false on error
	 */
	public abstract boolean onCommand(
			CommandSender sender, 
			Command command, 
			String commandLabel, 
			String[] args);
	
	/**
	 * 
	 * This method is used to get the name of the command
	 * 
	 * @return the command name
	 */
	public String getName() 
	{
	
		return this.name;
	
	}

	/**
	 * 
	 * This method is used to get the string there triggers the command
	 * 
	 * @return the command string
	 */
	public String getCommand()
	{
	
		return this.command;
	
	}

	/**
	 * 
	 * This method is used to get the description of the command
	 * 
	 * @return the command description
	 */
	public String getDescription()
	{
	
		return this.description;
	
	}

	/**
	 * 
	 * This method is used to get the command usage
	 * 
	 * @return the command usage
	 */
	public String getUsage()
	{
	
		return this.usage;
	
	}
	
	/**
	 * 
	 * This method is used to get the command aliases
	 * 
	 * @return the command aliases
	 */
	public String[] getAliases()
	{
		
		return this.aliases;
		
	}

	/**
	 * 
	 * This method is used to get all the registered subcommands
	 * 
	 * @return the sub commands
	 */
	public ArrayList<SubCommand> getSubcommands()
	{
	
		return this.subcommands;
	
	}
	
}