/**
 * 
 */
package me.knighthood.server.command.commands;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.command.*;
import me.knighthood.server.user.Console;

/**
 * @author Roe
 *
 */
public class Stop extends Command
{

	public Stop()
	{
	
		super("Stop", "stop", "Stops the server", "/stop", new String[]
				{
				
					"shutdown",
					"OhHellNo!"
				
				});

	}

	/**
	 * 
	 * @see me.knighthood.server.command.Command#onCommand(me.knighthood.server.user.User, me.knighthood.server.command.Command, java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) 
	{
		
		if(sender instanceof Console)
		{
			
                        if(args.length == 0)
                        {
                            
        			sender.sendMessage("Shutting down!");
                		KnighthoodServer.getInstance().setRunning(false);
                                
			}
                        else if (args.length == 1)
                        {
                        
                                String serverName = args[0].replaceAll("_", " ");
                                try 
                                {
                                    
                                        if (KnighthoodServer.getInstance().getServerManager().stopServer(serverName))
                                        {
                                            
                                                sender.sendMessage("Stopped server " + serverName);
                                            
                                        }
                                        else
                                        {
                                            
                                                sender.sendMessage("Server " + serverName+" does not exist");
                                            
                                        }
                                        
                                } 
                                catch (IOException ex)
                                {
                                    
                                        Logger.getLogger(Stop.class.getName()).log(Level.SEVERE, null, ex);
                                        sender.sendMessage("Couldn't stop " + serverName + ", maybe it's already closed?");
                                        
                                }
                                
                        }
                        else
                        {
                        
                                sender.sendMessage("Usage: "+this.getUsage());
                            
                        }
                        
			return true;
			
		}

		sender.sendMessage("This command can only be fired from the console!");
		
		return false;
	
	}

}
