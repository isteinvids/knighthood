package me.knighthood.server.command.commands;

import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.command.Command;
import me.knighthood.server.command.CommandSender;
import me.knighthood.server.user.Player;

/**
 *
 * @author EmirRhouni
 */
public class Noclip extends Command {

    public Noclip() {

        super("Noclip", "noclip", "Cheat command", "/noclip", new String[0]);

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player ply = (Player) sender;
            ply.getAntiCheat().setNoclip(!ply.getAntiCheat().isNoclip());
            return true;
        }
        sender.sendMessage("This command can only be fired from a player");
        return false;
    }

}
