package me.knighthood.server.command.commands;

import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.command.Command;
import me.knighthood.server.command.CommandSender;
import me.knighthood.server.server.Server;

/**
 *
 * @author EmirRhouni
 */
public class Reload extends Command {

    public Reload() {

        super("Reload", "reload", "Reloads the plugins", "/reload", new String[0]);

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if (args.length == 0) {
            for (Server serv : KnighthoodServer.getInstance().getServerManager().getServers()) {
                KnighthoodServer.getInstance().getPluginManager().reload(serv);
            }
        }
        if (args.length == 1 && args[0].equals("--w")) {
            for (Server server : KnighthoodServer.getInstance().getServerManager().getServers()) {
                server.getWorld().reload();
            }
        }
        return false;

    }

}
