package me.knighthood.server.command;

import java.util.ArrayList;
import java.util.Arrays;
import me.knighthood.server.command.commands.Noclip;
import me.knighthood.server.command.commands.Reload;
import me.knighthood.server.command.commands.Stop;

public final class CommandManager
{
	
	private final ArrayList<Command> commands = new ArrayList<Command>();
	
	public CommandManager()
	{
		
		this.addCommand(new Stop());
		this.addCommand(new Reload());
		this.addCommand(new Noclip());
		
	}

	public void addCommand(Command command)
	{
            
                boolean add = true;

                for (Command cmd : getCommands())
                {
                    
                        if (cmd.getCommand().equalsIgnoreCase(command.getCommand()) || cmd.getName().equalsIgnoreCase(command.getName()))
                        {
                            
                                add = false;
                                
                        }
                        
                }

                if (add) 
                {
                    
                        this.getCommands().add(command);
                        System.out.println("Added command: " + command.getName());
                        
                }
		
	}
	
	public boolean executeCommand(CommandSender sender, String in)
	{
		
		try
		{
                        String commandLabel = in.trim();
                        String[] args = new String[0];

                        if (in.contains(" "))
                        {

                                String[] inSplit = in.split(" ");
                                args = Arrays.copyOfRange( inSplit, 1, inSplit.length);

                                commandLabel = inSplit[0];

                        }
		
			for(Command command : this.getCommands())
			{
				
				if(command.getCommand().equals(commandLabel))
				{
					return this.onCommand(sender, command, commandLabel, args);
					
				}
				
				for(String alias : command.getAliases())
				{
					
					if(alias.equals(commandLabel))
					{
						return this.onCommand(sender, command, commandLabel, args);
						
					}
					
				}
				
			}
		}
		catch(Exception e)
		{
			
			//We're catching this to make sure that no uncaught exceptions break something
			e.printStackTrace();
			sender.sendMessage("An internal error occoured while performing this command.");
			
		}
		sender.sendMessage("Invalid command");
		return false;
		
	}
	
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args)
	{
		
		if(args.length > 0)
		{
			
			for(String arg : args)
			{
				
				for(SubCommand subcommand : command.getSubcommands())
				{
					
					if(subcommand.getName().equals(commandLabel))
					{
						
						return subcommand.onCommand(sender, command, commandLabel, arg, args);
						
					}
					
					for(String alias : subcommand.getAliases())
					{
						
						if(alias.equals(arg))
						{

							return subcommand.onCommand(sender, command, commandLabel, arg, args);
							
						}
						
					}
					
				}
				
			}
			
		}
		
		return command.onCommand(sender, command, commandLabel, args);
		
	}
	
	public ArrayList<Command> getCommands()
	{
	
		return this.commands;
	
	}

}