package me.knighthood.server;

import com.google.gson.JsonObject;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;

/**
 *
 * @author Emir
 */
public class NBTTagCompound {

//    private final Map<String, Object> values = new HashMap<String, Object>();
    private JsonObject jsonObject = new JsonObject();

    public void setJsonObject(JsonObject jsonObject) {
        if (jsonObject != null) {
            this.jsonObject = jsonObject;
        }
    }

    public void add(String name, Object val) {
        if (val instanceof Number) {
            jsonObject.addProperty(name, (Number) val);
        }
        if (val instanceof Boolean) {
            jsonObject.addProperty(name, (Boolean) val);
        }
        if (val instanceof String) {
            jsonObject.addProperty(name, (String) val);
        }
    }

    public String getAsString(String name) {
        return jsonObject.get(name).getAsString();
    }

    public Number getAsNumber(String name) {
        return jsonObject.get(name).getAsNumber();
    }

    public Boolean getAsBoolean(String name) {
        return jsonObject.get(name).getAsBoolean();
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }

    public LuaTable getLuaTable() {
        LuaTable table = new LuaTable();
        table.set("add", new TwoArgFunction() {

            @Override
            public LuaValue call(LuaValue lv, LuaValue lv1) {
                String name1 = lv.checkjstring();
                Object value1 = lv1.toString();
                if (lv1.isstring()) {
                    value1 = lv1.tojstring();
                }
                if (lv1.isboolean()) {
                    value1 = lv1.toboolean();
                }
                if (lv1.isint()) {
                    value1 = lv1.toint();
                }
                NBTTagCompound.this.add(name1, value1);
                return NIL;
            }
        });
        table.set("getAsString", new OneArgFunction() {

            @Override
            public LuaValue call(LuaValue lv) {
                String ret = "";
                if (jsonObject.has(lv.checkjstring())) {
                    ret = jsonObject.get(lv.checkjstring()).getAsString();
                }
                return LuaValue.valueOf(ret);
            }
        });
        table.set("getAsNumber", new OneArgFunction() {

            @Override
            public LuaValue call(LuaValue lv) {
                double ret = 0;
                if (jsonObject.has(lv.checkjstring())) {
                    ret = jsonObject.get(lv.checkjstring()).getAsDouble();
                }
                return LuaValue.valueOf(ret);
            }
        });
        table.set("getAsBoolean", new OneArgFunction() {

            @Override
            public LuaValue call(LuaValue lv) {
                boolean ret = false;
                if (jsonObject.has(lv.checkjstring())) {
                    ret = jsonObject.get(lv.checkjstring()).getAsBoolean();
                }
                return LuaValue.valueOf(ret);
            }
        });
        table.set("has", new OneArgFunction() {

            @Override
            public LuaValue call(LuaValue lv) {
                return LuaValue.valueOf(has(lv.checkjstring()));
            }
        });
        return table;
    }

    public boolean has(String val) {
        return jsonObject.has(val);
    }
}
