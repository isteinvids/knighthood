package me.knighthood.server.inventory;

import me.knighthood.protocol.Item;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.server.KnighthoodServer;

/**
 *
 * @author Emir
 */
public class Inventory {

	/* Instance items*/
	private final List<InventoryItem> itemList = new ArrayList();
	private final int size;
	private InventoryItem using;

	public Inventory(int size) {
		this.size = size;
	}

	public boolean hasItem(String itm) {
		return getItemCount(itm) > 0;
	}

	public int getItemCount(String itm) {
		int ret = 0;
		for (InventoryItem it : getItemList()) {
			if (it.getItemName().equals(itm)) {
				Item it0 = getItem(it);
				if (it0.isStackable()) {
					ret += it.getCount();
				} else {
					ret++;
				}
			}
		}
		return ret;
	}

	public boolean addItem(String itm, int amount) {
		boolean ret = false;
		Item item = getItem(itm);
		if (item != null) {
			if (item.isStackable()) {
				Iterator<InventoryItem> iterator = itemList.iterator();
				while (iterator.hasNext()) {
					InventoryItem inventoryItem = iterator.next();
					if (inventoryItem.getItemName().equalsIgnoreCase(itm)) {
						inventoryItem.setCount(inventoryItem.getCount() + amount);
						ret = true;
					}
				}
				if (!ret) {
					if (this.getItemList().size() < size) {
						ret = this.getItemList().add(new InventoryItem(itm, amount));
					}
				}
			} else {
				for (int i = 0; i < amount; i++) {
					if (this.getItemList().size() < size) {
						ret = this.getItemList().add(new InventoryItem(itm, 1));
					} else {
						ret = false;
					}
				}
			}
		}
		return ret;
	}

	public boolean removeItem(String itm, int amount) {
		boolean ret = false;
		Iterator<InventoryItem> iterator = itemList.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			InventoryItem inventoryItem = iterator.next();
			Item item = getItem(inventoryItem);
			if (inventoryItem.getItemName().equalsIgnoreCase(itm)) {
				ret = true;
				if (item.isStackable()) {
					int newcount = inventoryItem.getCount() - amount;
					if (newcount <= 0) {
						iterator.remove();
					} else {
						inventoryItem.setCount(newcount);
					}
				} else if (i < amount) {
					iterator.remove();
					i++;
				}
			}
		}
		return ret;
	}

	public InventoryItem[] getItemArray() {
		return itemList.toArray(new InventoryItem[0]);
	}

	public List<InventoryItem> getItemList() {
		return itemList;
	}

	public InventoryItem getInvFromItem(Item item) {
		for (InventoryItem ii : getItemList()) {
			if (ii.getItemName().equals(item.getName())) {
				return ii;
			}
		}
		return null;
	}

	public static Item getItem(String itemName) {
		for (Item item : KnighthoodServer.getInstance().getItemManager().getItems()) {
			if (item.getName().equalsIgnoreCase(itemName)) {
				return item;
			}
		}
		return null;
	}

	public static Item getItem(InventoryItem tm) {
		for (Item item : KnighthoodServer.getInstance().getItemManager().getItems()) {
			if (item.getName().equalsIgnoreCase(tm.getItemName())) {
				return item;
			}
		}
		return null;
	}

	public InventoryItem getUsing() {
		return using;
	}

	public void setUsing(InventoryItem using) {
		this.using = using;
	}

	public static String firstLetterToCaps(String value) {
		value = value.replaceAll("_", " ");
		final StringBuilder result = new StringBuilder(value.length());
		String[] words = value.split("\\s");
		for (int i = 0, l = words.length; i < l; ++i) {
			if (i > 0) {
				result.append(" ");
			}
			result.append(Character.toUpperCase(words[i].charAt(0))).append(words[i].substring(1));
		}
		return result.toString();
	}

	public static String getIntegerAmount(long value) {
		String v = Long.toString(value);
		if (value < 1000) { // 1 thousand
			return value + "";
		}
		if (value < 1000000) {
			return v.substring(0, v.length() - ("000".length())) + "k";
		}
		if (value < 1000000000) { // 1 million
			return v.substring(0, v.length() - ("000000".length())) + "m";
		}
		if (value > 1000000000) { // 1 million
			return v.substring(0, v.length() - ("000000000".length())) + "B";
		}
		return "null";
	}

	public int getSize() {
		return size;
	}

	public int getFreeSpace() {
		return getSize() - getUsedSpace();
	}

	public int howMuchOfItemCanFit(String item) {
		Item it = getItem(item);
		if (it != null) {
			if (it.isStackable() && (getFreeSpace() > 0 || hasItem(item))) {
				return Integer.MAX_VALUE - 1 - getItemCount(item);
			}
			if (!it.isStackable()) {
				return getFreeSpace();
			}
		}
		return 0;
	}

	public boolean canItemFit(String item, int amount) {
		Item it = getItem(item);
		if (it != null) {
			if (it.isStackable()) {
				if (hasItem(item)) {
					return true;
				}
				if (getFreeSpace() > 0) {
					return true;
				}
			} else {
				if (getFreeSpace() > amount) {
					return true;
				}
			}
		}
		return false;
	}

	public int getUsedSpace() {
		int space = 0;
		for (InventoryItem tm : itemList) {
			Item item = getItem(tm);
			if (item != null) {
				space++;
			}
		}
		return space;
	}
}
