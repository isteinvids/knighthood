package me.knighthood.server.inventory;

import java.util.HashMap;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.protocol.Item;
import me.knighthood.protocol.PacketShop;
import me.knighthood.server.user.Player;

/**
 *
 * @author Emir
 */
public class Shop {

	public static class ShopInfo {

		public int id;
		public String shopName;
		public final HashMap<String, InventoryItem> itemList = new HashMap<String, InventoryItem>();

		public ShopInfo(int id, String shopName) {
			this.id = id;
			this.shopName = shopName;
		}

	}
	private int id;
	private final String shopName;
	private ShopManager shopManager;
//    add later, when having selling becomes inventoryitem
//    private final HashMap<String, InventoryItem> spawnItemList = new HashMap<String, InventoryItem>();

    //selling, inReturnFor
	//TODO: change key to item with amount, make boolean that decides if stock is infinite
	private final HashMap<String, InventoryItem> itemList = new HashMap<String, InventoryItem>();

	protected Shop(String shopName) {
		this.shopName = shopName;
	}

	public Shop setShopManager(ShopManager shopManager) {
		this.shopManager = shopManager;
		return this;
	}

	public ShopManager getShopManager() {
		return shopManager;
	}

	public int getId() {
		return id;
	}

	protected Shop setId(int id) {
		this.id = id;
		return this;
	}

	public int getItemCount(String itm) {
		for (InventoryItem it : getItemList().values()) {
			if (it.getItemName().equals(itm)) {
				return it.getCount();
			}
		}
		return 0;
	}

	public Shop addSellingItem(String selling, String inReturn, int amount) {
		itemList.put(selling, new InventoryItem(inReturn, amount));
		return this;
	}

	public Shop addSellingItem(String selling, InventoryItem inventoryItem) {
		itemList.put(selling, inventoryItem);
		return this;
	}

	public HashMap<String, InventoryItem> getItemList() {
		return itemList;
	}

	public String[] getSellingItem() {
		return itemList.keySet().toArray(new String[0]);
	}

	public InventoryItem[] getInReturnFors() {
		return itemList.values().toArray(new InventoryItem[0]);
	}

	public InventoryItem getInvFromItem(Item item) {
		for (InventoryItem ii : getItemList().values()) {
			if (ii.getItemName().equals(item.getName())) {
				return ii;
			}
		}
		return null;
	}

	//todo: amount
	public void playerSellsItem(Player player, String shopItem, int amount) {
		InventoryItem inret = itemList.get(shopItem);
		if (inret == null) {
			getShopManager().getServer().getLoginLog().logSuspiciousActivity(player, "Invalid item (" + shopItem + ") for shop " + getId());
			player.sendMessage("Invalid item. Suspicious log goes here");
			return;
		}
		//if amount is bigger than howmuchcanfit, make amount the value of howmuchcanfit
		int howmuchcanfit = player.getInventory().howMuchOfItemCanFit(inret.getItemName());
		if (amount > howmuchcanfit) {
			amount = howmuchcanfit;
		}
		int inretCount = inret.getCount() * amount;
		if (amount == 0) {
			player.sendMessage("Not enough inventory space");
			return;
		}
		System.out.println("item fit: " + amount);
		if (player.getInventory().getItemCount(shopItem) >= amount) {
			player.getInventory().addItem(inret.getItemName(), inretCount);
			player.getInventory().removeItem(shopItem, amount);
			player.sendMessage("sold " + amount + " of " + shopItem.replaceAll("_", " "));
		} else {
			player.sendMessage("You do not have enough " + shopItem.replaceAll("_", " "));
		}
	}

	public void playerBuysItem(Player player, String shopItem, int amount) {
		InventoryItem inret = itemList.get(shopItem);
		if (inret == null) {
			getShopManager().getServer().getLoginLog().logSuspiciousActivity(player, "Invalid item (" + shopItem + ") for shop " + getId());
			player.sendMessage("Invalid item. Suspicious log goes here");
			return;
		}
		//if amount is bigger than howmuchcanfit, make amount the value of howmuchcanfit
		int howmuchcanfit = player.getInventory().howMuchOfItemCanFit(shopItem);
		if (amount > howmuchcanfit) {
			amount = howmuchcanfit;
		}
		int inretCount = inret.getCount() * amount;
		if (amount == 0) {
			player.sendMessage("Not enough inventory space");
			return;
		}
		if (player.getInventory().getItemCount(inret.getItemName()) > inretCount) {
			player.getInventory().removeItem(inret.getItemName(), inretCount);
			player.getInventory().addItem(shopItem, amount);
			player.sendMessage("Bought " + amount + " of " + shopItem.replaceAll("_", " "));
		} else {
			player.sendMessage("You do not have enough " + inret.getItemName().replaceAll("_", " "));
		}
	}

	public String getShopName() {
		return shopName;
	}

	public PacketShop toPacket() {
		return new PacketShop(getId(), getShopName(), getSellingItem(), getInReturnFors());
	}
}
