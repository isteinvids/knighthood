package me.knighthood.server.inventory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.Item;
import me.knighthood.server.KnighthoodServer;

/**
 *
 * @author Emir
 */
public class ItemManager {

	private final File itemsdir;
	private Item[] items;

	public ItemManager() {
		this.itemsdir = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "items");
		this.loadItems();
	}

	public void loadItems() {
		try {
			itemsdir.mkdirs();
			File jsonfile = new File(itemsdir, "items.json");

			List<Item> itemlist = new ArrayList();
			JsonObject jsonObject = new JsonParser().parse(new FileReader(jsonfile)).getAsJsonObject();
			for (JsonElement je : jsonObject.get("items").getAsJsonArray()) {
				//name examine imageid stackable
				String name = je.getAsJsonObject().get("name").getAsString();
				String examine = je.getAsJsonObject().get("examine").getAsString();
				String imageid = je.getAsJsonObject().get("imageid").getAsString();
				boolean stackable = je.getAsJsonObject().get("stackable").getAsBoolean();
				String[] rightClick = new Gson().fromJson(je.getAsJsonObject().get("rightClick"), String[].class);
				System.out.println("Adding item " + name + " with image " + imageid);
				KnighthoodServer.getInstance().getImageManager().addImage(new File(itemsdir, imageid));
				itemlist.add(new Item(name, examine, imageid, stackable, rightClick));
			}
			this.items = itemlist.toArray(new Item[0]);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE, null, ex);
			throw new RuntimeException(ex);
		}
	}

	public File getItemImageFile(Item item) {
		return new File(itemsdir, item.getImageid());
	}

	public Item[] getItems() {
		return items;
	}
}
