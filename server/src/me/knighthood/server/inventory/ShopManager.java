package me.knighthood.server.inventory;

import java.util.HashMap;
import java.util.Map;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.server.server.Server;

/**
 *
 * @author Emir
 */
public class ShopManager {

	private final Server server;
	private final HashMap<Integer, Shop> shopMap = new HashMap<Integer, Shop>();

	public ShopManager(Server server) {
		this.server = server;
	}

	public Server getServer() {
		return server;
	}

	public HashMap<Integer, Shop> getShopMap() {
		return shopMap;
	}

	public Shop getShop(int id) {
		return shopMap.get(id);
	}

	public void removeShop(int id) {
		shopMap.remove(id);
	}

	public Shop addShopInfo(Shop.ShopInfo shopInfo) {
		Shop shop = new Shop(shopInfo.shopName);
		shop.setId(shopInfo.id);
		for (Map.Entry<String, InventoryItem> ent : shopInfo.itemList.entrySet()) {
			shop.addSellingItem(ent.getKey(), ent.getValue());
		}
		shopMap.put(shopInfo.id, shop);
		return shop.setShopManager(this);
	}

	public Shop addShop(int id, String name) {
		Shop shop = new Shop(name);
		shopMap.put(id, shop);
		return shop.setId(id).setShopManager(this);
	}

	public void removeAllShops() {
		shopMap.clear();
	}
}
