package me.knighthood.server.inventory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.File;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.protocol.Item;
import me.knighthood.protocol.PacketImageMD5;
import me.knighthood.protocol.PacketInventory;
import me.knighthood.protocol.PacketInventoryInit;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.server.PacketManager;
import me.knighthood.server.user.Player;

/**
 *
 * @author Emir
 */
public class PlayerInventory extends Inventory {

	private final Player player;

	public PlayerInventory(int size, Player player) {
		super(size);
		this.player = player;
		this.getPlayer().getConnection().getPacketManager().addPacketToSendQueue(new PacketInventoryInit(KnighthoodServer.getInstance().getItemManager().getItems()));
		PacketManager packetManager = this.getPlayer().getConnection().getPacketManager();
		for (Item it : KnighthoodServer.getInstance().getItemManager().getItems()) {
			File file = KnighthoodServer.getInstance().getItemManager().getItemImageFile(it);
			PacketImageMD5 packetImageMD5 = KnighthoodServer.getInstance().getImageManager().imageMD5ToPacket(file);
			packetManager.addPacketToSendQueue(packetImageMD5);
		}
	}

	@Override
	public boolean addItem(String itm, int amount) {
		boolean ret = super.addItem(itm, amount);
		InventoryItem[] itemarray = getItemList().toArray(new InventoryItem[0]);
		this.getPlayer().getConnection().getPacketManager().addPacketToSendQueue(new PacketInventory(itemarray));
		return ret;
	}

	@Override
	public boolean removeItem(String itm, int amount) {
		boolean ret = super.removeItem(itm, amount);
		this.getPlayer().getConnection().getPacketManager().addPacketToSendQueue(new PacketInventory(getItemArray()));
		return ret;
	}

	public void load() {
		if (getPlayer().getPlayerSaveHandler().getJsonObject().get("items") == null) {
			this.save();
		}
		JsonArray jsonArray = getPlayer().getPlayerSaveHandler().getJsonObject().get("items").getAsJsonArray();
		getItemList().clear();
		for (JsonElement je : jsonArray) {
			JsonObject jo = je.getAsJsonObject();
			Item item = getItem(jo.get("item").getAsString());
			int count = jo.get("count").getAsInt();
			if (item != null) {
				getItemList().add(new InventoryItem(item.getName(), count));
			}
		}
		this.getPlayer().getConnection().getPacketManager().addPacketToSendQueue(new PacketInventory(getItemList().toArray(new InventoryItem[0])));
	}

	public void save() {
		JsonArray jsonArray = new JsonArray();
		for (InventoryItem item : getItemList()) {
			JsonObject jsonObject = new JsonObject().getAsJsonObject();
			jsonObject.addProperty("item", item.getItemName());
			jsonObject.addProperty("count", item.getCount());
			jsonArray.add(jsonObject);
		}
		getPlayer().getPlayerSaveHandler().getJsonObject().add("items", jsonArray);
	}

	public Player getPlayer() {
		return player;
	}

}
