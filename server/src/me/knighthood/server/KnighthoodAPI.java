package me.knighthood.server;

import me.knighthood.server.command.Command;
import me.knighthood.server.server.Server;

/**
 *
 * @author Emir
 */
public class KnighthoodAPI {

    /**
     * Register an event
     *
     * @param plugin The plugin object itself
     * @param listener The event listener
     */
    public static void registerEvent(Object plugin, Object listener) {
//        KnighthoodServer.getInstance().getPluginManager().getEventManager().addEvent(plugin, listener);
    }

    /**
     * Register a command
     *
     * @param plugin The plugin object itself
     * @param command The command involved
     */
    public static void registerCommand(Object plugin, Command command) {
        KnighthoodServer.getInstance().getCommandManager().addCommand(command);
    }

    /**
     * Broadcast a message to a server
     *
     * @param server Server to broadcast message to. If null, message will be
     * sent to all servers.
     * @param message Message to be broadcasted
     */
    public static void broadcastMessage(Server server, String message) {
        if (server == null) {
            for (Server s : KnighthoodServer.getInstance().getServerManager().getServers()) {
                s.broadcastMessage(message);
            }
        } else {
            server.broadcastMessage(message);
        }
    }

    /**
     * Gets a server
     *
     * @param id Server's id
     * @return Returns the server which has the name. If no server is found,
     * returns null.
     */
    public static Server getServer(String id) {
        return KnighthoodServer.getInstance().getServerManager().getServer(id);
    }
}
