package me.knighthood.server.user;

import java.util.HashMap;
import java.util.Map;
import me.knighthood.protocol.PacketAction;
import me.knighthood.protocol.PacketChat;
import me.knighthood.protocol.PacketTeleport;
import me.knighthood.protocol.PacketUpdateClientPlayer;
import me.knighthood.protocol.PacketUpdatePlayers;
import me.knighthood.protocol.Position;
import me.knighthood.protocol.Tile;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.command.CommandSender;
import me.knighthood.server.entity.EntityPlayer;
import me.knighthood.server.inventory.PlayerInventory;
import me.knighthood.server.pathfinding.Node;
import me.knighthood.server.pathfinding.Pathfinder;
import me.knighthood.server.server.Connection;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.AntiCheat.DialogHandler;
import me.knighthood.server.world.World;

public class Player extends EntityPlayer implements CommandSender {

    private final Connection connection;
    private final int rights;
    private final PlayerSaveHandler playerSaveHandler;
    private final AntiCheat antiCheat;
    private final PlayerInventory inventory;
    //PATH FINDING STUFFS
    private Node[] nodes = null;
    private int index = 0, tick = 0;

    /**
     *
     * @param connection the connection the player is connected through
     * @see me.knighthood.server.entity.Entity#Entity(java.lang.String)
     */
    public Player(String name, int rights, Connection connection, World world) {

        this(name, name, rights, connection, world);

    }

    /**
     *
     * @param connection the connection the player is connected through
     * @see me.knighthood.server.entity.Entity#Entity(java.lang.String,
     * java.lang.String)
     */
    public Player(String name, String displayName, int rights, Connection connection, World world) {
        super(name, displayName, world);
        this.rights = rights;
        this.connection = connection;
        this.playerSaveHandler = new PlayerSaveHandler(this);
        this.antiCheat = new AntiCheat(this);
        this.inventory = new PlayerInventory(35, this);
        this.getPlayerSaveHandler().load();
        this.setPosition(new Position(this.getPlayerSaveHandler().getJsonObject().get("posX").getAsInt(), this.getPlayerSaveHandler().getJsonObject().get("posY").getAsInt()));
        this.updateClientPosition();

    }

    @Override
    public String getType() {
        return "player";
    }

    /**
     *
     * @see
     * me.knighthood.server.command.CommandSender#sendMessage(java.lang.String)
     *
     */
    @Override
    public void sendMessage(String message) {

        this.getConnection().getPacketManager().addPacketToSendQueue(new PacketChat(message));

    }

    public Connection getConnection() {

        return this.connection;

    }

    @Override
    public int getRights() {

        return rights;

    }

    public PlayerSaveHandler getPlayerSaveHandler() {

        return playerSaveHandler;

    }

    public AntiCheat getAntiCheat() {

        return antiCheat;

    }

    public void teleport(Position newpos) {

        this.setPosition(newpos);
        this.updateClientPosition();
        this.getAntiCheat().setWorldNeedsUpdate(true);

    }

    public PlayerInventory getInventory() {

        return inventory;

    }

    public void sendDialog(String opt, String yes, String no, DialogHandler dialogHandler) {

        String fin = opt + ":" + yes + ":" + no;
        getAntiCheat().setDialog(fin, dialogHandler);

    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Player) {

            Player pl = (Player) obj;

            if (pl.getName().equals(this.getName())) {

                return true;

            }
        }

        return false;

    }

    /**
     * Does path finding for the new position.<br/>
     * TODO: add a noclip param
     *
     * @param to
     */
    @Override
    public void updatePosition(Position to) {
        if (this.nodes != null) {
            return;
        }
        boolean cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerStartingMove", this, this.getPosition(), to);
        if (cancelled) {
            return;
        }

        getAntiCheat().setWorldNeedsUpdate(true);
        Map<Node, Boolean> collids = tilesToBooleanArrayArray();
        try {
            this.nodes = Pathfinder.generate(this.getPosition().getX(), this.getPosition().getY(), to.getX(), to.getY(), collids).toArray(new Node[0]);
        } catch (Exception e) {
            System.err.println("Error during path finding for " + this.getDisplayName() + ". Aborting!");
            return;
        }
        this.index = 1;

        this.getConnection().getPacketManager().addPacketToSendQueue(new PacketAction("moving true"));
    }

    @Override
    public void onTick(Server server) {
        if (this.nodes == null) {
            return;
        }
        if (this.nodes.length == 1 || this.index >= this.nodes.length) {
            this.nodes = null;
            this.getConnection().getPacketManager().addPacketToSendQueue(new PacketAction("moving false"));
            return;
        }

        if (tick > 3 || getAntiCheat().isNoclip()) {
            Position to = new Position(nodes[index].x, (nodes[index].y));
            this.setPosition(to);
            this.updateClientPosition();
            boolean cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerMoving", this, to);
            if (this.index == this.nodes.length - 1 || cancelled) {
                this.getConnection().getPacketManager().addPacketToSendQueue(new PacketAction("moving false"));
                KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerEndingMove", this);
                this.nodes = null;
                this.index = -1;
                getAntiCheat().setWorldNeedsUpdate(true);
            }
            //        this.send
            this.index++;
            tick = 0;
        }
        tick++;
    }

    public void updateClientPosition() {
        getConnection().getPacketManager().addPacketToSendQueue(new PacketUpdateClientPlayer(toProtocolPlayer()));
//        getConnection().getPacketManager().addPacketToSendQueue(new PacketTeleport(getPosition()));
    }

    //not very efficient, need to rewrite
    private Map<Node, Boolean> tilesToBooleanArrayArray() {
        World world = this.getConnection().getServer().getWorld();

        Map<Node, Boolean> ret = new HashMap<Node, Boolean>();

        if (!getAntiCheat().isNoclip()) {
            for (Tile t : world.getTiles(this)) {
                if (ret.get(new Node(t.getX(), t.getY())) == null || !ret.get(new Node(t.getX(), t.getY()))) {
                    ret.put(new Node(t.getX(), t.getY()), t.isCollide());
                }
            }
        }
        for (int i = world.getMinX(); i < world.getMaxX(); i++) {
            for (int j = world.getMinY(); j < world.getMaxY(); j++) {
                if (!ret.containsKey(new Node(i, j))) {
                    ret.put(new Node(i, j), !getAntiCheat().isNoclip());
                }
            }
        }
        return ret;
    }

    @Override
    public void setHealth(double health) {
        updateClientPosition();
        super.setHealth(health);
    }

    public me.knighthood.protocol.Player toProtocolPlayer() {
        return new me.knighthood.protocol.Player(getDisplayName(), getPosition(), getHealth());
    }
}
