package me.knighthood.server.user;

import me.knighthood.server.inventory.Inventory;
import java.util.Map;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.protocol.Item;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketAction;
import me.knighthood.protocol.PacketLights;
import me.knighthood.protocol.PacketShop;
import me.knighthood.protocol.PacketUpdatePlayers;
import me.knighthood.protocol.PacketWorld;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.entity.Entity;
import me.knighthood.server.entity.EntityPerson;
import me.knighthood.server.inventory.Shop;
//import me.knighthood.server.events.DialogHandler;
import me.knighthood.server.world.World;

/**
 *
 * @author Emir
 */
public final class AntiCheat {

    public static final int TILE_RADIUS = 32;
    private final me.knighthood.server.user.Player player;
    //
    private String dialog;
    private DialogHandler dialogHandler;
    private boolean loaded = false;
//    private Node[] nodes = null;
//    private int index = 0, tick = 0;
    private boolean worldNeedsUpdate = false;
    private boolean noclip = false;
    private int openedShopId;

    public AntiCheat(me.knighthood.server.user.Player player) {
        this.player = player;
        this.setWorldNeedsUpdate(true);

        System.gc();
        for (Map.Entry<String, String> ent : player.getConnection().getServer().getWorld().getImagesMD5().entrySet()) {
            Packet p = KnighthoodServer.getInstance().getImageManager().imageMD5ToPacket(ent.getKey(), ent.getValue());
            this.player.getConnection().getPacketManager().addPacketToSendQueue(p);
        }

        for (Entity ent : player.getConnection().getServer().getWorld().getEntities().values()) {
            if (ent instanceof EntityPerson) {
                Packet p = KnighthoodServer.getInstance().getImageManager().imageMD5ToPacket(((EntityPerson) ent).getTextureFile());
                this.player.getConnection().getPacketManager().addPacketToSendQueue(p);
            }
        }

    }

    public void update() {
        if (!loaded) {
            this.player.getConnection().getPacketManager().addPacketToSendQueue(new PacketAction("loaded"));
            loaded = true;
        }
        player.getPlayerSaveHandler().updatePosition();
        World world = player.getConnection().getServer().getWorld();
        me.knighthood.protocol.Player[] playas = new me.knighthood.protocol.Player[world.getPlayers().size()];
        for (int i = 0; i < world.getPlayers().size(); i++) {
            if (!this.player.getName().equalsIgnoreCase(world.getPlayers().get(i).getName())) {
                me.knighthood.server.user.Player serverplyr = world.getPlayers().get(i);
                if (serverplyr != null) {
                    playas[i] = serverplyr.toProtocolPlayer();
                }
            }
        }
        player.getConnection().getPacketManager().addPacketToSendQueue(new PacketUpdatePlayers(playas));
        if (worldNeedsUpdate()) {
            player.getConnection().getPacketManager().addPacketToSendQueue(new PacketLights(world.getLights(player)));
            player.getConnection().getPacketManager().addPacketToSendQueue(new PacketWorld(world.getSize(), world.getTiles(player)));
            AntiCheat.this.setWorldNeedsUpdate(false);
        }

//        if(){}
    }

    public boolean worldNeedsUpdate() {
        return worldNeedsUpdate;
    }

    public void setWorldNeedsUpdate(boolean worldNeedsUpdate) {
        this.worldNeedsUpdate = worldNeedsUpdate;
    }

    public boolean isMuted() {
        //send message if muted
        return false;
    }

    public void runAction(String action) {
        if (action.equals("space")) {
            KnighthoodServer.getInstance().getPluginManager().runEvent("EventSpacePressed", player);
        }
        if (action.startsWith("shop ")) {
            String[] args = action.split(" ");
            String dialogact = action.substring(5);
            if (dialogact.endsWith("close") || !isShopOpen()) {
                this.openShop(null);
            } else {
                //shop shopId itemId amount
                String choice = args[1];
                int shopId = Integer.parseInt(args[2]);
                String itemId = args[3];
                int amount = Integer.parseInt(args[4]);

                if (shopId == this.getOpenedShopId()) {
                    Shop shop = this.player.getWorld().getShopManager().getShop(shopId);
                    if (shop != null) {
                        if (choice.equals("buy")) {
                            shop.playerBuysItem(this.player, itemId, amount);
                        } else if (choice.equals("sell")) {
                            shop.playerSellsItem(this.player, itemId, amount);
                        }
                    }
                }
            }
        }
        if (action.startsWith("dialog ")) {
            String dialogact = action.substring(7);
            if (dialogact.startsWith("option ")) {
                dialogact = dialogact.substring(7);
                if (this.dialogHandler != null) {
                    if (dialogact.equals("0")) {
                        this.dialogHandler.optionNo(player);
                    }
                    if (dialogact.equals("1")) {
                        this.dialogHandler.optionYes(player);
                    }

                }
            }
        }
        if (action.startsWith("inv ")) {
            String invact = action.substring(4);
            if (invact.startsWith("use ")) {
                Item item = Inventory.getItem(invact.split(" ")[1]);
                InventoryItem ii = player.getInventory().getInvFromItem(item);
                boolean cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventUseItem", player, ii);
                if (!cancelled) {
                    player.sendMessage("This item cannot be used right here, right now.");
                }
            }
            if (invact.startsWith("right ")) {
                Item item = Inventory.getItem(invact.split(" ")[1]);
                String option = invact.split(" ")[2];
                InventoryItem ii = player.getInventory().getInvFromItem(item);
                KnighthoodServer.getInstance().getPluginManager().runEvent("EventItemCustomRightClick", player, ii, option);
            }
            if (invact.startsWith("examine ")) {
                Item item = Inventory.getItem(invact.split(" ")[1]);
                InventoryItem ii = player.getInventory().getInvFromItem(item);
                boolean cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventExamineItem", player, ii);
                if (!cancelled) {
                    player.sendMessage(item.getExamine());
                }
            }
        }
    }

    public String getDialog() {
        return dialog;
    }

    public void setDialog(String dialog, DialogHandler dialogHandler1) {
        this.dialog = dialog;
        this.dialogHandler = dialogHandler1;
        Packet act = new PacketAction("dialog " + dialog.replaceAll(" ", "_"));
        player.getConnection().getPacketManager().addPacketToSendQueue(act);
    }

    public static interface DialogHandler {

        public void optionNo(Player player);

        public void optionYes(Player player);
    }

    public void openShop(Shop shop) {
        if (shop == null) {
            if (isShopOpen()) {
                this.setOpenedShopId(-1);
                this.getPlayer().getConnection().getPacketManager().addPacketToSendQueue(new PacketShop(-1, "", null, null));
            }
            return;
        }
        this.setOpenedShopId(shop.getId());
        this.getPlayer().getConnection().getPacketManager().addPacketToSendQueue(shop.toPacket());
        //todo: send packet
    }

    public int getOpenedShopId() {
        return openedShopId;
    }

    public boolean isShopOpen() {
        return openedShopId > 0;
    }

    private void setOpenedShopId(int openedShopId) {
        this.openedShopId = openedShopId;
    }

    public boolean isNoclip() {
        return noclip;
    }

    public void setNoclip(boolean noclip) {
        this.noclip = noclip;
    }

    public Player getPlayer() {
        return player;
    }

}
