package me.knighthood.server.user;

import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.command.CommandSender;

/**
 * @author Roe
 *
 */
public class Console implements CommandSender
{

	private final ConsoleListener listener;
	
	public Console()
	{
		
		this.listener = new ConsoleListener();
		this.getListener().start();
		
	}

	
	/**
	 * 
	 * @see me.knighthood.server.command.CommandSender#getType()
	 */
	@Override
	public String getType()
	{

		return "Console";
	
	}

	
	/**
	 * 
	 * @see me.knighthood.server.command.CommandSender#getName()
	 */
	@Override
	public String getName()
	{

		return "Console";
	
	}

	/**
	 * 
	 * @see me.knighthood.server.command.CommandSender#sendMessage(java.lang.String)
	 */
	@Override
	public void sendMessage(String message)
	{

		System.out.println(message);

	}

        @Override
        public int getRights()
        {
        
                return 2;
        
        }
	

	/**
	 * 
	 * This method is a shorter way to get the console
	 * 
	 * @return the console
	 */
	public static Console getConsole()
	{
		
		return KnighthoodServer.getInstance().getConsole();
		
	}

	private ConsoleListener getListener()
	{
	
		return this.listener;
	
	}
	
}
