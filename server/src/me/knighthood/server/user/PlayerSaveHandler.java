package me.knighthood.server.user;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.server.KnighthoodServer;

/**
 *
 * @author Emir
 */
public class PlayerSaveHandler {

    private final Player player;
    private final File jsonFile;
    private JsonObject jsonObject;

    public PlayerSaveHandler(Player player) {
        this.player = player;
        this.jsonFile = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "players" + File.separator + this.player.getName() + ".json");
    }

    public void updatePosition() {
        this.jsonObject.addProperty("posX", this.getPlayer().getPosition().getX());
        this.jsonObject.addProperty("posY", this.getPlayer().getPosition().getY());
    }

    private void initJson() {
        this.jsonObject = new JsonObject();
        this.jsonObject.addProperty("displayName", this.getPlayer().getDisplayName());
        this.jsonObject.addProperty("posX", this.getPlayer().getPosition().getX());
        this.jsonObject.addProperty("posY", this.getPlayer().getPosition().getY());
    }

    public Player getPlayer() {
        return player;
    }

    public boolean saveExists() {
        return this.jsonFile.exists();
    }

    public void load() {
        try {
            if (!this.jsonFile.exists()) {
                this.initJson();
                this.save();
            }
            this.jsonObject = new JsonParser().parse(new FileReader(this.jsonFile)).getAsJsonObject();
            if (this.jsonObject.has("playerData")) {
                this.getPlayer().getNBT().setJsonObject(this.jsonObject.get("playerData").getAsJsonObject());
            }
            this.getPlayer().getInventory().load();
        } catch (IOException ex) {
            Logger.getLogger(PlayerSaveHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void save() {
        try {
            this.jsonObject.add("playerData", player.getNBT().getJsonObject());
            this.jsonFile.getParentFile().mkdirs();
            this.getPlayer().getInventory().save();
            PrintWriter out = new PrintWriter(new FileWriter(jsonFile));
            out.println(new GsonBuilder().setPrettyPrinting().create().toJson(this.jsonObject));
            out.close();
//        fileWriter
        } catch (IOException ex) {
            Logger.getLogger(PlayerSaveHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }

}
