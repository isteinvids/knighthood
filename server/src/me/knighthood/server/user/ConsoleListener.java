/**
 * 
 */
package me.knighthood.server.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import me.knighthood.server.KnighthoodServer;

/**
 * @author Roe
 * 
 */
public class ConsoleListener extends Thread
{

	public ConsoleListener()
	{

		super("Console Listener");

	}

        @Override
	public void run()
	{

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String in;

		try
		{

			while (((in = reader.readLine()) != null) && (KnighthoodServer.getInstance().isRunning()))
			{

                                in = in.trim();

				KnighthoodServer.getInstance().getCommandManager().executeCommand(Console.getConsole(), in);

			}

		}
		catch (IOException e)
		{

			e.printStackTrace();

		}

	}

}