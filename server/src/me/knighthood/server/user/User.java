package me.knighthood.server.user;

/**
 * @author Roe
 *
 */
public abstract class User
{

	private final String name;
	private String displayName;
	
	public User(String name)
	{
		
		this.name = name;
		this.setDisplayName(this.getName());
		
	}
	
	public User(String name, String displayName)
	{
		
		this.name = name;
		this.setDisplayName(displayName);
		
	}
	
	public abstract boolean hasPermission(String permission);
	public abstract void sendMessage(String message);

	/**
	 * 
	 * This method is used to get the <b>ORIGINAL</b> name of the player
	 * 
	 * @return the original name
	 */
	public String getName()
	{
	
		return this.name;
	
	}

	/**
	 * 
	 * This method is used to get the name of the player to display.
	 * 
	 * @return the display name
	 */
	public String getDisplayName()
	{
	
		return this.displayName;
	
	}

	/**
	 * 
	 * This method is used to set the display name
	 * 
	 * @param displayName the new displayname
	 */
	public void setDisplayName(String displayName)
	{
	
		this.displayName = displayName;
	
	}
	
}