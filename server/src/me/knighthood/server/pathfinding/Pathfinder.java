package me.knighthood.server.pathfinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 Self-explanatory. Comes with 2 methods you can use, one for integer positions and another for nodes as positions. paths return are lists of nodes, but using my class should be very simple :)
 */
public class Pathfinder {

	public static boolean canCutCorners = false;

	private static Node end;
//	private static int[][] gScore;
//	private static int[][] hScore;
//	private static int[][] fScore;
	private static Map<Node, Integer> gScore;
	private static Map<Node, Integer> hScore;
	private static Map<Node, Integer> fScore;
	private static Map<Node, Node> cameFrom;
	private static Map<Node, Boolean> walls;
//    private static boolean[][] walls;

	public static Node toNode(int i, int j) {
		return new Node(i, j);
	}

	public static List<Node> generate(int startX, int startY, int endX, int endY, Map<Node, Boolean> mapWalls) {
		return generate(toNode(startX, startY), toNode(endX, endY), mapWalls);
	}

	public static List<Node> generate(Node start, Node finish, Map<Node, Boolean> mapWalls) {
		List<Node> openNodes = new ArrayList<Node>();
		List<Node> closedNodes = new ArrayList<Node>();
		walls = mapWalls;
		end = finish;
		gScore = new HashMap<Node, Integer>();//int[walls.length][walls[0].length];
		fScore = new HashMap<Node, Integer>();
		hScore = new HashMap<Node, Integer>();
		cameFrom = new HashMap<Node, Node>();
//        cameFrom = new Node[walls.size()][walls.size()];
		openNodes.add(start);
		gScore.put(new Node(start.x, start.y), 0);
		hScore.put(new Node(start.x, start.y), calculateHeuristic(start));
		fScore.put(new Node(start.x, start.y), (int) hScore.get(new Node(start.x, start.y)));
//		gScore[start.x][start.y] = 0;
//		hScore[start.x][start.y] = calculateHeuristic(start);
//		fScore[start.x][start.y] = hScore[start.x][start.y];

		while (openNodes.size() > 0) {
			Node current = getLowestNodeIn(openNodes);
			if (current == null) {
				break;
			}
			if (current.equals(end)) {
				return reconstructPath(current);
			}

			openNodes.remove(current);
			closedNodes.add(current);

			List<Node> neighbors = getNeighborNodes(current);
			for (Node n : neighbors) {

				if (closedNodes.contains(n)) {
					continue;
				}

				int tempGscore = (int) gScore.get(new Node(current.x, current.y)) + distanceBetween(n, current);

				boolean proceed = false;
				if (!openNodes.contains(n)) {
					openNodes.add(n);
					proceed = true;
				} else if (tempGscore < (int) gScore.get(new Node(n.x, n.y))) {
					proceed = true;
				}

				if (proceed) {
					cameFrom.put(new Node(n.x, n.y), current);
					gScore.put(new Node(n.x, n.y), tempGscore);
					hScore.put(new Node(n.x, n.y), calculateHeuristic(n));
					fScore.put(new Node(n.x, n.y), (int) gScore.get(new Node(n.x, n.y)) + (int) hScore.get(new Node(n.x, n.y)));
//                    gScore[n.x][n.y] = tempGscore;
//                    hScore[n.x][n.y] = calculateHeuristic(n);
//                    fScore[n.x][n.y] = gScore[n.x][n.y] + hScore[n.x][n.y];
				}
			}
		}
		return new ArrayList<Node>();
	}

	private static List<Node> reconstructPath(Node n) {
		if (cameFrom.get(new Node(n.x, n.y)) != null) {
			List<Node> path = reconstructPath(cameFrom.get(new Node(n.x, n.y)));
			path.add(n);
			return path;
		} else {
			List<Node> path = new ArrayList<Node>();
			path.add(n);
			return path;
		}
	}

	private static List<Node> getNeighborNodes(Node n) {
		List<Node> found = new ArrayList<Node>();
		//TODO
		if (!(boolean) walls.get(new Node(n.x + 1, n.y))) {
			found.add(toNode(n.x + 1, n.y));
		}
		if (!(boolean) walls.get(new Node(n.x - 1, n.y))) {
			found.add(toNode(n.x - 1, n.y));
		}
		if (!(boolean) walls.get(new Node(n.x, n.y + 1))) {
			found.add(toNode(n.x, n.y + 1));
		}
		if (!(boolean) walls.get(new Node(n.x, n.y - 1))) {
			found.add(toNode(n.x, n.y - 1));
		}
		if (canCutCorners) {
			if (!(boolean) walls.get(new Node(n.x + 1, n.y + 1)) && (!(boolean) walls.get(new Node(n.x + 1, n.y)) || !(boolean) walls.get(new Node(n.x, n.y + 1)))) {
				found.add(toNode(n.x + 1, n.y + 1));
			}
			if (!(boolean) walls.get(new Node(n.x - 1, n.y + 1)) && (!(boolean) walls.get(new Node(n.x - 1, n.y)) || !(boolean) walls.get(new Node(n.x, n.y + 1)))) {
				found.add(toNode(n.x - 1, n.y + 1));
			}
			if (!(boolean) walls.get(new Node(n.x - 1, n.y - 1)) && (!(boolean) walls.get(new Node(n.x - 1, n.y)) || !(boolean) walls.get(new Node(n.x, n.y - 1)))) {
				found.add(toNode(n.x - 1, n.y - 1));
			}
			if (!(boolean) walls.get(new Node(n.x + 1, n.y - 1)) && (!(boolean) walls.get(new Node(n.x + 1, n.y)) || !(boolean) walls.get(new Node(n.x, n.y - 1)))) {
				found.add(toNode(n.x + 1, n.y - 1));
			}
		} else {
			if (!(boolean) walls.get(new Node(n.x + 1, n.y + 1)) && (!(boolean) walls.get(new Node(n.x + 1, n.y)) && !(boolean) walls.get(new Node(n.x, n.y + 1)))) {
				found.add(toNode(n.x + 1, n.y + 1));
			}
			if (!(boolean) walls.get(new Node(n.x - 1, n.y + 1)) && (!(boolean) walls.get(new Node(n.x - 1, n.y)) && !(boolean) walls.get(new Node(n.x, n.y + 1)))) {
				found.add(toNode(n.x - 1, n.y + 1));
			}
			if (!(boolean) walls.get(new Node(n.x - 1, n.y - 1)) && (!(boolean) walls.get(new Node(n.x - 1, n.y)) && !(boolean) walls.get(new Node(n.x, n.y - 1)))) {
				found.add(toNode(n.x - 1, n.y - 1));
			}
			if (!(boolean) walls.get(new Node(n.x + 1, n.y - 1)) && (!(boolean) walls.get(new Node(n.x + 1, n.y)) && !(boolean) walls.get(new Node(n.x, n.y - 1)))) {
				found.add(toNode(n.x + 1, n.y - 1));
			}
		}
		return found;
	}

	private static Node getLowestNodeIn(List<Node> nodes) {
		int lowest = -1;
		Node found = null;
		for (Node n : nodes) {
			int dist = cameFrom.get(new Node(n.x, n.y)) == null ? -1
					: (int) gScore.get(new Node(cameFrom.get(new Node(n.x, n.y)).x, cameFrom.get(new Node(n.x, n.y)).y))
					+ distanceBetween(n, cameFrom.get(new Node(n.x, n.y))) + calculateHeuristic(n);
			if (dist <= lowest || lowest == -1) {
				lowest = dist;
				found = n;
			}
		}
		return found;
	}

	private static int distanceBetween(Node n1, Node n2) {
		return (int) Math.round(10 * Math.sqrt(Math.pow(n1.x - n2.x, 2) + Math.pow(n1.y - n2.y, 2)));
	}

	private static int calculateHeuristic(Node start) {
		return 10 * (Math.abs(start.x - end.x) + Math.abs(start.y - end.y));
	}
}
