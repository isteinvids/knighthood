package me.knighthood.server.pathfinding;

public class Node {

	public int x, y;

	public Node(int i, int j) {
		x = i;
		y = j;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Node) {
			Node n = (Node) obj;
			return (x == n.x && y == n.y);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 89 * hash + this.x;
		hash = 89 * hash + this.y;
		return hash;
	}
}
