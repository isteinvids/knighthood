package me.knighthood.server.plugins;

import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.Player;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.BaseLib;
import org.luaj.vm2.lib.VarArgFunction;

/**
 *
 * @author Emir
 */
public class LuaKnighthood extends BaseLib {
//registerCommand, registerEvent, broadcast

	@Override
	public LuaValue call(LuaValue modname, LuaValue env) {
		LuaTable tc = new LuaTable();
		tc.set("addEntity", new addEntity());
		tc.set("getPlayer", new getPlayer());
		tc.set("removeEntity", new removeEntity());
		tc.set("registerShop", new registerShop());
		tc.set("registerEvent", new registerEvent());
		tc.set("registerCommand", new registerCommand());
		env.set("Knighthood", tc);
		return tc;
	}

	private static class getPlayer extends VarArgFunction {

		@Override
		public Varargs onInvoke(Varargs args) {
			Player player = KnighthoodServer.getInstance().getServerManager().getPlayer(args.arg1().checkjstring());
			if (player != null) {
				LuaTable ret = KnighthoodServer.getInstance().getPluginManager().commandSenderToLua(player);
				return ret;
			}
			return NIL;
		}

	}

	private static class registerEvent extends VarArgFunction {

		@Override
		public Varargs onInvoke(Varargs args) {
			KnighthoodServer.getInstance().getPluginManager().addEvent(args.arg(1).checkjstring(), args.arg(2).checkfunction());
			return NIL;
		}

	}

	private static class addEntity extends VarArgFunction {

		@Override
		public Varargs onInvoke(Varargs args) {
			KnighthoodServer.getInstance().getPluginManager().addEntity(args.arg1().checktable());
			return NIL;
		}
	}

	private static class removeEntity extends VarArgFunction {

		@Override
		public Varargs onInvoke(Varargs args) {
			for (Server serv : KnighthoodServer.getInstance().getServerManager().getServers()) {
				serv.getWorld().removeEntity(args.arg1().checkjstring());
			}
			return NIL;
		}
	}

	private static class registerCommand extends VarArgFunction {

		@Override
		public Varargs onInvoke(Varargs args) {
			KnighthoodServer.getInstance().getPluginManager().addCommand(args.arg(1).checktable());
			return NIL;
		}

	}

	private static class registerShop extends VarArgFunction {

		@Override
		public Varargs onInvoke(Varargs args) {
			KnighthoodServer.getInstance().getPluginManager().addShop(args.arg(1).checktable());
			return NIL;
		}

	}
}
