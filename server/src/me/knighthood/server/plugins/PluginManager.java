package me.knighthood.server.plugins;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.InventoryItem;
import me.knighthood.protocol.Item;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.Position;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.NBTTagCompound;
import me.knighthood.server.Utils;
import me.knighthood.server.command.Command;
import me.knighthood.server.command.CommandSender;
import me.knighthood.server.entity.Entity;
import me.knighthood.server.entity.EntityMonster;
import me.knighthood.server.entity.EntityPerson;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.AntiCheat;
import me.knighthood.server.inventory.Inventory;
import me.knighthood.server.inventory.Shop;
import me.knighthood.server.server.Connection;
import me.knighthood.server.user.Player;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;
import org.luaj.vm2.lib.jse.JsePlatform;

/**
 *
 * @author Emir
 */
public class PluginManager {

	private Globals globals;
	private final Map<LuaFunction, String> eventFunctions = new HashMap<LuaFunction, String>();
	private final Map<LuaTable, String> entityTables = new HashMap<LuaTable, String>();
	private final List<Shop.ShopInfo> shopTables = new ArrayList<Shop.ShopInfo>();

	public PluginManager() {
	}

	public void init() {
		System.out.println("Started loading plugins");
		load();
	}

	public void reload(Server serv) {
		System.out.println("Reloading plugins");
		for (Entity ent : serv.getWorld().getEntities().values()) {
			for (String nn : entityTables.values()) {
				if (ent.getName().equals(nn)) {
					ent.setDead(true);
				}
			}
		}
		serv.getWorld().getShopManager().removeAllShops();
		shopTables.clear();
		entityTables.clear();
		load();
		runEntitiesInit(serv);
		runShopsInit(serv);
	}

	public void tick() {
	}

	public void addShop(LuaTable luaShop) {
		luaTableToShop(luaShop);
	}

	public void addCommand(LuaTable luaCommand) {
		Command cmd = luaTableToCommand(luaCommand);
		KnighthoodServer.getInstance().getCommandManager().addCommand(cmd);
	}

	public void addEntity(LuaTable table) {
		this.entityTables.put(table, table.get("name").checkjstring());
		for (Server serv : KnighthoodServer.getInstance().getServerManager().getServers()) {
			if (serv.isStarted()) {
				EntityPerson ent = luaTableToEntity(serv, table);
				Packet packet = KnighthoodServer.getInstance().getImageManager().imageMD5ToPacket((ent).getTextureFile());
				for (Connection conn : serv.getConnections()) {
					conn.getPacketManager().addPacketToSendQueue(packet);
				}
			}
		}
	}

	public void addEvent(String event, LuaFunction func) {
		this.eventFunctions.put(func, event);
	}

    //Player, InventoryItem, Position, Server, World
	//returns cancelled
	public boolean runEvent(String runEvent, Object... args) {
		boolean cancelled = false;
//        /*
		try {
			for (Map.Entry<LuaFunction, String> ent : eventFunctions.entrySet()) {
				if (ent.getValue().equals(runEvent)) {
					LuaValue retval = ent.getKey().invoke(objectArrayToVarargs(args)).arg1();
					if (retval == LuaValue.TRUE) {
						cancelled = true;
					}
				}
			}
		} catch (Exception ex) {
			System.err.println("A script crashed during event (" + runEvent + ")");
			ex.printStackTrace();
		}
//        */
//        this.globals.call
		return cancelled;
	}

	private Varargs objectArrayToVarargs(Object[] objs) {
		LuaValue[] ret = new LuaValue[objs.length];
		for (int i = 0; i < objs.length; i++) {
			if (objs[i] == null) {
				throw new RuntimeException("Object argument is nulled");
			}
			if (objs[i] instanceof Integer) {
				ret[i] = LuaValue.valueOf((Integer) objs[i]);
			}
			if (objs[i] instanceof Boolean) {
				ret[i] = LuaValue.valueOf((Boolean) objs[i]);
			}
			if (objs[i] instanceof String) {
				ret[i] = LuaValue.valueOf((String) objs[i]);
			}
			if (objs[i] instanceof CommandSender) {
				ret[i] = commandSenderToLua((CommandSender) objs[i]);
			}
//            if (objs[i] instanceof Player) {
//                ret[i] = playerToLua((Player) objs[i]);
//            }
			if (objs[i] instanceof Position) {
				ret[i] = positionToLua((Position) objs[i]);
			}
			if (objs[i] instanceof NBTTagCompound) {
				ret[i] = nbtToLua((NBTTagCompound) objs[i]);
			}
			if (objs[i] instanceof Shop) {
				ret[i] = shopToLua((Shop) objs[i]);
			}
			if (objs[i] instanceof InventoryItem) {
				ret[i] = inventoryItemToLua((InventoryItem) objs[i]);
			}
			if (objs[i] instanceof Server) {
				ret[i] = serverToLua((Server) objs[i]);
			}
		}
		return LuaValue.varargsOf(ret);
	}

	public LuaTable inventoryToLua(final Inventory inventory) {
		LuaTable table = new LuaTable();
		LuaTable itms = new LuaTable();
		InventoryItem[] itemArray = inventory.getItemArray();
		for (int i = 0; i < itemArray.length; i++) {
			itms.set(i + 1, inventoryItemToLua(itemArray[i]));
		}
		table.set("getItemArray", luaArgFunc(itms));
		table.set("getSize", luaArgFunc(inventory.getSize()));
		table.set("getFreeSpace", luaArgFunc(inventory.getFreeSpace()));
		table.set("getItemCount", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg) {
				return valueOf(inventory.getItemCount(arg.checkjstring()));
			}
		});
		table.set("getItemCount", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg) {
				return valueOf(inventory.getItemCount(arg.checkjstring()));
			}
		});
		table.set("hasItem", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg) {
				return valueOf(inventory.hasItem(arg.checkjstring()));
			}
		});
		table.set("addItem", new TwoArgFunction() {

			@Override
			public LuaValue call(LuaValue a, LuaValue b) {
				return valueOf(inventory.addItem(a.checkjstring(), b.checkint()));
			}
		});
		table.set("removeItem", new TwoArgFunction() {

			@Override
			public LuaValue call(LuaValue a, LuaValue b) {
				return valueOf(inventory.removeItem(a.checkjstring(), b.checkint()));
			}
		});
		return table;
	}

	public LuaTable playerToLua(final Player player) {
		LuaTable table = new LuaTable();
		table.set("getName", luaArgFunc(player.getName()));
		table.set("getHealth", luaArgFunc(player.getHealth()));
		table.set("getRights", luaArgFunc(player.getRights()));
		table.set("getInventory", luaArgFunc(inventoryToLua(player.getInventory())));
		table.set("getPosition", luaArgFunc(positionToLua(player.getPosition())));
		table.set("getServer", serverToLua(player.getConnection().getServer()));
		table.set("getNBT", luaArgFunc(nbtToLua(player.getNBT())));
		table.set("setHealth", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue lv) {
				player.setHealth(lv.checkint());
				return NIL;
			}
		});
		table.set("openShop", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue lv) {
				Shop shop = player.getWorld().getShopManager().getShop(lv.checkint());
				player.getAntiCheat().openShop(shop);
				return LuaValue.NIL;
			}
		});
		table.set("sendDialog", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg) {
				luaTableToDialog(arg.checktable(), player);
				return LuaValue.NIL;
			}
		});
		table.set("teleport", new TwoArgFunction() {

			@Override
			public LuaValue call(LuaValue arg1, LuaValue arg2) {
				player.teleport(new Position(arg1.checkint(), arg2.checkint()));
				return LuaValue.NIL;
			}
		});
		table.set("sendMessage", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg) {
				player.sendMessage(arg.toString());
				return NIL;
			}
		});
		return table;
	}

	public LuaTable nbtToLua(final NBTTagCompound nbt) {
		return nbt.getLuaTable();
	}

	public LuaTable commandSenderToLua(final CommandSender commandSender) {
		if (commandSender instanceof Player) {
			return playerToLua((Player) commandSender);
		}
		LuaTable table = new LuaTable();
		table.set("getType", luaArgFunc(commandSender.getType()));
		table.set("getName", luaArgFunc(commandSender.getName()));
		table.set("getRights", luaArgFunc(commandSender.getRights()));
		table.set("sendMessage", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg) {
				commandSender.sendMessage(arg.toString());
				return NIL;
			}
		});
		return table;
	}

	public LuaTable positionToLua(final Position position) {
		LuaTable table = new LuaTable();
		table.set("x", position.getX());
		table.set("y", position.getY());
		return table;
	}

	public LuaTable inventoryItemToLua(final InventoryItem inventoryItem) {
		LuaTable table = new LuaTable();
		table.set("getItemName", luaArgFunc(inventoryItem.getItemName()));
		table.set("getCount", luaArgFunc(inventoryItem.getCount()));
		return table;
	}

	public LuaTable commandToLua(Command command) {
		LuaTable table = new LuaTable();
		table.set("getName", luaArgFunc(command.getName()));
		table.set("getCommand", luaArgFunc(command.getCommand()));
		table.set("getDescription", luaArgFunc(command.getDescription()));
		table.set("getUsage", luaArgFunc(command.getUsage()));
		return table;
	}

	public LuaTable serverToLua(final Server server) {
		LuaTable table = new LuaTable();
		table.set("stop", new ZeroArgFunction() {

			@Override
			public LuaValue call() {
				try {
					KnighthoodServer.getInstance().getServerManager().stopServer(server.getServerName());
				} catch (IOException ex) {
					Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
				}
				return NIL;
			}
		});
		table.set("getName", luaArgFunc(server.getServerName()));
		table.set("isAccepting", luaArgFunc(server.isAccepting()));
		return table;
	}

	public LuaTable itemToLua(final Item item) {
		LuaTable table = new LuaTable();
		table.set("getName", luaArgFunc(item.getName()));
		table.set("getExamine", luaArgFunc(item.getExamine()));
		table.set("getImageid", luaArgFunc(item.getImageid()));
		table.set("getStackable", luaArgFunc(item.isStackable()));
		table.set("getRightClick", stringArrayToLua(item.getRightClick()));
		return table;
	}

	public LuaTable shopToLua(final Shop item) {
		LuaTable table = new LuaTable();
		table.set("getId", luaArgFunc(item.getId()));
		table.set("getShopName", luaArgFunc(item.getShopName()));
		table.set("getSellingItems", luaArgFunc(stringArrayToLua(item.getSellingItem())));
		return table;
	}

	public String[] luaTableToStringArray(LuaTable table) {
		String[] ret = new String[table.length()];
		for (int i = 0; i < table.length(); i++) {
			ret[i] = table.get(i + 1).checkjstring();
		}
		return ret;
	}

	public EntityPerson luaTableToEntity(final Server serv, final LuaTable table) {
		String name = table.get("name").checkjstring();
		String type = table.get("type").isstring() ? table.get("type").checkjstring() : "person";
		String texture = table.get("texture").isstring() ? table.get("texture").checkjstring() : "npc.png";
		int wandering = table.get("wandering").isint() ? table.get("wandering").checkint() : -1;
		int wanderMax = table.get("wanderMax").isint() ? table.get("wanderMax").checkint() : 8;
		int speed = table.get("speed").isint() ? table.get("speed").checkint() : 6;
		Position spawn = luaTableToPosition(table.get("spawn").checktable());
		final LuaFunction onInteractFunc = table.get("onInteract").isfunction() ? table.get("onInteract").checkfunction() : null;

		EntityPerson entityPerson;
		if (type.equals("monster")) {
			entityPerson = new EntityMonster(name, spawn, texture, serv.getWorld());
		} else {
			entityPerson = new EntityPerson(name, spawn, texture, serv.getWorld()) {

				@Override
				public void onInteract(Player player) {
					if (onInteractFunc != null) {
						onInteractFunc.invoke(serverToLua(serv), entityPersonToLua(this), playerToLua(player));
					}
				}
			};
		}
		entityPerson.setWandering(wandering);
		entityPerson.setWanderMax(wanderMax);
		entityPerson.setSpeed(speed);
		serv.getWorld().addEntity(entityPerson);
		return entityPerson;
	}

	public LuaValue entityPersonToLua(EntityPerson entityPerson) {
		LuaTable table = new LuaTable();
		table.set("getName", luaArgFunc(entityPerson.getName()));
		table.set("getTexture", luaArgFunc(entityPerson.getTexture()));
		table.set("getPosition", positionToLua(entityPerson.getPosition()));
		table.set("getSpawnPosition", positionToLua(entityPerson.getSpawnPosition()));
		table.set("getNBT", luaArgFunc(nbtToLua(entityPerson.getNBT())));
		return table;
	}

	public void luaTableToDialog(final LuaTable table, final Player player) {
		AntiCheat.DialogHandler dialogHandler = new AntiCheat.DialogHandler() {

			@Override
			public void optionNo(Player player) {
				if (table.get("onNo").isfunction()) {
					table.get("onNo").checkfunction().invoke(playerToLua(player));
				}
			}

			@Override
			public void optionYes(Player player) {
				if (table.get("onYes").isfunction()) {
					table.get("onYes").checkfunction().invoke(playerToLua(player));
				}
			}
		};
		String text = table.get("text").checkjstring();
		String yes = table.get("yes").checkjstring();
		String no = table.get("no").checkjstring();
		player.sendDialog(text, yes, no, dialogHandler);
	}

	public void luaTableToShop(LuaTable table) {
		final int id = table.get("id").checkint();
		final String name = table.get("name").checkjstring();
		final LuaTable items = table.get("items").checktable();

		Shop.ShopInfo shopInfo = new Shop.ShopInfo(id, name);
		/*
        
		 items = {{selling = lobster, inReturnFor = {itemName = money, count = 10}}}
        
		 */
		for (int i = 1; i <= items.length(); i++) {
			LuaValue lv = items.get(i);
			String selling = lv.get("selling").checkjstring();
			InventoryItem inventoryItem = luaTableToInventoryItem(lv.get("inReturnFor").checktable());
			shopInfo.itemList.put(selling, inventoryItem);
		}
		shopTables.add(shopInfo);
	}

	public InventoryItem luaTableToInventoryItem(LuaTable table) {
		final String itemName = table.get("itemName").checkjstring();
		final int count = table.get("count").checkint();
		InventoryItem ret = new InventoryItem(itemName, count);
		return ret;
	}

	public Command luaTableToCommand(LuaTable table) {
		final String name = table.get("name").checkjstring();
		final String command = table.get("command").checkjstring();
		final String description = table.get("description").checkjstring();
		final String usage = table.get("usage").checkjstring();
		final LuaFunction onCmd = table.get("onCommand").checkfunction();
		final String[] aliases = luaTableToStringArray(table.get("aliases").checktable());
		Command cmd = new Command(name, command, description, usage, aliases) {

			@Override
			public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
				Varargs ret = onCmd.invoke(new LuaValue[]{
					commandSenderToLua(sender),
					commandToLua(command),
					LuaValue.valueOf(commandLabel),
					stringArrayToLua(args)
				});
				return ret.arg1().isboolean() ? ret.arg1().toboolean() : false;
			}
		};
		return cmd;
	}

	private LuaFunction luaArgFunc(final LuaTable arg) {
		return new ZeroArgFunction() {

			@Override
			public LuaValue call() {
				return arg;
			}
		};
	}

	private LuaFunction luaArgFunc(final String arg) {
		return new ZeroArgFunction() {

			@Override
			public LuaValue call() {
				return valueOf(arg);
			}
		};
	}

	private LuaFunction luaArgFunc(final boolean arg) {
		return new ZeroArgFunction() {

			@Override
			public LuaValue call() {
				return valueOf(arg);
			}
		};
	}

	private LuaFunction luaArgFunc(final double arg) {
		return new ZeroArgFunction() {

			@Override
			public LuaValue call() {
				return valueOf(arg);
			}
		};
	}

	private LuaFunction luaArgFunc(final int arg) {
		return new ZeroArgFunction() {

			@Override
			public LuaValue call() {
				return valueOf(arg);
			}
		};
	}

	public boolean setField(Class clazz, String name, Object value, Object instance) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			field.set(instance, value);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public Object getField(Class clazz, String name, Object instance) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field.get(instance);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private void readFilesInDirectory(File dir) {
		for (File subf : dir.listFiles()) {
			if (subf.isDirectory()) {
				readFilesInDirectory(subf);
			}
			if (subf.isFile()) {
				try {
					String script = Utils.readFile(subf);
					globals.load(script).invoke();
//                    LuaValue val = globals.load(globals.FINDER.findResource(filename), "@"+filename, "bt", globals);
//                    globals.set("lcl",val);
//                    globals.get("lcl").invoke();
//                    val.invoke();
//                    globals.set(name, val);
//                    val.invoke();
//                    this.globals.loadfile().invoke();
//                    Varargs args = this.globals.get("dofile").call();
//                    System.out.println("ARGS: "+globals.get("name2").typename());
//                    this.globals.set(name, this.globals.load(script));
				} catch (Exception ex) {
					Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	private void load() {
		this.globals = JsePlatform.standardGlobals();
		this.globals.load(new LuaKnighthood());
		this.eventFunctions.clear();

		File f = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "plugins");
		f.mkdirs();
		for (File subf : f.listFiles()) {
			if (subf.isDirectory()) {
				readFilesInDirectory(subf);
			}
		}
		for (File subf : f.listFiles()) {
			if (subf.isFile()) {
				if (subf.getName().endsWith(".lua")) {
					try {
						this.globals.load(Utils.readFile(subf)).call();
					} catch (IOException ex) {
						Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}

	public Globals getGlobals() {
		return globals;
	}

	private LuaTable stringArrayToLua(String[] strs) {
		LuaTable table = new LuaTable();
		for (int i = 1; i <= strs.length; i++) {
			table.set(i, strs[i - 1]);
		}
		return table;
	}

	private Position luaTableToPosition(LuaTable checktable) {
		Position position = new Position(0, 0);
		position.setX(checktable.get("x").checkint());
		position.setY(checktable.get("y").checkint());
		return position;
	}

	public void runEntitiesInit(Server server) {
		for (LuaTable ent : entityTables.keySet()) {
			if (!server.isStarted()) {
				luaTableToEntity(server, ent);
			}
		}
	}

	public void runShopsInit(Server server) {
		for (Shop.ShopInfo ent : shopTables) {
			server.getWorld().getShopManager().addShopInfo(ent);
		}
	}
}
