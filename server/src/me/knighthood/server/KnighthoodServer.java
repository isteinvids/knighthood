package me.knighthood.server;

import java.io.File;
import me.knighthood.server.command.CommandManager;
import me.knighthood.server.inventory.ItemManager;
import me.knighthood.server.plugins.PluginManager;
import me.knighthood.server.server.*;
import me.knighthood.server.user.*;
import me.knighthood.server.webpanel.WebServer;

/**
 *
 * @author Roe
 *
 */
public final class KnighthoodServer {

	private final File workingDirectory;
	private static KnighthoodServer instance;
	private boolean running;
	private Console console;
	private CommandManager commandManager;
	private ServerManager serverManager;
	private ImageManager imageManager;
	private PluginManager pluginManager;
	private ItemManager itemManager;
	private WebServer webServer;

	public KnighthoodServer(File workingDirectory) {
		instance = this;
		this.workingDirectory = workingDirectory;
		this.setRunning(true);
		this.setConsole(new Console());
		this.setWebServer(new WebServer());
		this.setImageManager(new ImageManager());
		this.setCommandManager(new CommandManager());
		this.setPluginManager(new PluginManager());
		this.setServerManager(new ServerManager());
		this.setItemManager(new ItemManager());
		this.getPluginManager().init();
	}

	private void shutdown() {
		KnighthoodServer.getInstance().getPluginManager().runEvent("OnDisable");
		System.exit(0);
	}

	public static void main(String[] args) {
		new KnighthoodServer(new File(args[0]));
	}

	/**
	 *
	 * This method is used to get the instance of the KnighthoodServer
	 *
	 * @return the knighthood server instance
	 */
	public static KnighthoodServer getInstance() {
		return instance;
	}

	/**
	 *
	 * This method is used to see if the the program is running
	 *
	 * @return true if it's running and false if it's shutting down
	 */
	public boolean isRunning() {
		return this.running;
	}

	/**
	 *
	 * This method is used to tell wether the program should stop or not
	 *
	 * @param running false to shut down, true to let it keep running
	 */
	public void setRunning(boolean running) {
		this.running = running;
		if (!this.isRunning()) {
			this.shutdown();
		}
	}

	/**
	 *
	 * This method is used to get the console
	 *
	 * @return the console
	 */
	public Console getConsole() {
		return this.console;
	}

	private void setConsole(Console console) {
		this.console = console;
	}

	/**
	 *
	 * This method is used to get the command manager
	 *
	 * @return the command manager
	 */
	public CommandManager getCommandManager() {
		return this.commandManager;
	}

	private void setCommandManager(CommandManager commandManager) {
		this.commandManager = commandManager;
	}

	/**
	 *
	 * This method is used to get the server manager
	 *
	 * @return the server manager
	 */
	public ServerManager getServerManager() {
		return this.serverManager;
	}

	private void setServerManager(ServerManager serverManager) {
		this.serverManager = serverManager;
	}

	public ImageManager getImageManager() {
		return imageManager;
	}

	public void setImageManager(ImageManager imageManager) {
		this.imageManager = imageManager;
	}

	public void setPluginManager(PluginManager pluginManager) {
		this.pluginManager = pluginManager;
	}

	public PluginManager getPluginManager() {
		return pluginManager;
	}

	public File getWorkingDirectory() {
		return workingDirectory;
	}

	public WebServer getWebServer() {
		return webServer;
	}

	private void setWebServer(WebServer webServer) {
		this.webServer = webServer;
	}

	public ItemManager getItemManager() {
		return itemManager;
	}

	private void setItemManager(ItemManager itemManager) {
		this.itemManager = itemManager;
	}

}
