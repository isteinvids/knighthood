package me.knighthood.server.world;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.Light;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.Position;
import me.knighthood.protocol.Tile;
import me.knighthood.server.user.AntiCheat;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.Utils;
import me.knighthood.server.entity.Entity;
import me.knighthood.server.inventory.ShopManager;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.Player;

/**
 *
 * @author Emir
 */
public class World {

    public static final int HOURS = 240;
    private static final File tileDirectory = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "world");
    private final List<String> imagesIds = new ArrayList();
    private final List<Player> players = new ArrayList();
    private final Map<String, String> imagesMD5 = new HashMap();
    private Tile[] tiles;
    private Light[] lights;
    private int size;
    private final Map<String, Entity> entities = new ConcurrentHashMap<String, Entity>();
    private final Server server;
    private final ShopManager shopManager;
    private int maxX, maxY, minX, minY;

    public World(Server server) {
        this.server = server;
        this.shopManager = new ShopManager(this.getServer());
        reload();
    }

    public void saveAllEntities() {
        for (Entity ent : entities.values()) {
            ent.saveFile();
        }
    }

    public void reload() {
        try {
            if (!tileDirectory.exists()) {
                tileDirectory.mkdirs();
            }
            for (File f : tileDirectory.listFiles()) {
                if (f.isFile() && f.getName().endsWith(".png")) {
                    KnighthoodServer.getInstance().getImageManager().addImage(f);
                    imagesIds.add(f.getName());
                    imagesMD5.put(f.getName(), Utils.getMD5Checksum(f));
                }
            }
            File jsonfile = new File(tileDirectory, "world.json");
            File lightfile = new File(tileDirectory, "lights.json");

            Gson gson = new Gson();
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(new FileReader(jsonfile)).getAsJsonObject();
            JsonArray lightJsonObject = parser.parse(new FileReader(lightfile)).getAsJsonArray();

            this.size = gson.fromJson(jsonObject.get("size"), Integer.TYPE);
            this.tiles = gson.fromJson(jsonObject.getAsJsonArray("tiles"), Tile[].class);
            this.lights = new Light[lightJsonObject.size()];
            int count = 0;

            for (JsonElement je : lightJsonObject) {
                JsonObject jo1 = je.getAsJsonObject();
                int x = jo1.get("x").getAsInt();
                int y = jo1.get("y").getAsInt();
                float red = jo1.get("red").getAsFloat();
                float green = jo1.get("green").getAsFloat();
                float blue = jo1.get("blue").getAsFloat();
                float alpha = jo1.get("alpha").getAsFloat();
                int scale = jo1.get("scale").getAsInt();
                this.lights[count] = new Light(x, y, scale, red, green, blue, alpha);
                count++;
            }
            calculateSize();
        } catch (Exception ex) {
            Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
            KnighthoodServer.getInstance().setRunning(false);
            throw new RuntimeException(ex);
        }
    }

    public void calculateSize() {
        int maxsx = 0, maxsy = 0;
        int minsx = 0, minsy = 0;
        for (Tile t : getTiles()) {
            if (t.getX() > maxsx) {
                maxsx = t.getX();
            }
            if (t.getY() > maxsy) {
                maxsy = t.getY();
            }
            if (t.getX() < minsx) {
                minsx = t.getX();
            }
            if (t.getY() < minsy) {
                minsy = t.getY();
            }
        }
        this.maxX = maxsx;
        this.maxY = maxsy;
        this.minX = minsx;
        this.minY = minsy;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Tile[] getTiles(Entity entity) {
        List<Tile> tls = new ArrayList();
        for (Tile t : tiles) {
            final int r = AntiCheat.TILE_RADIUS;
            int tx = t.getX();// * this.size;
            int ty = t.getY();// * this.size;

            if (entity.getPosition().getX() < tx + r && entity.getPosition().getX() > tx - r) {
                if (entity.getPosition().getY() < ty + r && entity.getPosition().getY() > ty - r) {
                    tls.add(t);
                }
            }
        }
        return tls.isEmpty() ? null : tls.toArray(new Tile[0]);
    }

    public Light[] getLights(Player player) {
        return getLights();
        /*
         List<Light> tls = new ArrayList();
         for (Light l : lights) {
         final int r = AntiCheat.TILE_RADIUS;
         int tx = l.getX();
         int ty = l.getY();

         if (player.getPosition().getX() < tx + r && player.getPosition().getX() > tx - r) {
         if (player.getPosition().getY() < ty + r && player.getPosition().getY() > ty - r) {
         tls.add(l);
         }
         }
         }
         return tls.isEmpty() ? null : tls.toArray(new Light[0]);
         */
    }

    public Tile[] getTiles() {
        return tiles;
    }

    public int getSize() {
        return size;
    }

    public Map<String, String> getImagesMD5() {
        return imagesMD5;
    }

    public List<String> getImagesIds() {
        return imagesIds;
    }

    public Entity getEntity(String name) {
        for (Entity ent : entities.values()) {
            if (ent.getName().equals(name)) {
                return ent;
            }
        }
        return null;
    }

    public Map<String, Entity> getEntities() {
        return entities;
    }

    public void updateEntity(Entity entity) {
        this.getEntities().put(entity.getName(), entity);
        for (Player p : getPlayers()) {
            sendEntities(p);
        }
    }

    //TODO: make more efficient
    public void sendEntities(Player plyr) {
        for (Entity ent : getEntities().values()) {
            Packet p = ent.getPacket(plyr);
            if (p != null) {
                plyr.getConnection().getPacketManager().addPacketToSendQueue(p);
            }
        }
    }

    public void addEntity(Entity entity) {
        entities.put(entity.getName(), entity);
    }

    public void removeEntityDangerously(String entityName) {
        entities.remove(entityName);
    }

    public void removeEntity(String entityName) {
        Entity torem = entities.get(entityName);
        if (torem != null) {
            torem.setDead(true);
        }
    }

    public Server getServer() {
        return server;
    }

    public void tick() throws Exception {
        KnighthoodServer.getInstance().getPluginManager().runEvent("WorldTick", getServer());
        for (Entity ent : entities.values()) {
            if (!ent.isDead()) {
                ent.onTick(server);
            }
        }
        for (Player player : players) {
            player.onTick(server);
            sendEntities(player);
        }
        for (Entity ent : entities.values()) {
            if (ent.isDead()) {
                entities.remove(ent.getName());
            }
        }
    }

    public void startTickThread() {
        new Thread(server.getServerName() + " world ticks") {
            @Override
            public void run() {
                while (KnighthoodServer.getInstance().isRunning() && KnighthoodServer.getInstance().getServerManager().serverExists(World.this.server.getServerName())) {
                    try {
                        World.this.tick();
                        Thread.sleep(150);
                    } catch (Exception ex) {
                        System.err.println("A server error has occurred!");
                        Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }.start();
    }

    public Light[] getLights() {
        return lights;
    }

    public ShopManager getShopManager() {
        return shopManager;
    }

    public Player[] getPlayersWithinRadius(Position pos, int radius) {
        List<Player> toret = new ArrayList();
        for (Player plyr : getPlayers()) {
            if (plyr.getPosition().getX() < pos.getX() + radius && plyr.getPosition().getX() > pos.getX() - radius) {
                if (plyr.getPosition().getY() < pos.getY() + radius && plyr.getPosition().getY() > pos.getY() - radius) {
                    toret.add(plyr);
                }
            }
        }
        return toret.toArray(new Player[0]);
    }

}
