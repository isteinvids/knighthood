package me.knighthood.server;

import me.knighthood.server.user.AntiCheat;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.PacketImageData;
import me.knighthood.protocol.PacketImageMD5;

/**
 *
 * @author Emir
 */
public class ImageManager {

    private final HashMap<String, byte[]> images = new HashMap();

    public ImageManager() {
    }

    public void addImage(File file) {
        try {
            images.put(file.getName(), read(file));
        } catch (IOException ex) {
            Logger.getLogger(ImageManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public byte[] getImage(String id) {
        return images.get(id);
    }

    public HashMap<String, byte[]> getImages() {
        return images;
    }

    public PacketImageData imageDataToPacket(File file) {
        try {
            return imageDataToPacket(file.getName(), read(file));
        } catch (IOException ex) {
            Logger.getLogger(AntiCheat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PacketImageData imageDataToPacket(String id) {
        return imageDataToPacket(id, getImage(id));
    }

    public PacketImageData imageDataToPacket(String id, byte[] bytes) {
        return new PacketImageData(id, getImage(id));
    }

    public PacketImageMD5 imageMD5ToPacket(File file) {
        try {
            return imageMD5ToPacket(file.getName(), Utils.getMD5Checksum(file));
        } catch (Exception ex) {
            Logger.getLogger(AntiCheat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PacketImageMD5 imageMD5ToPacket(String id, String md5) {
        return new PacketImageMD5(id, md5);
    }

    public void write(File file, byte[] bytes) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.close();
    }

    private byte[] read(File file) throws IOException {
        byte[] buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);
            if (ios.read(buffer) == -1) {
                throw new IOException("EOF reached while trying to read the whole file");
            }
        } finally {
            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException e) {
            }
        }
        return buffer;
    }

}
