package me.knighthood.server.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketInventory;

public class PacketManager {

	private final Connection connection;
	private final ArrayList<Packet> queue = new ArrayList();
	private Packet lastSentPacket;
	private Packet lastRecievedPacket;
	private long lastReset = 0;

	public PacketManager(Connection connection) {
		this.connection = connection;
		lastReset = System.currentTimeMillis();
	}

	public void sendPackets() throws IOException {
		Packet[] packets = this.getQueue().toArray(new Packet[0]);

		this.getQueue().clear();
		this.getConnection().getOut().writeObject(packets);

		for (Packet p : packets) {
			getConnection().getPacketHandling().handleSentPacket(p);
		}

	}

	public void addPacketToSendQueue(Packet packet) {
		this.getQueue().add(packet);
		if (packet instanceof PacketInventory) {
			try {
				this.getConnection().getOut().reset();
			} catch (IOException ex) {
				Logger.getLogger(PacketManager.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public Connection getConnection() {
		return this.connection;
	}

	public ArrayList<Packet> getQueue() {
		return this.queue;
	}

	public Packet getLastSentPacket() {
		return this.lastSentPacket;
	}

	public void setLastSentPacket(Packet lastSentPacket) {
		this.lastSentPacket = lastSentPacket;
	}

	public Packet getLastRecievedPacket() {
		return this.lastRecievedPacket;
	}

	public void setLastRecievedPacket(Packet lastRecievedPacket) {
		this.lastRecievedPacket = lastRecievedPacket;
	}

}
