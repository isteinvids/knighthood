package me.knighthood.server.server;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.user.Player;

/**
 *
 * @author Emir
 */
public class LoginLog {

	private final File logDirectory;
	private final File suspiciousDirectory;

	public LoginLog() {
		this.logDirectory = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "logs" + File.separator + "loginlog" + File.separator + System.currentTimeMillis());
		this.suspiciousDirectory = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "logs" + File.separator + "suspiciousactivity" + File.separator + System.currentTimeMillis());
	}

	public void login(String username, String ip, int rights) {
		File file = new File(logDirectory, username + ip + System.currentTimeMillis() + ".txt");
		file.getParentFile().mkdirs();

		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("username", username);
		jsonObject.addProperty("time", System.currentTimeMillis());
		jsonObject.addProperty("ip", ip);
		jsonObject.addProperty("rights", rights);

		try {
			PrintWriter out = new PrintWriter(new FileWriter(file));
			out.write(new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject));
			out.close();
		} catch (IOException ex) {
			Logger.getLogger(LoginLog.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void logSuspiciousActivity(Player player, String message) {
		long time = System.currentTimeMillis();
		File file = new File(suspiciousDirectory, player.getName() + time + ".txt");
		file.getParentFile().mkdirs();

		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("username", player.getDisplayName());
		jsonObject.addProperty("time", time);
		jsonObject.addProperty("message", message);

		try {
			PrintWriter out = new PrintWriter(new FileWriter(file));
			out.write(new GsonBuilder().setPrettyPrinting().create().toJson(jsonObject));
			out.close();
		} catch (IOException ex) {
			Logger.getLogger(LoginLog.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
