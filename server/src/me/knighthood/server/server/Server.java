package me.knighthood.server.server;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.inventory.ShopManager;
import me.knighthood.server.user.Player;
import me.knighthood.server.world.World;

/**
 * 
 * This class is used to run the internal server
 * 
 * @author Roe
 *
 */
public class Server extends Thread
{

        private final String verifyLink;
	private final String serverName;
	private final String ip;
        private final boolean onlineMode;
	private final int port;
        private final LoginLog loginLog;
	private ServerSocket socket;
	private boolean accepting = true;
	private boolean started = false;
	private final ArrayList<Connection> connections = new ArrayList();
        private final World world;

	/**
	 * 
	 * @param ip the ip the server should bind to
	 * @param port the port the server should bind to
	 */

        public Server(String serverName, String verifyLink, String ip, boolean onlineMode, int port)
        {
		
		super("internal server \""+serverName+"\" running on " + ip + ":" + port);	
                this.serverName = serverName;
                this.onlineMode = onlineMode;
                this.verifyLink = verifyLink;
		this.ip = ip;
		this.port = port;
                this.loginLog = new LoginLog();
		this.world = new World(this);
        }

        @Override
	public void run()
	{
                
		System.out.println("Starting " + this.getName());
                KnighthoodServer.getInstance().getPluginManager().runShopsInit(this);
                KnighthoodServer.getInstance().getPluginManager().runEntitiesInit(this);
                setStarted(true);
                KnighthoodServer.getInstance().getPluginManager().runEvent("ServerStarted", this);
                
		try
		{
		
			this.setSocket(new ServerSocket());
			this.getSocket().bind(new InetSocketAddress(this.getIp(), this.getPort()));
		
		}
		catch (IOException e)
		{

			e.printStackTrace();
                        KnighthoodServer.getInstance().setRunning(false);
			
			return;
		
		}
            
		new ServerChecker(this).start();
                
                getWorld().startTickThread();
                
		while(KnighthoodServer.getInstance().isRunning() && !this.getSocket().isClosed())
		{
			
			if(!this.isAccepting())
			{
				
				break;
				
			}
                        
                        Socket acceptedsocket = null;
			
                        try
			{

				acceptedsocket = this.getSocket().accept();
				this.getConnections().add(new Connection(this, acceptedsocket));
				
			}
			catch(NullPointerException e)
			{
				
				//Surpressing this warning 
				
			}
			catch(Exception e)
			{
				
                                if (acceptedsocket != null)
                                {
                                    
                                        try
                                        {
                                            
                                                acceptedsocket.close();
                                                System.out.println("closed socket");
                                                
                                        }
                                        catch (IOException ex)
                                        {
                                        }
                                }
				System.err.println("Server "+getServerName()+" has crashed, "+e.getMessage());
                                e.printStackTrace();
				
			}
			
		}
	
	}

        public boolean isStarted()
        {
            
                return started;
                
        }

        private void setStarted(boolean started)
        {
            
                this.started = started;
                
        }

	/**
	 * 
	 * This method is used to get the ip of the server
	 * 
	 * @return the server ip
	 */
	public String getIp()
	{
	
		return this.ip;
	
	}

	/**
	 * 
	 * This method is used to get the port of the server
	 * 
	 * @return the server port
	 */
	public int getPort()
	{
	
		return this.port;
	
	}

	/**
	 * 
	 * This method is used to get the server socket
	 * 
	 * @return the server socket
	 */
	public ServerSocket getSocket()
	{
	
		return this.socket;
	
	}

	/**
	 * 
	 * This method is used to set the server socket
	 * 
	 * @param socket the server socket you want to set
	 */
	public void setSocket(ServerSocket socket)
	{
	
		this.socket = socket;
	
	}

	/**
	 * 
	 * This method is used to see wether or not this internal server is accepting new connections
	 * 
	 * @return true if the server is accepting new connections, false if not
	 */
	public boolean isAccepting()
	{
	
		return this.accepting;
	
	}

	/**
	 * 
	 * This method is used to set wether or not the server is accepting
	 * 
	 * @param accepting
	 */
	public void setAccepting(boolean accepting)
	{
	
		this.accepting = accepting;
	
	}

	public ArrayList<Connection> getConnections()
	{
	
		return this.connections;
	
	}

	private class ServerChecker extends Thread
	{
		
		private final Server server;
		
		private ServerChecker(Server server)
		{
			
			this.server = server;
			
		}
		
		public void run()
		{
			
			while(!this.getServer().getSocket().isClosed())
			{
				
				if(!this.getServer().isAccepting() && this.getServer().getConnections().isEmpty())
				{
					
					try
					{
					
						this.getServer().getSocket().close();
					
					}
					catch (IOException e)
					{

						e.printStackTrace();
					
					}
					
				}
				
				try
				{
				
					Thread.sleep(5000);
				
				}
				catch (InterruptedException e)
				{

				}
				
			}
			
		}

		public Server getServer()
		{
		
			return this.server;
		
		}
		
	}

        public LoginLog getLoginLog()
        {
            
                return loginLog;
        
        }

        public World getWorld()
        {
            
                return world;
                
        }
        
        public void broadcastMessage(String message)
        {
        
                for(Player player : getWorld().getPlayers())
                {
                
                        player.sendMessage(message);
                    
                }
            
        }

        public boolean isOnlineMode() 
        {
            
                return onlineMode;
        
        }

        public String getServerName()
        {
            
                return serverName;
        
        }

        public String getVerifyLink()
        {
            
                return verifyLink;
                
        }
}