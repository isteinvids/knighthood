package me.knighthood.server.server.exceptions;

/**
 * @author Roe
 *
 */
public class ServerException extends Exception
{

	private static final long serialVersionUID = -6592568030158005337L;

	public ServerException(String message)
	{
		
		super(message);
		
	}
	
}