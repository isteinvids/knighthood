/**
 * 
 */
package me.knighthood.server.server.exceptions;

/**
 * @author Roe
 *
 */
public class AddressInUseException extends ServerException
{

	private static final long serialVersionUID = -6857244640192889330L;
	private final String address;
	private final String ip;
	private final int port;

	/**
	 * 
	 * @param ip the conflicting ip
	 * @param port the conflicting port
	 */
	public AddressInUseException(String ip, int port)
	{
	
		super("The address " + ip + ":" + port + " are already in use!");
		
		this.address = ip + ":" + port;
		this.ip = ip;
		this.port = port;
		
	}

	/**
	 * 
	 * This method is used to get the ip and port split by a :
	 * 
	 * @return the ip and port split by :
	 */
	public String getAddress()
	{
	
		return this.address;
	
	}

	/**
	 * 
	 * This method is used to get the conflicting ip
	 * 
	 * @return the ip
	 */
	public String getIp()
	{
	
		return this.ip;
	
	}

	/**
	 * 
	 * This method is used to get the conflicting port
	 * 
	 * @return the port
	 */
	public int getPort()
	{
	
		return this.port;
	
	}
	
}