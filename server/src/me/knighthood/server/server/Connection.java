package me.knighthood.server.server;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketAuthenticate;
import me.knighthood.protocol.PacketHandshake;
import me.knighthood.protocol.PacketKick;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.Utils;
import me.knighthood.server.user.Player;

public class Connection extends Thread {

	private final Socket socket;
	private final ObjectInputStream in;
	private final ObjectOutputStream out;
	private final PacketManager packetManager;
	private final Player player;
	private final Server server;
	private final PacketHandling packetHandling;
	private final String ip;

	public Connection(Server server, Socket socket) throws IOException {

		this.socket = socket;
		this.server = server;
		this.packetManager = new PacketManager(this);
		this.packetHandling = new PacketHandling(this);
		this.in = new ObjectInputStream(socket.getInputStream());
		this.out = new ObjectOutputStream(socket.getOutputStream());
		this.ip = socket.getRemoteSocketAddress().toString();
		this.player = handshake();

	}

	private Player handshake() throws IOException {
		Player plyr = null;

		try {
			PacketHandshake clientHandshake = (PacketHandshake) this.getIn().readObject();
			PacketHandshake serverHandshake = new PacketHandshake();

			if (clientHandshake.getMajor() != serverHandshake.getMajor() || clientHandshake.getMinor() != serverHandshake.getMinor()) {

				throw new IOException("client has invalid protocol version!");

			}

//                        this.getOut().writeObject(serverHandshake);
			String sendsession = "null";
			PacketAuthenticate packetAuthenticate = (PacketAuthenticate) this.getIn().readObject();
			//Authenticate session id and get rights
			int rights = 2;
			boolean disc = false;
			if (getServer().isOnlineMode()) {
				String verify = Utils.verifyID(getServer().getVerifyLink(), packetAuthenticate.getUsername(), packetAuthenticate.getSessionID());
				rights = verify.contains(":") ? Integer.parseInt(verify.split(":")[0]) : Integer.parseInt(verify);
				sendsession = rights == 5 ? "Could not verify your account" : verify.split(":")[1];
				if (rights == 5) {
					sendsession = "Could not verify your account";
					disc = true;
				}
			}
			if (KnighthoodServer.getInstance().getServerManager().getPlayer(packetAuthenticate.getUsername()) != null) {
				//TODO: disconnect the player already in server instead
				sendsession = "Already in-game";
				disc = true;
			}
			this.getOut().writeObject(sendsession);
			if (disc) {
				this.close();
			}
			plyr = new Player(packetAuthenticate.getUsername(), rights, this, getServer().getWorld());
			this.getServer().getWorld().getPlayers().add(plyr);
			this.getServer().getWorld().sendEntities(plyr);
			KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerLogin", plyr, server);
			this.getServer().getLoginLog().login(packetAuthenticate.getUsername(), this.getSocket().getInetAddress().getHostAddress(), rights);
			System.out.println(packetAuthenticate.getUsername() + " has connected to the server");
			this.start();

		} catch (Exception ex) {
			throw new IOException(ex);
		}

		return plyr;
	}

	@Override
	public void run() {

		boolean loop = true;
		this.getPlayer().sendMessage("Welcome to Knighthood");
		while (KnighthoodServer.getInstance().isRunning() && (this.getSocket().isConnected() && !this.getSocket().isClosed() && loop)) {

			try {
				this.getPlayer().getAntiCheat().update();
				Packet[] packets = (Packet[]) this.getIn().readObject();
				this.getPacketManager().sendPackets();
				this.getOut().flush();

				if (packets != null) {
					for (Packet p : packets) {
						if (p != null) {
							this.getPacketHandling().handlePacket(p);
						}
					}
				}

				Thread.sleep(20);
			} catch (Exception e) {
				loop = false;
				if (!getServer().isOnlineMode()) {
					e.printStackTrace();
				}
			}
		}
		this.close();
	}

	public void kick(String message) {
		this.getPacketManager().addPacketToSendQueue(new PacketKick(message));
	}

	public void close() {
		System.out.println(this.getPlayer().getName() + " has disconnected");
		getServer().getWorld().saveAllEntities();

		if (getPlayer() != null) {
			this.getPlayer().getPlayerSaveHandler().save();
			this.getServer().getWorld().getPlayers().remove(getPlayer());
			this.getServer().broadcastMessage(getPlayer().getDisplayName() + " has left the world.");
			KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerLogout", player, server);
		}

		try {
			this.getSocket().close();
		} catch (IOException ex) {
			Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 *
	 * This method is used to get the socket of the connected user
	 *
	 * @return the socket
	 */
	public Socket getSocket() {

		return this.socket;

	}

	/**
	 *
	 * This method is used to get the ObjectInputStream of the connected user
	 *
	 * @return the ObjectInputStream
	 */
	public ObjectInputStream getIn() {

		return this.in;

	}

	/**
	 *
	 * This method is used to get the ObjectOutputStream of the connected user
	 *
	 * @return the ObjectOutputStream
	 */
	public ObjectOutputStream getOut() {

		return this.out;

	}

	/**
	 *
	 * This method is used to get the packet manager for the connection
	 *
	 * @return the connection's packet manager
	 */
	public PacketManager getPacketManager() {

		return this.packetManager;

	}

	/**
	 * This method is used to get the player involved in this connection
	 *
	 * @return the connection's player
	 */
	public Player getPlayer() {

		return player;

	}

	/**
	 * This method is used to get the server involved in this connection
	 *
	 * @return the connection's server
	 */
	public Server getServer() {

		return server;

	}

	/**
	 * This method is used to get the packet handling object
	 *
	 * @return the packet handling object
	 */
	public PacketHandling getPacketHandling() {

		return packetHandling;

	}

	public String getIp() {

		return ip;

	}
}
