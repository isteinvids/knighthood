package me.knighthood.server.server;

import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketAction;
import me.knighthood.protocol.PacketAuthenticate;
import me.knighthood.protocol.PacketChat;
import me.knighthood.protocol.PacketEntityInteract;
import me.knighthood.protocol.PacketHandler;
import me.knighthood.protocol.PacketHandshake;
import me.knighthood.protocol.PacketImageData;
import me.knighthood.protocol.PacketImageMD5;
import me.knighthood.protocol.PacketInventory;
import me.knighthood.protocol.PacketInventoryInit;
import me.knighthood.protocol.PacketKick;
import me.knighthood.protocol.PacketLights;
import me.knighthood.protocol.PacketMoveRequest;
import me.knighthood.protocol.PacketQuery;
import me.knighthood.protocol.PacketShop;
import me.knighthood.protocol.PacketTeleport;
import me.knighthood.protocol.PacketUpdateClientPlayer;
import me.knighthood.protocol.PacketUpdateNPC;
import me.knighthood.protocol.PacketUpdatePlayers;
import me.knighthood.protocol.PacketWorld;
import me.knighthood.protocol.Position;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.entity.Entity;
import me.knighthood.server.entity.EntityPerson;

/**
 *
 * @author Emir
 */
public class PacketHandling extends PacketHandler {

	private final Connection connection;

	public PacketHandling(Connection connection) {
		this.connection = connection;
	}

	public Connection getConnection() {
		return connection;
	}

	@Override
	public void handlePacketShop(PacketShop packetShop) {
	}

	@Override
	public void handlePacketAction(PacketAction packetAction) {
		getConnection().getPlayer().getAntiCheat().runAction(packetAction.getCommand());
	}

	@Override
	public void handlePacketImageData(PacketImageData packetImageData) {
	}

	@Override
	public void handlePacketImageMD5(PacketImageMD5 packetImage) {
		Packet p = KnighthoodServer.getInstance().getImageManager().imageDataToPacket(packetImage.getImageName());
		this.getConnection().getPacketManager().addPacketToSendQueue(p);
	}

	@Override
	public void handlePacketInventoryInit(PacketInventoryInit packetInventoryInit) {
		//client only
	}

	@Override
	public void handlePacketAuthenticate(PacketAuthenticate packetAuthenticate) {
		//is not handled here
	}

	@Override
	public void handlePacketChat(PacketChat packetChat) {
		String str = packetChat.getMessage().trim();
		if (getConnection().getPlayer().getRights() == 2 || getConnection().getPlayer().getRights() == 1) {
			if (packetChat.getMessage().startsWith("/")) {
				KnighthoodServer.getInstance().getCommandManager().executeCommand(getConnection().getPlayer(), packetChat.getMessage().substring(1));
				return;
			}
		}
		if (!getConnection().getPlayer().getAntiCheat().isMuted() && !str.equals("")) {
			final int rights = getConnection().getPlayer().getRights();
			int col = rights == 0 ? 7 : (rights == 1 ? 3 : (rights == 2 ? 4 : 0));
			getConnection().getServer().broadcastMessage("#" + col + getConnection().getPlayer().getDisplayName() + ": " + packetChat.getMessage());
		}
	}

	@Override
	public void handlePacketHandshake(PacketHandshake packetHandshake) {
		//idek
	}

//    @Override
//    public void handlePacketMove(PacketMove packetMove) {
//        Position curpos = this.getConnection().getPlayer().getPosition();
//        Position newpos = packetMove.getPosition();
//        //moves
//        boolean cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerMove", getConnection().getPlayer(), curpos, newpos);
//        if (cancelled) {
//            newpos = curpos;
//            this.getConnection().getPacketManager().addPacketToSendQueue(new PacketTeleport(curpos));
//        }
//        this.getConnection().getPlayer().setPosition(newpos);
//    }
	@Override
	public void handlePacketQuery(PacketQuery packetQuery) {
		//not handled here
	}

	@Override
	public void handlePacketTeleport(PacketTeleport packetQuery) {
		//client only
	}

	@Override
	public void handlePacketWorld(PacketWorld packetWorld) {
		//client only
	}

	@Override
	public void handlePacketUpdatePlayers(PacketUpdatePlayers packetUpdatePlayers) {
		//client only
	}

	@Override
	public void handlePacketInventory(PacketInventory packetInventory) {
		//client only
	}

	@Override
	public void handlePacketUpdateNPC(PacketUpdateNPC packetUpdateNPC) {
	}

	@Override
	public void handlePacketLights(PacketLights packetLights) {
//        throw new UnsupportedOperationException("Server-only packet has been received.");
	}

	@Override
	public void handlePacketEntityInteract(PacketEntityInteract packetEntityInteract) {
		getConnection().getPlayer().getAntiCheat().openShop(null);
		Entity ent = getConnection().getServer().getWorld().getEntity(packetEntityInteract.getEntityName());
		if (ent != null && (ent instanceof EntityPerson)) {
			final int ENTITY_RANGE = 2;
			int ex = ent.getPosition().getX();
			int ey = ent.getPosition().getY();

			int px = getConnection().getPlayer().getPosition().getX();
			int py = getConnection().getPlayer().getPosition().getY();

			boolean succ = false;
			if (ex > px - ENTITY_RANGE && ex < px + ENTITY_RANGE) {
				if (ey > py - ENTITY_RANGE && ey < py + ENTITY_RANGE) {
					((EntityPerson) ent).onInteract(getConnection().getPlayer());
					succ = true;
				}
			}
			if (!succ) {
				this.handlePacketMoveRequest(new PacketMoveRequest(ex - 1, ey));
			}
		}
	}

	@Override
	public void handlePacketMoveRequest(PacketMoveRequest packetMoveRequest) {
		getConnection().getPlayer().getAntiCheat().openShop(null);
		int newposx = packetMoveRequest.getNewX();
		int newposy = packetMoveRequest.getNewY();
		this.getConnection().getPlayer().updatePosition(new Position(newposx, newposy));
	}

	@Override
	public void handlePacketKick(PacketKick packetKick) {
	}

	@Override
	public void handleSentPacket(Packet packet) {
		if (packet instanceof PacketKick) {
			getConnection().close();
		}
	}

	@Override
	public void handlePacketUpdateClientPlayer(PacketUpdateClientPlayer packetUpdateClientPlayer) {
	}
}
