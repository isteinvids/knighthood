package me.knighthood.server.server;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.RandomString;
import me.knighthood.server.Utils;
import me.knighthood.server.server.exceptions.*;
import me.knighthood.server.user.Player;

/**
 * 
 * This class is used to manage all the internal servers
 * 
 * @author Roe
 *
 */
public class ServerManager 
{

	private final ArrayList<Server> servers = new ArrayList<Server>();
	private int slots = 45;
	
	public ServerManager()
	{
		
		new ServerChecker().start();
		
	}
	
	public Server createServer(String name, String ip, boolean onlineMode, int port) throws AddressInUseException
	{
            
                return createServer(name, Utils.DEFAULT_VERIFY_LINK, ip, onlineMode, port); 
            
        }
	/**
	 * 
	 * This method is used to create a new server
	 * 
	 * @param ip the ip to bind the server to
	 * @param port the port to bind the server to
	 * @return the created server on success
	 * @throws AddressInUseException this method is thrown when another server is using the ip and port
	 */
	public Server createServer(String name, String verifyLink, String ip, boolean onlineMode, int port) throws AddressInUseException
	{
		
		for(Server server : this.getServers())
		{
			
			if(server.getIp().equalsIgnoreCase(ip) 
					&& server.getPort() == port)
			{
				
				throw new AddressInUseException(ip, port);
				
			}
			
		}
                
                Server server = new Server(name, verifyLink, ip, onlineMode, port);
		this.getServers().add(server);
		
		return server;
		
	}
        
	public boolean serverExists(String ip, int port)
	{
		
		for(Server server : this.getServers())
		{
			
			if(server.getIp().equalsIgnoreCase(ip) 
					&& server.getPort() == port)
			{
				
                                return true;
				
			}
			
		}
                
                return false;
        }
        
	public boolean serverExists(String serverName)
	{
		
		for(Server server : this.getServers())
		{
			
			if(server.getServerName().equalsIgnoreCase(serverName))
			{
				
                                return true;
				
			}
			
		}
                
                return false;
        }
        
	public boolean stopServer(String serverName) throws IOException
	{
		for (Server server : this.getServers())
		{
			
                        for (Player player : server.getWorld().getPlayers())
                        {
                        
                                player.getConnection().kick("Server has stopped");
                            
                        }
                    
			if(server.getServerName().equalsIgnoreCase(serverName))
			{
                            
                                KnighthoodServer.getInstance().getPluginManager().runEvent("ServerStopped", server);
                                server.getWorld().saveAllEntities();
                                server.getSocket().close();
                                getServers().remove(server);
                                return true;
				
			}
			
		}
                
                return false;
        }
	
	public ArrayList<Server> getServers()
	{
	
		return this.servers;
	
	}

	public int getSlots()
	{
	
		return this.slots;
	
	}

	public void setSlots(int slots)
	{
	
		this.slots = slots;
	
	}
	
	private class ServerChecker extends Thread
	{
	
                @Override
		public void run()
		{
			
			while(KnighthoodServer.getInstance().isRunning())
			{
				
				ArrayList<Server> deadServers = new ArrayList();
				
				for(Server server : KnighthoodServer.getInstance().getServerManager().getServers())
				{
					
					try
					{
					
						if(server.getSocket().isClosed())
						{
							
							deadServers.add(server);
							
						}
					
					}
					catch(NullPointerException e)
					{
						
						//Surpressing this
						
					}
					
				}
				
				for(Server server : deadServers)
				{
					
					KnighthoodServer.getInstance().getServerManager().getServers().remove(server);
					
				}
				
				int aliveServers = 0;
				
				for(Server server : KnighthoodServer.getInstance().getServerManager().getServers())
				{
					
					if(server.isAccepting() && !server.getSocket().isClosed())
					{
						
						aliveServers++;
						
					}
					
				}
				
				for (Server server : addServersFromFile())
				{
                                    
                                        if(server.isAccepting())
					{
						
						aliveServers++;
						
					}
					
				}
				
				if(aliveServers == 0)
				{
					
					System.out.println("No alive servers available! Starting a new one!");
					
					try
					{
                                            
						KnighthoodServer.getInstance().getServerManager().createServer("Default", "0.0.0.0", false, 4444).start();
					
					}
					catch (AddressInUseException e)
					{

						e.printStackTrace();
					
					}
					
				}
				
				try
				{
				
					Thread.sleep(25000);
				
				}
				catch (InterruptedException e)
				{

					//Again we surpress this warning cause we don't give a DAYUM
				
				}
				
			}
			
		}
		
	}
        
        public Player[] getAllPlayers()
        {
            
                List<Player> ret = new ArrayList<Player>();
                
            
                for (Server server : getServers())
                {
                    
                        ret.addAll(server.getWorld().getPlayers());
                        
                }
                
                return ret.toArray(new Player[0]);
        }
        
        public Player getPlayer(String name)
        {
            
                for (Server server : getServers())
                {
                    
                        for (Player player : server.getWorld().getPlayers())
                        {
                            
                                if (player.getName().equals(name))
                                {
                                    
                                        return player;
                                        
                                }
                                
                        }
                        
                }
                
                return null;
        }
        
	public Server getServer(String id)
        {
            
                for(Server s : getServers())
                {
                    
                        if(s.getServerName().equalsIgnoreCase(id))
                        {
                            
                                return s;
                                
                        }
                        
                }
                
                return null;
        }
        
        
        public Server[] addServersFromFile()
        {
                List<Server> server = new ArrayList();
                File serverjson = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "servers.json");
                if (serverjson.exists())
                {
                        try
                        {
                            
                                JsonArray jsonArray = new JsonParser().parse(new FileReader(serverjson)).getAsJsonArray();
                                for(JsonElement jo : jsonArray)
                                {
                                    
                                        JsonObject jsonObject = jo.getAsJsonObject();
                                        String name = jsonObject.get("name").getAsString();
                                        String verifyLink = jsonObject.has("verifyLink") ? jsonObject.get("verifyLink").getAsString() : Utils.DEFAULT_VERIFY_LINK;
                                        String ip = jsonObject.get("ip").getAsString();
                                        int port = jsonObject.get("port").getAsInt();
                                        boolean online = jsonObject.get("onlineMode").getAsBoolean();
                                        
                                        try
                                        {
                                                
                                                if(!KnighthoodServer.getInstance().getServerManager().serverExists(ip, port))
                                                {
                                                    
                                                        Server s = createServer(name, verifyLink, ip, online, port);
                                                        server.add(s);
                                                        s.start();
                                                        
                                                }
                                                
                                        }
                                        catch (AddressInUseException ex)
                                        {
                                            
                                                Logger.getLogger(ServerManager.class.getName()).log(Level.SEVERE, null, ex);
                                                
                                        }
                                        
                                }
                                
                        }
                        catch (FileNotFoundException ex)
                        {
                            
                                Logger.getLogger(ServerManager.class.getName()).log(Level.SEVERE, null, ex);
                                
                        }
                
                }
        
                return server.toArray(new Server[0]);
        }
}