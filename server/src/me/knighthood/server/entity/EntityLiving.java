/**
 *
 */
package me.knighthood.server.entity;

import me.knighthood.server.world.World;

/**
 *
 * @author Roe
 *
 */
public class EntityLiving extends Entity {

	private double health = 100;

	public EntityLiving(String name, World world) {

		super(name, world);

	}

	public String getType() {
		return "living";
	}

	/**
	 *
	 * This method is used to return the health of the entity
	 *
	 * @return the health of the entity
	 */
	public double getHealth() {
		return this.health;
	}

	/**
	 *
	 * This method is used to set the health of the entity
	 *
	 * @param health the new health
	 */
	public void setHealth(double health) {
		this.health = health;
	}

}
