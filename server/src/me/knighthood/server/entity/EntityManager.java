package me.knighthood.server.entity;

import java.util.ArrayList;

public class EntityManager {

	private final ArrayList<Entity> entities = new ArrayList<Entity>();

	public void updateEntities() {
		for (Entity entity : this.getEntities()) {
			if (entity instanceof EntityLiving) {
				EntityLiving entityLiving = (EntityLiving) entity;
				if (entityLiving.getHealth() <= 0) {
					entityLiving.setDead(entity.onDeath());
					if (!entityLiving.isDead() && entityLiving.getHealth() <= 0) {
						entityLiving.setHealth(.01D);
					}
				}
			}
			entity.onUpdate();
		}
	}

	public ArrayList<Entity> getEntities() {
		return this.entities;
	}

}
