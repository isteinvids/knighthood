package me.knighthood.server.entity;

import java.util.Random;
import me.knighthood.protocol.Position;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.Player;
import me.knighthood.server.world.World;

/**
 *
 * @author Emir
 */
public class EntityMonster extends EntityPerson {

	private final Random rand = new Random();
	private Player attacking;
	private int lastAttack;

	public EntityMonster(String name, Position spawn, World world) {
		this(name, spawn, "npc.png", world);
	}

	public EntityMonster(String name, Position spawn, String texture, World world) {
		super(name, spawn, texture, world);
	}

	@Override
	public String getType() {
		return "monster";
	}

	@Override
	public void onTick(Server server) {
		shouldWander = (getAttacking() == null);

		super.onTick(server);

		final int radius = 10;
		if (getAttacking() != null) {
			boolean flag = false;
			if (getAttacking().getPosition().getX() < getPosition().getX() + radius && getAttacking().getPosition().getX() > getPosition().getX() - radius) {
				if (getAttacking().getPosition().getY() < getPosition().getY() + radius && getAttacking().getPosition().getY() > getPosition().getY() - radius) {
					flag = true;
				}
			}
			if (lastAttack > 7) {
				if (flag) {
					if (!getAttacking().getPosition().equals(getPosition())) {
						this.updatePosition(getAttacking().getPosition().copy().offset(0, 0));
					}

					if (!isCurrentlyWalking()) {
						if (getAttacking().getPosition().equals(getPosition())) {
							System.out.println("setting health " + getAttacking().getHealth());
							getAttacking().setHealth(getAttacking().getHealth() - 5);
						}
					}
				}
				lastAttack = 0;
			}
			lastAttack++;
			if (!flag) {
				setAttacking(null);
			}
		} else {
			for (Player pl : getWorld().getPlayers()) {
				if (pl != null) {
					if (pl.getPosition().getX() < getPosition().getX() + radius && pl.getPosition().getX() > getPosition().getX() - radius) {
						if (pl.getPosition().getY() < getPosition().getY() + radius && pl.getPosition().getY() > getPosition().getY() - radius) {
							setAttacking(pl);
						}
					}
				}
			}
		}
	}

	@Override
	public void onInteract(Player player) {
		super.onInteract(player);
		boolean flag = true;
		if (attacking != null) {
			flag = rand.nextBoolean();
		}
		if (flag) {
			player.setHealth(player.getHealth() - 10); //todo
			setAttacking(player);
		}
	}

	public void setAttacking(Player attacking) {
		this.attacking = attacking;
	}

	public Player getAttacking() {
		return attacking;
	}
}
