package me.knighthood.server.entity;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.Position;
import me.knighthood.protocol.Tile;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.NBTTagCompound;
import me.knighthood.server.pathfinding.Node;
import me.knighthood.server.pathfinding.Pathfinder;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.Player;
import me.knighthood.server.world.World;

/**
 * @author Roe
 *
 */
public abstract class Entity {

    private final String name;
    //TEMPORARY MEMORY, data is lost after player reconnects/server restart
    private NBTTagCompound nbt;
    private boolean dead = false;
    private Position position;
    private World world;
    private Node[] nodes = null;
    private int index = 0, tick = 0;

    public Entity(String name, World world) {
        this(name, world, new Position(0, 0));
    }

    public Entity(String name, World world, Position position) {
        this.name = name;
        this.world = world;
        this.position = position;
        this.nbt = new NBTTagCompound();
    }

    public void loadFile() {
        try {
            File jsonFile = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "entities" + File.separator + this.getName() + ".json");
            if (!jsonFile.exists()) {
                saveFile();
            }
            JsonObject jsonObject = new JsonParser().parse(new FileReader(jsonFile)).getAsJsonObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveFile() {
        File jsonFile = new File(KnighthoodServer.getInstance().getWorkingDirectory(), "entities" + File.separator + this.getName() + ".json");
        jsonFile.getParentFile().mkdirs();
        try {
            PrintWriter out = new PrintWriter(new FileWriter(jsonFile));
            out.println(new GsonBuilder().setPrettyPrinting().create().toJson(getNBT().getJsonObject()));
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * This method is called when the entity is told to update. This can be used
     * to move the entity and such
     *
     */
    public void onUpdate() {
    }

    /**
     *
     * This method is called when the entity is dead.
     *
     * @return true to accept the entity death, false to keep it alive
     * <b>NOTE</b> Returning false will set the health of the entity to .01 IF
     * the entity health is 0
     */
    public boolean onDeath() {
        return true;
    }

    /**
     *
     * This method is used to get the <b>ORIGINAL</b> name of the entity
     *
     * @return the original name
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * This method is used to get the coord set of the entity
     *
     * @return the coord set
     */
    public Position getPosition() {
        return this.position == null ? new Position(0, 0) : this.position;
    }

    /**
     *
     * This method is used to set the coord set of the entity
     *
     * @param coords the coord set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * Does path finding for the new position.<br/>
     * TODO: add a noclip param
     *
     * @param to
     */
    public void updatePosition(Position to) {
        if (this.nodes != null) {
            return;
        }
        boolean cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerStartingMove", this, this.getPosition(), to);
        if (cancelled) {
            return;
        }

        Map<Node, Boolean> collids = tilesToBooleanArrayArray();
        try {
            this.nodes = Pathfinder.generate(this.getPosition().getX(), this.getPosition().getY(), to.getX(), to.getY(), collids).toArray(new Node[0]);
        } catch (Exception e) {
            System.err.println("Error during path finding for " + this.getName() + ". Aborting!");
            return;
        }
        this.index = 1;

        Position[] temp = new Position[this.nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            temp[i] = new Position(nodes[i].x, nodes[i].y);
        }
    }

    public void cancelWalking() {
        this.nodes = null;
    }

    public boolean isCurrentlyWalking() {
        return this.nodes != null;
    }

    public void onTick(Server server) {
        if (this.nodes == null) {
            return;
        }
        if (this.nodes.length == 1 || this.index >= this.nodes.length) {
            this.nodes = null;
            return;
        }

        if (tick > 3) {
            Position to = new Position(nodes[index].x, (nodes[index].y));
            this.setPosition(to);
//          Send position updates to client via entity packet?
            boolean cancelled = false;
            if (this instanceof EntityPlayer) {
                cancelled = KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerMoving", this, to);
            }
            if (this.index == this.nodes.length - 1 || cancelled) {
                if (this instanceof EntityPlayer) {
                    KnighthoodServer.getInstance().getPluginManager().runEvent("EventPlayerEndingMove", this);
                }
                this.nodes = null;
                this.index = -1;
            }
            this.index++;
            tick = 0;
        }
        tick++;
    }

    public Packet getPacket(Player player) {
        return null;
    }

    private Map<Node, Boolean> tilesToBooleanArrayArray() {
        World world1 = this.getWorld();
        Map<Node, Boolean> ret = new HashMap<Node, Boolean>();

        for (Tile t : world1.getTiles(this)) {
            if (ret.get(new Node(t.getX(), t.getY())) == null || !ret.get(new Node(t.getX(), t.getY()))) {
                ret.put(new Node(t.getX(), t.getY()), t.isCollide());
            }
        }
        for (int i = world1.getMinX(); i < world1.getMaxX(); i++) {
            for (int j = world1.getMinY(); j < world1.getMaxY(); j++) {
                if (!ret.containsKey(new Node(i, j))) {
                    ret.put(new Node(i, j), true);
                }
            }
        }
        return ret;
    }

    public World getWorld() {
        return world;
    }

    /**
     *
     * This method is used to tell wether or not the entity is dead
     *
     * @return true if it's dead and false if it's alive
     */
    public boolean isDead() {
        return this.dead;
    }

    /**
     *
     * @param dead
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public NBTTagCompound getNBT() {
        return nbt;
    }
}
