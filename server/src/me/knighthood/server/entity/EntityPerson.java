package me.knighthood.server.entity;

import java.io.File;
import java.util.Random;
import me.knighthood.protocol.Packet;
import me.knighthood.protocol.PacketUpdateNPC;
import me.knighthood.protocol.Position;
import me.knighthood.server.KnighthoodServer;
import me.knighthood.server.server.Server;
import me.knighthood.server.user.Player;
import me.knighthood.server.world.World;

/**
 *
 * @author Emir
 */
public class EntityPerson extends EntityLiving {

	private String texture;
	private int wanderTick, wandering = -1, wanderMax = 16, speed;
	private Position wanderTo = null, spawnPosition;
	protected boolean shouldWander = true;

	public EntityPerson(String name, Position spawn, World world) {
		this(name, spawn, "npc.png", world);
	}

	public EntityPerson(String name, Position spawn, String texture, World world) {
		super(name, world);
		this.spawnPosition = spawn;
		this.setPosition(getSpawnPosition());
		addImage(texture);
	}

	@Override
	public String getType() {
		return "person";
	}

	public void addImage(String img) {
		this.setTexture(img);
		KnighthoodServer.getInstance().getImageManager().addImage(getTextureFile());
	}

	public void setTexture(String texture) {
		this.texture = texture;
	}

	public String getTexture() {
		return texture;
	}

	public File getTextureFile() {
		return new File(KnighthoodServer.getInstance().getWorkingDirectory(), texture);
	}

	@Override
	public Packet getPacket(Player player) {
		return new PacketUpdateNPC(getName(), getType(), getTexture(), getHealth(), getPosition(), isDead());
	}

	@Override
	public void onTick(Server server) {
		super.onTick(server);

		if (shouldWander) {
			if (wandering > 0) {
				if (!isCurrentlyWalking()) {
					boolean toofar = false;
					if (getPosition().getX() > getSpawnPosition().getX() + wanderMax || getPosition().getX() < getSpawnPosition().getX() - wanderMax) {
						toofar = true;
					}
					if (getPosition().getY() > getSpawnPosition().getY() + wanderMax || getPosition().getY() < getSpawnPosition().getY() - wanderMax) {
						toofar = true;
					}
					//def speed 30, speed = 6
					if (wanderTick > (speed * 5)) {
						Random rand = new Random();
						int r0 = rand.nextInt(wandering * 2) - wandering;
						int r1 = rand.nextInt(wandering * 2) - wandering;
						wanderTo = new Position(getPosition().getX() + r0, getPosition().getY() + r1);
						if (toofar) {
							wanderTo = getSpawnPosition();
						}
						wanderTick = 0;
						this.updatePosition(wanderTo);
					}
					wanderTick++;
				}
			}
		}
	}

	public void setWandering(int wandering) {
		this.wandering = wandering;
	}

	public void setWanderMax(int wanderMax) {
		this.wanderMax = wanderMax;
	}

	public Position getSpawnPosition() {
		return spawnPosition;
	}

	public void onInteract(Player player) {
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return speed;
	}

	@Override
	public void setHealth(double health) {
		super.setHealth(health);
		for (Player plyr : getWorld().getPlayersWithinRadius(getPosition(), 16)) {
			plyr.getConnection().getPacketManager().addPacketToSendQueue(getPacket(plyr));
		}
	}
}
