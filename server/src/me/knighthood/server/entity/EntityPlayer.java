package me.knighthood.server.entity;

import me.knighthood.server.world.World;

/**
 * @author Roe
 *
 */
public class EntityPlayer extends EntityLiving {

	private String displayName;

	/**
	 * @param name the name of the player
	 */
	public EntityPlayer(String name, World world) {
		this(name, name, world);
	}

	/**
	 *
	 * @param name the player's username
	 * @param displayName the name to display
	 */
	public EntityPlayer(String name, String displayName, World world) {
		super(name, world);
		this.setDisplayName(displayName);
	}

	/**
	 *
	 * This method is used to get the player's display name
	 *
	 * @return the player's display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 *
	 * This method is used to set the player's display name
	 *
	 * @param displayName the player's new display name
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
