--print("test loaded");
print("Loading utils");

Knighthood.registerCommand(CMD_tp);
Knighthood.registerCommand(CMD_tell);
Knighthood.registerCommand(CMD_give);
Knighthood.registerCommand(CMD_debug);

Knighthood.registerEvent("EventItemCustomRightClick", moneyLookAction);
Knighthood.registerEvent("EventItemCustomRightClick", beachballKickAction);
Knighthood.registerEvent("EventItemCustomRightClick", cakeEatAction);

Knighthood.registerEvent("EventPlayerMoving", playerInHouse);

Knighthood.registerShop(cake_shop);

Knighthood.addEntity(cakeman);


--			player.getNBT().add("caketest", player.getNBT().getAsNumber("caketest") + 1);
--			player.sendMessage("TESTTEST " .. player.getNBT().getAsNumber("caketest"));


--[[
--Shop test: 
shopt = {}
shopt.id = 15;
shopt.name = "some shop name v2";
shopt.items = {
                  {selling = "lobster", inReturnFor = {itemName = "money", count = 30}},
                  {selling = "shrimp", inReturnFor = {itemName = "money", count = 20}},
                  {selling = "raw_shrimp", inReturnFor = {itemName = "money", count = 10}}
              };
Knighthood.registerShop(shopt);
--]]