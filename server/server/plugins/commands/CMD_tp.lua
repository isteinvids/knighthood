CMD_tp = {} -- command new

function oncmd(sender, command, label, args)
	if sender.getName() ~= "Console" then
		if #args == 2 then
			tpx = tonumber(args[1]);
			tpy = tonumber(args[2]);
			if tpx ~= nil and tpy ~= nil then
				sender.teleport(tpx, tpy);
				sender.sendMessage("teleported to " .. tpx .. ", " .. tpy);
				return true;
			else
				sender.sendMessage("Invalid coords");
			end
		else
			sender.sendMessage("Not enough arguments");
		end
	else
		sender.sendMessage("Console can't");
	end
	return false;
end

CMD_tp = {}
CMD_tp.name = "TP";
CMD_tp.command = "tp";
CMD_tp.description = "tp to position";
CMD_tp.usage = "/tp <x> <y>";
CMD_tp.aliases = {};
CMD_tp.onCommand = oncmd