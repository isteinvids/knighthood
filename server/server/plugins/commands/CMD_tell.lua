CMD_tell = {} -- command new

function oncmd(sender, command, label, args)
    if #args >= 2 then
        playerName = args[1];
        recev = Knighthood.getPlayer(playerName);
        fin = "";
        for cc = 2, #args do
            fin = fin .. args[cc] .. " ";
        end
        if recev == nil then
            sender.sendMessage("Invalid player: " .. playerName);
            return false;
        end
        recev.sendMessage("PM From " .. sender.getName() .. ": " .. fin);
        return true;
    end
    
    sender.sendMessage("Not enough arguments");
    return false;
end

CMD_tell = {}

CMD_tell.name = "Tell";
CMD_tell.command = "Tell";
CMD_tell.description = "send a private message to a player";
CMD_tell.usage = "/tell <username> <message>";
CMD_tell.aliases = {"whisper", "pm", "msg"};
CMD_tell.onCommand = oncmd