CMD_give = {} -- command new

function oncmd(sender, command, label, args)
	if sender.getName() ~= "Console" then
		if #args == 2 then
			count = tonumber(args[2]);
			if count ~= nil then
				if label == "take" then
					sender.getInventory().removeItem(args[1], count);
					sender.sendMessage("Took " .. count .. " of " .. args[1]);
				else
					sender.getInventory().addItem(args[1], count);
					sender.sendMessage("Gave " .. count .. " of " .. args[1]);
				end
				return true;
			else
				sender.sendMessage("Invalid count");
			end
		else
			sender.sendMessage("Not enough arguments");
		end
	else
		sender.sendMessage("Console can't");
	end
	return false;
end

CMD_give = {}
CMD_give.name = "Give";
CMD_give.command = "give";
CMD_give.description = "give an item to yourself";
CMD_give.usage = "/give <item> <count>";
CMD_give.aliases = {"take"};
CMD_give.onCommand = oncmd