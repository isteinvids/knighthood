cake_shop = {}
cake_shop.id = 10;
cake_shop.name = "Cakeman Shop";
cake_shop.items = {
                  {selling = "cake", inReturnFor = {itemName = "money", count = 20}},
                  {selling = "copper_ore", inReturnFor = {itemName = "money", count = 50}}
              };

function cakeEatAction(player, item, option)
	if item.getItemName() == "cake" then
		if option == "eat" then
			--add heal effect
			player.sendMessage("You eat a cake...");
			player.getInventory().removeItem(item.getItemName(), 1);
		end
	end
end

function cakemanFunc(server, entity, player)
	dialog = {};
	dialog.text = "Do you want to buy cake plox";
	dialog.yes = "sure";
	dialog.no = "no wtf";
	dialog.onYes = function (player) player.openShop(10) end;
	dialog.onNo = function (player) player.sendMessage("cakeman: i thought you were my friend") end;
	player.sendDialog(dialog);
end

cakeman = {};
cakeman.name = "cakeman";
--cakeman.type = "monster";
cakeman.wandering = 4;
cakeman.wanderMax = 4;
cakeman.spawn = {x = 10, y = 10};
cakeman.onInteract = cakemanFunc;