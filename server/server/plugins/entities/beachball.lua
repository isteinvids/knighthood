--todo: save custom information to entities/players

function onBeachballYes(player)
	bchb = player.getNBT().getAsString("beachballTemp");
	player.getInventory().addItem("beachball", 1);
	Knighthood.removeEntity(bchb);
end;

function beachballFunc(server, entity, player)
	player.getNBT().add("beachballTemp", entity.getName());
	dialog = {};
	dialog.text = "Pick up the magic beachball?";
	dialog.yes = "Yes";
	dialog.no = "No";
	dialog.onYes = onBeachballYes;
	dialog.onNo = function (player) --[[player.sendMessage("nah")]] end;
	player.sendDialog(dialog);
end

beachball = {};
beachball.texture = "ent_beachball.png";
beachball.name = "beachball";
beachball.wandering = 4;
beachball.wanderMax = 8;
beachball.speed = 0;
beachball.spawn = {x = 10, y = 10};
beachball.onInteract = beachballFunc;

function beachballKickAction(player, item, option)
	if item.getItemName() == "beachball" then
		if option == "kick" then
			pos = player.getPosition();
			beachball.name = "--beachball_" .. pos.x .. "-" .. pos.y;
			beachball.spawn = {x = pos.x, y = pos.y};
			player.getInventory().removeItem(item.getItemName(), 1);
			Knighthood.addEntity(beachball);
		end
	end
end